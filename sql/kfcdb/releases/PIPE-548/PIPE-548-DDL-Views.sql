use
kfcdb;

#Note: Some views depend upon/select from other views, so dependency order matters.


create
or replace view vw_carrier
as
select id
     , name
     , created_on
     , updated_on
     , created_by
     , updated_by
from carrier
order by name;


create
or replace view vw_cluster
as
select cl.id
     , cl.name
     , cl.panorama_url
     , cl.address_limit
     , cl.address_group_limit
     , cl.exception_limit
     , (select count(1) from address ad join address_group ag on ag.id=ad.address_group_id where ag.cluster_id = cl.id) as address_count
     , (select count(1) from address_group ag where ag.cluster_id = cl.id) as address_group_count
     , cl.created_on
     , cl.updated_on
     , cl.created_by
     , cl.updated_by
from cluster cl;


create
or replace view vw_ip_pool
as
select ip.id
     , ip.cluster_id
     , ip.carrier_id
     , cl.name cluster_name
     , ca.name carrier_name
     , ip.pool
     , ip.cidr
     , ip.start_ip
     , ip.end_ip
     , ip.created_on
     , ip.updated_on
     , ip.created_by
     , ip.updated_by
     , ip.pool_size
     , ip.num_assigned
from ip_pool ip
         join cluster cl on cl.id = ip.cluster_id
         join carrier ca on ca.id = ip.carrier_id
order by cl.name, ca.name;
#limit 1;


create
or replace view vw_policy
as
select id
     , name
     , palo_alto_id
     , description
     , created_on
     , updated_on
     , created_by
     , updated_by
from policy
order by name
;


create
or replace view vw_address_group
as
select ag.id
     , ag.name
     , ag.cluster_id
     , ag.corp_id
     , (select count(1) from address where address_group_id=ag.id) as address_count
     , ag.is_available
     , ag.created_on
     , ag.updated_on
     , ag.created_by
     , ag.updated_by
from address_group ag
order by name
;


create
or replace view vw_address
as
select ad.id
     , ad.address_group_id
     , ag.name address_group_name
     , cp.name corp 
     , ad.mdn
     , ad.imei
     , ad.ip_address
     , ad.type
     , ad.created_on
     , ad.updated_on
     , ad.created_by
     , ad.updated_by
from address ad
         join address_group ag on ag.id = ad.address_group_id
         join corp cp on cp.id = ag.corp_id 
order by ag.name, ad.updated_on desc
;


/*
drop view vw_address_group_policy
as
select agp.id
     , agp.policy_id
     , pol.name policy_name
     , agp.address_group_id
     , adg.name address_group_name
     , agp.source_transaction_id
     , agp.created_on
     , agp.updated_on
     , agp.created_by
     , agp.updated_by
from address_group_policy agp
         join policy pol on pol.id = agp.policy_id
         join address_group adg on adg.id = agp.address_group_id
order by adg.name, pol.name, agp.id
;
*/


create
or replace view vw_corp
as
select cp.id
     , cp.name
     , cp.policy_id 
     , py.name policy_name
     , cp.created_on
     , cp.updated_on
     , cp.created_by
     , cp.updated_by
from corp cp
join policy py on py.id = cp.policy_id
order by cp.name
;


create
or replace view vw_corp_policy_history
as
select cph.id
     , cph.corp_id
     , cph.policy_id
     , crp.name corp_name
     , crp.policy_name
     , cph.source_transaction_id
     , cph.created_on
     , cph.updated_on
     , cph.created_by
     , cph.updated_by
from corp_policy_history cph
         join vw_corp crp on crp.id = cph.corp_id
order by crp.name, crp.policy_name, cph.id
;




create
or replace view vw_custom_url_policy
as
select cup.id
     , cup.name
     , cup.corp_id 
     , crp.name corp_name
     , crp.policy_name
     , cup.created_on
     , cup.updated_on
     , cup.created_by
     , cup.updated_by
from custom_url_policy cup
         join vw_corp crp on crp.id = cup.corp_id
order by crp.name, crp.policy_name, cup.name
;


create
or replace view vw_source_transaction
as
select id
     , type
     , status
     , request_json
     , created_on
     , updated_on
     , created_by
     , updated_by
from source_transaction
order by id
;

create
or replace view vw_panorama_transaction
as
select id
     , type
     , status
     , created_on
     , updated_on
     , created_by
     , updated_by
from panorama_transaction
;



