package com.kajeet.filteringcomponent.stack;

import com.kajeet.filteringcomponent.common.Constant;
import software.amazon.awscdk.App;
import software.amazon.awscdk.Environment;
import software.amazon.awscdk.StackProps;

public class FilteringComponentServiceApp {

    static Environment createEnvironment(String account, String region) {
        account = (account == null) ? System.getenv("CDK_DEPLOY_ACCOUNT") : account;
        region = (region == null) ? System.getenv("CDK_DEPLOY_REGION") : region;
        account = (account == null) ? System.getenv("CDK_DEFAULT_ACCOUNT") : account;
        region = (region == null) ? System.getenv("CDK_DEFAULT_REGION") : region;

        return Environment.builder()
                .account(account)
                .region(region)
                .build();
    }

    public static void main(final String[] args) {
        App app = new App();

//        This line should be uncommented when deploying to DEV
        Environment envDev = createEnvironment("352725093332", "us-east-1");

//        This line should be uncommented when deploying to PROD
//        Environment envDev = createEnvironment("994617646846", "us-east-1");

        new FilteringComponentServiceStack(app, "FilteringComponentServiceStack", StackProps.builder()
                .stackName(Constant.STACKNAME)
                .env(envDev)
                .build());

        app.synth();
    }

}
