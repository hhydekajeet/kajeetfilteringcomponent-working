package com.kajeet.filteringcomponent.model.panorama.policy;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kajeet.filteringcomponent.model.panorama.Member;

public class Profiles {
    @JsonProperty("url-filtering")
    Member urlFiltering;

    public Member getUrlFiltering() {
        return urlFiltering;
    }

    public void setUrlFiltering(Member urlFiltering) {
        this.urlFiltering = urlFiltering;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
