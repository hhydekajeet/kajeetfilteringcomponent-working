var _ = require('lodash');
var logger = require('./logger');

function getResponseSchema(endpointSpec, resCode) {
  var schema;

  try {
    resCode = resCode.toString();
    schema = endpointSpec.responses[resCode].schema;
  } catch (err) {
    throw new Error('Could not find response schema when sanitizing null values. ' + err);
  }

  return schema;
}

function convertToSwaggerPath(path) {
  var pathStart = '';

  // If path does not start as an array, it must start as an object
  if (!_.startsWith(path, '[')) {
    pathStart = 'properties.';
  }

  // Replace "." with ".properties."
  path = _.replace(path, /\./g, '.properties.');

  // Replace "[*]" with ".items"
  path = _.replace(path, /\[[0-9]*\]/g, '.items');

  // If path starts with a ".", remove it
  if (_.startsWith(path, '.')) {
    path = path.substr(1);
  }

  // Prepend with pathStart
  path = pathStart + path;

  return path;
}

function generateValueFromSchema(path, schema) {
  var pathValue;
  var swaggerPath;
  var pathSchema;
  var SCHEMA_NOT_FOUND = 'SCHEMA_NOT_FOUND';

  // Convert the response path to a swagger path
  swaggerPath = convertToSwaggerPath(path);

  // Extract the relevant schema for the path
  pathSchema = _.get(schema, swaggerPath, SCHEMA_NOT_FOUND);

  // Throw error if a schema is not found
  if (pathSchema === SCHEMA_NOT_FOUND || !_.isObject(pathSchema)) {
    // TODO: This is a temporary workaround for letting the test continue on 500 responses.
    // This is not a good fix because it's specific to ModestoGW API's 500 error response. If 
    // this response changes or this swagger testing tool is used in a different project, it 
    // will not work and just throw the error. Perhaps just log the error instead of throwing, 
    // but need to find way of aggregating duplicates.
    if (swaggerPath === 'properties.fields') {
      pathSchema = { type: 'null' };
    } else {
      throw new Error('Could not find schema for path "' + swaggerPath + '" when sanitizing null values.');
    }
  }

  // Handle missing type in schema
  if (!pathSchema.type) {
    // If there's a properties field, assume type is object
    if (pathSchema.properties) {
      pathSchema.type = 'object';
      logger.warn('Missing type from schema for path "' + swaggerPath + '".');
    } else {
      /* Possible alternative:
      // Keep null value if type is not defined in schema
      pathSchema.type = 'null';
      logger.error('Could not resolve type from schema for path "' + swaggerPath + '" when sanitizing null values.');
      */

      // Throw error if type is not defined in schema
      throw new Error('Could not resolve type from schema for path "' + swaggerPath + '" when sanitizing null values.');
    }
  }

  // Assign new path value based on type
  switch (pathSchema.type) {
    case 'string':
      pathValue = '';
      break;
    case 'number':
      pathValue = 0;
      break;
    case 'boolean':
      pathValue = false;
      break;
    case 'object':
      pathValue = {};
      break;
    case 'array':
      pathValue = [];
      break;
    case 'null': // falls through
    default:
      // Unknown type
      pathValue = null;
  }

  // Override path value for date and date-time formats
  if (pathSchema.format) {
    if (pathSchema.format === 'date') {
      pathValue = '1970-01-01';
    } else if (pathSchema.format === 'date-time') {
      pathValue = '1970-01-01T00:00:00.000Z';
    }
  }

  // Override path value for required values in enum
  if (pathSchema.enum) {
    pathValue = pathSchema.enum[0];
  }

  return pathValue;
}

module.exports = function(resBody, resCode, endpointTest) {
  // Verify if response body is an object or an array
  if (_.isObject(resBody) || _.isArray(resBody)) {
    // Clone response body, but change any null or empty string value to a value with a valid type based on spec
    return _.cloneDeepTransform(resBody, function(val, key, resPath){
      if (_.isNull(val) || val === '') {
        logger.warn('Found null value for: ' + resPath);

        var resSchema = getResponseSchema(endpointTest.spec, resCode);
        return generateValueFromSchema(resPath, resSchema);
      }
    });
  } else { // if not, return original response body
    return resBody;
  }
};