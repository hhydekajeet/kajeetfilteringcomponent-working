package com.kajeet.filteringcomponent.model.panorama.device;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kajeet.filteringcomponent.model.panorama.Member;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Entry {
    @JsonProperty("@name")
    String name;
    @JsonProperty("@location")
    String location;
    @JsonProperty("@loc")
    String loc;
    @JsonProperty("@device-group")
    String deviceGroup;
    @JsonProperty("ip-netmask")
    String ipNetMask;
    String description;
    @JsonProperty("static")
    Member staticElement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(String deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public String getIpNetMask() {
        return ipNetMask;
    }

    public void setIpNetMask(String ipNetMask) {
        this.ipNetMask = ipNetMask;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Member getStaticElement() {
        return staticElement;
    }

    public void setStaticElement(Member staticElement) {
        this.staticElement = staticElement;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
