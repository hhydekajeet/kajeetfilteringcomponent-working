USE kfcdb;
DELIMITER $$
DROP PROCEDURE IF EXISTS PIPE_548_DDL $$

CREATE PROCEDURE PIPE_548_DDL()

BEGIN
SET @cnt = 0;
SET FOREIGN_KEY_CHECKS = 0;
drop trigger if exists trg_ip_pool_active_ins_ck;
drop trigger if exists trg_ip_pool_active_upd_ck;
drop trigger if exists trg_corp_xux_row;
drop table if exists ip_pool;
drop table if exists carrier;
drop table if exists address;
drop table if exists custom_url_policy;
drop table if exists corp_policy_history;
drop table if exists address_group;
drop table if exists source_transaction;
drop table if exists panorama_transaction;
drop table if exists cluster;
drop table if exists corp;
drop table if exists policy;
drop table if exists custom_url_policy_history;

CREATE TABLE IF NOT EXISTS carrier
(
    id         int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name       varchar(50) NOT NULL,
    created_on datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by varchar(50) NOT NULL,
    updated_by varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS cluster
(
    id                          int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name                        varchar(50) NOT NULL,
    panorama_url                varchar(50) NOT NULL,
    address_limit               int         NOT NULL,
    address_group_limit         int         NOT NULL,
    exception_limit             int         null,
    created_on                  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on                  datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by                  varchar(50) NOT NULL,
    updated_by                  varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS ip_pool
(
    id           int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    cluster_id   int         NOT NULL,
    carrier_id   int         NOT NULL,
    pool         varchar(50) NOT NULL,
    active_num   int         NOT NULL COMMENT 'Set = ip_pool.carrier_id for active, = -1*ip_pool.id for inactive',
    cidr         varchar(50) NOT NULL,
    start_ip     varchar(50) NOT NULL,
    end_ip       varchar(50) NOT NULL,
    pool_size    int         NOT NULL,
    num_assigned int         NOT NULL,
    created_on   datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on   datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50) NOT NULL,
    updated_by   varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS policy
(
    id           int          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name         varchar(50)  NOT NULL,
    palo_alto_id varchar(50)  NOT NULL,
    description  varchar(1000) NOT NULL,
    custom_filtering enum('Y', 'N') NOT NULL DEFAULT 'N' COMMENT 'Denotes whether policy allows corps to filter specific urls',
    custom_filtering_type enum('Allow', 'Deny', 'N/A') NOT NULL DEFAULT 'N/A',
    created_on   datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on   datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50)  NOT NULL,
    updated_by   varchar(50)  NOT NULL
);

CREATE TABLE IF NOT EXISTS corp
(
    id            int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          varchar(50) NOT NULL,
    policy_id     int         null,
    created_on    datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50) NOT NULL,
    updated_by    varchar(50) NOT NULL,
    custom_filtering enum('Y','N') NOT NULL DEFAULT 'N'
);

CREATE TABLE IF NOT EXISTS address_group
(
    id            int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          varchar(50) NOT NULL,
    cluster_id    int         NOT NULL,
    corp_id       int         NOT NULL,
    is_available  int         NOT NULL,
    created_on    datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50) NOT NULL,
    updated_by    varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS address
(
    id               int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    address_group_id int         NOT NULL,
    mdn              varchar(50) NOT NULL,
    imei             varchar(50) NOT NULL,
    ip_address       varchar(50) NOT NULL,
    type             varchar(50) NOT NULL,
    created_on       datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on       datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by       varchar(50) NOT NULL,
    updated_by       varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS corp_policy_history
(
    id                      int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    corp_id                 int         NOT NULL,
    policy_id               int         NOT NULL,
    source_transaction_id   varchar(100)         NOT NULL,
    created_on              datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on              datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) NOT NULL,
    updated_by              varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS custom_url_policy
(
    id                      int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    corp_id                 int         NOT NULL,
    name                    varchar(50) NOT NULL,
    custom_filtering 		enum('Y') default 'Y' not null,
    created_on              datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on              datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) NOT NULL,
    updated_by              varchar(50) NOT NULL
);


-- use kfcdb;
-- update source_transaction  set status = 'Pending' where status = 'Created';
-- commit;
-- alter table source_transaction modify column status       enum('Pending', 'Processing', 'Completed', 'Cancelled', 'Unknown') DEFAULT 'Unknown' NOT NULL;

CREATE TABLE IF NOT EXISTS source_transaction
(
    id           varchar(100)  NOT NULL primary key,
    source       varchar(50)   NOT NULL,
    type         varchar(50)   NOT NULL,
    status       enum('Created', 'Pending', 'Processing', 'Completed', 'Cancelled', 'Unknown') DEFAULT 'Unknown' NOT NULL,
    request_json varchar(1000) NOT NULL,
    created_on   datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on   datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50)   NOT NULL,
    updated_by   varchar(50)   NOT NULL
);

CREATE TABLE IF NOT EXISTS panorama_transaction
(
    id         int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type       varchar(50) NOT NULL,
    status     varchar(50) NOT NULL,
    created_on datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by varchar(50) NOT NULL,
    updated_by varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS custom_url_policy_history 
(
    id                      int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    custom_url_policy_id      int         NOT NULL,
    corp_id                 int         NOT NULL,
    corp_name               varchar(50) NOT NULL,
    policy_id               int         NOT NULL,
    policy_name             varchar(50) NOT NULL,
    policy_palo_alto_id     varchar(50),
    name                    varchar(50) NOT NULL,
    created_on              datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on              datetime    DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) NOT NULL,
    updated_by              varchar(50) NOT NULL
);


-- - START UNIQUE INDEX CREATION -------------------------------------------------------------------------------------	

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'ip_pool'
		and s.COLUMN_NAME = 'pool'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX ip_pool_ux on ip_pool (pool);
END IF;

IF NOT EXISTS
 ( SELECT * FROM 
   ( SELECT 'x' from information_schema.STATISTICS ic1
              join information_schema.STATISTICS ic2 on ic2.table_schema = ic1.table_schema              
                                                    and ic2.table_name   = ic1.table_name    
                                                    and ic2.index_name   = ic1.index_name                                                      
             where ic1.TABLE_SCHEMA = database()
               and ic1.TABLE_NAME = 'ip_pool'
               and ic1.COLUMN_NAME = 'carrier_id'
               and ic2.COLUMN_NAME = 'active_num'
               and ic1.NON_UNIQUE = 0
               and ic1.INDEX_NAME <> 'PRIMARY') INDEX_VIEW
 )
THEN
	CREATE UNIQUE INDEX ip_pool_active_ux on ip_pool (carrier_id, active_num); 
	-- The convention for an ACTIVE pool is active_num = carrier_id.
	-- Only one pool may be active at a time per carrier
	-- Inactive pools must be set active_num = -1*id (negative of ip_pool.id).
	-- Unique index enforces the business rule
	-- Active_flag field in VIEW vw_ip_pool presents friendly 'Y' or 'N'
    -- 
	-- For example
	-- ip_pool table:
	-- id:101 carrier_id:2001 active_num:-101
	-- id:102 carrier_id:2001 active_num:2001
	-- id:103 carrier_id:2001 active_num:-103
	-- 
	-- IP_POOL id 201 is the active for carrier 2001.
	-- 
	-- To change the active:
	-- update ip_pool set active_num=-1*id where carrier_id=2001;
	-- update ip_pool set active_num=2001 where id=101;
	-- 
	-- Now IP_POOL id 101 is the active for carrier 2001.
END IF;

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'carrier'
		and s.COLUMN_NAME = 'name'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX carrier_ux on carrier(name);
END IF;


IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'corp'
		and s.COLUMN_NAME = 'name'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX corp_ux on corp(name);
END IF;

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'cluster'
		and s.COLUMN_NAME = 'name'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX cluster_ux on cluster(name);
END IF;

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'address'
		and s.COLUMN_NAME = 'mdn'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX address_mdn_ux on address(mdn);
END IF;

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'address'
		and s.COLUMN_NAME = 'imei'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX address_imei_ux on address(imei);
END IF;

IF NOT EXISTS
 ( SELECT * FROM 
   ( SELECT 'x' from information_schema.STATISTICS ic1
              join information_schema.STATISTICS ic2 on ic2.table_schema = ic1.table_schema              
                                                    and ic2.table_name   = ic1.table_name    
                                                    and ic2.index_name   = ic1.index_name                                                      
             where ic1.TABLE_SCHEMA = database()
               and ic1.TABLE_NAME = 'custom_url_policy'
               and ic1.COLUMN_NAME = 'corp_id'
               and ic2.COLUMN_NAME = 'name'
               and ic1.NON_UNIQUE = 0
               and ic1.INDEX_NAME <> 'PRIMARY') INDEX_VIEW
 )
 THEN
	CREATE UNIQUE INDEX custom_url_policy_ux on custom_url_policy(corp_id, name);
END IF;

IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'address_group'
		and s.COLUMN_NAME = 'name'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX address_group_ux on address_group(name);
END IF;

-- policy_name_ux
IF NOT EXISTS
	 (SELECT *
        FROM information_schema.STATISTICS s 
		where s.TABLE_SCHEMA = database()
		and s.TABLE_NAME = 'policy'
		and s.COLUMN_NAME = 'name'
		and s.NON_UNIQUE = 0
		and s.INDEX_NAME <> 'PRIMARY')
THEN
	CREATE UNIQUE INDEX policy_name_ux on policy(name);
END IF;

IF NOT EXISTS
 ( SELECT * FROM 
   ( SELECT 'x' from information_schema.STATISTICS ic1
              join information_schema.STATISTICS ic2 on ic2.table_schema = ic1.table_schema              
                                                    and ic2.table_name   = ic1.table_name    
                                                    and ic2.index_name   = ic1.index_name                                                      
             where ic1.TABLE_SCHEMA = database()
               and ic1.TABLE_NAME = 'policy'
               and ic1.COLUMN_NAME = 'id'
               and ic2.COLUMN_NAME = 'custom_filtering'
               and ic1.NON_UNIQUE = 0
               and ic1.INDEX_NAME <> 'PRIMARY') INDEX_VIEW
 )
 THEN
	CREATE UNIQUE INDEX policy_super_ux on policy(id, custom_filtering); -- extra column in addition to already unique id creates a point of reference against which to validate
END IF;

IF NOT EXISTS
 ( SELECT * FROM 
   ( SELECT 'x' from information_schema.STATISTICS ic1
              join information_schema.STATISTICS ic2 on ic2.table_schema = ic1.table_schema              
                                                    and ic2.table_name   = ic1.table_name    
                                                    and ic2.index_name   = ic1.index_name                                                      
             where ic1.TABLE_SCHEMA = database()
               and ic1.TABLE_NAME = 'corp'
               and ic1.COLUMN_NAME = 'id'
               and ic2.COLUMN_NAME = 'custom_filtering'
               and ic1.NON_UNIQUE = 0
               and ic1.INDEX_NAME <> 'PRIMARY') INDEX_VIEW
 )
 THEN
	CREATE UNIQUE INDEX corp_super_ux on corp(id, custom_filtering);
END IF;


-- - END UNIQUE INDEX CREATION -------------------------------------------------------------------------------------	
-- - BEGIN FOREIGN KEY CREATION -------------------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

IF NOT EXISTS
	 ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'ip_pool'
		AND kcu.CONSTRAINT_NAME = 'ip_pool_cluster_fk'
		AND kcu.COLUMN_NAME = 'cluster_id'		
		AND kcu.REFERENCED_TABLE_NAME = 'cluster'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
	) 
THEN
  ALTER TABLE ip_pool
    ADD CONSTRAINT ip_pool_cluster_fk FOREIGN KEY (cluster_id) REFERENCES cluster (id);
END IF;

IF NOT EXISTS
	 ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'ip_pool'
		AND kcu.CONSTRAINT_NAME = 'ip_pool_carrier_fk'
		AND kcu.COLUMN_NAME = 'carrier_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'carrier'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
	)
THEN
  ALTER TABLE ip_pool
     ADD CONSTRAINT ip_pool_carrier_fk FOREIGN KEY (carrier_id) REFERENCES carrier (id);
END IF;

IF NOT EXISTS
  ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'address_group'
		AND kcu.CONSTRAINT_NAME = 'address_group_cluster_fk'
		AND kcu.COLUMN_NAME = 'cluster_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'cluster'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
  ) 
THEN
  ALTER TABLE address_group
    ADD CONSTRAINT address_group_cluster_fk FOREIGN KEY (cluster_id) REFERENCES cluster (id);
END IF;

IF NOT EXISTS
	 ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'address_group'
		AND kcu.CONSTRAINT_NAME = 'address_group_corp_fk'
		AND kcu.COLUMN_NAME = 'corp_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'corp'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
	 )
THEN
  ALTER TABLE address_group
    ADD CONSTRAINT address_group_corp_fk FOREIGN KEY (corp_id) REFERENCES corp (id);
END IF;

IF NOT EXISTS
	 ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'address'
		AND kcu.CONSTRAINT_NAME = 'address_address_group_fk'
		AND kcu.COLUMN_NAME = 'address_group_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'address_group'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
		)
THEN
  ALTER TABLE address
    ADD CONSTRAINT address_address_group_fk FOREIGN KEY (address_group_id) REFERENCES address_group (id);
END IF;

IF NOT EXISTS
	 ( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'custom_url_policy'
		AND kcu.CONSTRAINT_NAME = 'custom_url_policy_corp_fk'
		AND kcu.COLUMN_NAME = 'corp_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'corp'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
		AND EXISTS (SELECT 1
					FROM information_schema.KEY_COLUMN_USAGE kcu1 
					WHERE kcu.CONSTRAINT_SCHEMA = kcu1.CONSTRAINT_SCHEMA
					AND kcu.REFERENCED_TABLE_SCHEMA = kcu1.REFERENCED_TABLE_SCHEMA
					AND kcu.TABLE_NAME = kcu1.TABLE_NAME
					AND kcu.REFERENCED_TABLE_NAME = kcu1.REFERENCED_TABLE_NAME
					AND kcu1.COLUMN_NAME = 'custom_filtering' 
					AND kcu1.ORDINAL_POSITION = 2)
		)
THEN
  ALTER TABLE custom_url_policy
    ADD CONSTRAINT custom_url_policy_corp_fk FOREIGN KEY (corp_id, custom_filtering) REFERENCES corp (id, custom_filtering);
END IF;


IF NOT EXISTS
	( SELECT *
		FROM information_schema.KEY_COLUMN_USAGE kcu 
		WHERE kcu.REFERENCED_TABLE_SCHEMA = kcu.CONSTRAINT_SCHEMA
		AND kcu.CONSTRAINT_SCHEMA = database()	
		AND kcu.TABLE_NAME = 'corp'
		AND kcu.CONSTRAINT_NAME = 'corp_policy_fk'
		AND kcu.COLUMN_NAME = 'policy_id'	
		AND kcu.REFERENCED_TABLE_NAME = 'policy'
		AND kcu.REFERENCED_COLUMN_NAME = 'id'
		AND kcu.ORDINAL_POSITION = 1
		AND EXISTS (SELECT 1
					FROM information_schema.KEY_COLUMN_USAGE kcu1 
					WHERE kcu.CONSTRAINT_SCHEMA = kcu1.CONSTRAINT_SCHEMA
					AND kcu.REFERENCED_TABLE_SCHEMA = kcu1.REFERENCED_TABLE_SCHEMA
					AND kcu.TABLE_NAME = kcu1.TABLE_NAME
					AND kcu.REFERENCED_TABLE_NAME = kcu1.REFERENCED_TABLE_NAME
					AND kcu1.COLUMN_NAME = 'custom_filtering' 
					AND kcu1.ORDINAL_POSITION = 2)
		)
THEN
  ALTER TABLE corp
    ADD CONSTRAINT corp_policy_fk FOREIGN KEY (policy_id, custom_filtering) REFERENCES policy (id, custom_filtering);
END IF;

-- - END FOREIGN KEY CREATION -------------------------------------------------------------------------------------	

-- - BEGIN TRIGGER CREATION    -------------------------------------------------------------------------------------	
delimiter //
CREATE TRIGGER trg_ip_pool_active_ins_ck 
BEFORE INSERT ON ip_pool
FOR EACH ROW
BEGIN
	IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
	END IF;
END;//
delimiter ;

delimiter //
CREATE TRIGGER trg_ip_pool_active_upd_ck 
BEFORE UPDATE ON ip_pool
FOR EACH ROW
BEGIN
	IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
	END IF;
END;//
delimiter ;

delimiter //
CREATE TRIGGER trg_corp_xux_row 
BEFORE UPDATE ON corp
FOR EACH ROW
BEGIN
	IF NEW.policy_id != OLD.policy_id THEN
		set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
		insert into custom_url_policy_history (custom_url_policy_id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, created_by, updated_by)
		       select cst.id, NEW.id, NEW.name, OLD.policy_id, ply.name, ply.palo_alto_id, cst.name, 'trg_corp_xux_row', 'trg_corp_xux_row' from vw_custom_url_policy cst join policy ply on ply.id = OLD.policy_id where cst.corp_id = NEW.id;
        delete from custom_url_policy where corp_id = NEW.id;
	END IF;
END;//
delimiter ;


delimiter //
CREATE TRIGGER trg_policy_ixx_row 
BEFORE INSERT ON policy
FOR EACH ROW
BEGIN
	IF NEW.palo_alto_id is null THEN
		set NEW.palo_alto_id = NEW.name;
	END IF;
END;//
delimiter ;



-- - END TRIGGER CREATION    -------------------------------------------------------------------------------------

-- - BEGIN VIEW CREATION    -------------------------------------------------------------------------------------
create
or replace view vw_carrier
as
select id
     , name
     , created_on
     , updated_on
     , created_by
     , updated_by
from carrier
order by name;


create
or replace view vw_cluster
as
select cl.id
     , cl.name
     , cl.panorama_url
     , cl.address_limit
     , cl.address_group_limit
     , cl.exception_limit
     , (select count(1) from address ad join address_group ag on ag.id=ad.address_group_id where ag.cluster_id = cl.id) as address_count
     , (select count(1) from address_group ag where ag.cluster_id = cl.id) as address_group_count
     , cl.created_on
     , cl.updated_on
     , cl.created_by
     , cl.updated_by
from cluster cl;


create
or replace view vw_ip_pool
as
select ip.id
     , ip.cluster_id
     , ip.carrier_id
     , cl.name cluster_name
     , ca.name carrier_name
     , ip.pool
     , ip.active_num
     , if(ip.active_num=ip.carrier_id, 'Y', 'N') as active_flag
     , ip.cidr
     , ip.start_ip
     , ip.end_ip
     , ip.created_on
     , ip.updated_on
     , ip.created_by
     , ip.updated_by
     , ip.pool_size
     , ip.num_assigned
from ip_pool ip
         join cluster cl on cl.id = ip.cluster_id
         join carrier ca on ca.id = ip.carrier_id
order by cl.name, ca.name;

create
or replace view vw_policy
as
select id
     , name
     , palo_alto_id
     , description
     , custom_filtering 
     , custom_filtering_type 
     , created_on
     , updated_on
     , created_by
     , updated_by
from policy
order by name
;


create
or replace view vw_address_group
as
select ag.id
     , ag.name
     , ag.cluster_id
     , cl.name cluster_name
     , ag.corp_id
     , cp.name corp_name
     , (select count(1) from address where address_group_id=ag.id) as address_count
     , ag.is_available
     , ag.created_on
     , ag.updated_on
     , ag.created_by
     , ag.updated_by
from address_group ag
join corp cp on cp.id = ag.corp_id 
join cluster cl on cl.id = ag.cluster_id 
order by name
;


create
or replace view vw_address
as
select ad.id
     , ad.address_group_id
     , ag.name address_group_name
     , cp.name corp 
     , ad.mdn
     , ad.imei
     , ad.ip_address
     , ad.type
     , ad.created_on
     , ad.updated_on
     , ad.created_by
     , ad.updated_by
from address ad
         join address_group ag on ag.id = ad.address_group_id
         join corp cp on cp.id = ag.corp_id 
order by ag.name, ad.updated_on desc
;

create
or replace view vw_corp
as
select cp.id
     , cp.name
     , cp.policy_id 
     , py.custom_filtering 
     , py.custom_filtering_type 
     , py.name policy_name
     , py.palo_alto_id policy_palo_alto_id
     , py.description policy_description
     , cp.created_on
     , cp.updated_on
     , cp.created_by
     , cp.updated_by
from corp cp
left join policy py on py.id = cp.policy_id
order by cp.name
;

create
or replace view vw_corp_policy_history
as
select cph.id
     , cph.corp_id
     , cph.policy_id
     , crp.name corp_name
     , crp.policy_name
     , cph.source_transaction_id
     , cph.created_on
     , cph.updated_on
     , cph.created_by
     , cph.updated_by
from corp_policy_history cph
         join vw_corp crp on crp.id = cph.corp_id
order by crp.name, crp.policy_name, cph.id
;

create
or replace view vw_custom_url_policy
as
select cup.id
     , cup.name
     , crp.id corp_id 
     , crp.name corp_name
     , crp.policy_id
     , crp.policy_name
     , crp.custom_filtering as corp_custom_filtering 
     , crp.custom_filtering_type 
     , crp.policy_palo_alto_id
     , cup.created_on
     , cup.updated_on
     , cup.created_by
     , cup.updated_by
from custom_url_policy cup
         right join vw_corp crp on crp.id = cup.corp_id -- NOTE RIGHT OUTER JOIN TO GET HEADER INFO EVEN IF NO CHILDREN
         where crp.custom_filtering = 'Y'
order by crp.name, crp.policy_name, cup.name
;

create
or replace view vw_source_transaction
as
select id
     , type
     , status
     , request_json
     , created_on
     , updated_on
     , created_by
     , updated_by
from source_transaction
order by created_on desc
;

create
or replace view vw_panorama_transaction
as
select id
     , type
     , status
     , created_on
     , updated_on
     , created_by
     , updated_by
from panorama_transaction
;

create or replace view vw_pano_cluster_group_devices
as
select adr.id as address_id
, adr.mdn
, adr.imei
, adr.ip_address
, adr.type
, agr.id as address_group_id
, agr.name as address_group_name
, agr.is_available as address_group_is_available
, crp.id as corp_id
, crp.name as corp_name
, cls.id as cluster_id
, cls.name as cluster_name
, cls.panorama_url
, cls.address_limit
, cls.address_group_limit
, cls.exception_limit
from address adr
join address_group agr on agr.id = adr.address_group_id
join corp crp on crp.id = agr.corp_id
join cluster cls on cls.id =agr.cluster_id
order by cls.name, agr.name, adr.mdn;
-- - END VIEW CREATION    -------------------------------------------------------------------------------------

 
SET @cnt = 1;



END $$

CALL PIPE_548_DDL() $$
DROP PROCEDURE PIPE_548_DDL $$
DELIMITER ;

select @cnt;
show warnings;