package com.kajeet.filteringcomponent.model.panorama.customurl;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetResponse {
    @JsonProperty("@status")
    String status;
    @JsonProperty("@code")
    String code;
    Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
