package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.datadoghq.datadog_lambda_java.DDLambda;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.Address;
import com.kajeet.filteringcomponent.common.ClusterInfo;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.function.statemachine.util.PolicyUtil;
import com.kajeet.filteringcomponent.model.panorama.Member;
import com.kajeet.filteringcomponent.model.panorama.device.Entry;
import com.kajeet.filteringcomponent.model.panorama.device.GetResponse;
import com.kajeet.filteringcomponent.model.panorama.device.PutBody;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PanoramaDeviceUpdate {

    private static final Logger log = LogManager.getLogger(PanoramaDeviceUpdate.class.getName());
    private static final String GET_ADDRESS_URL = "/restapi/%s/Objects/Addresses";
    private static final String POST_ADDRESS_URL = "/restapi/%s/Objects/Addresses";
    private static final String GET_ADDRESS_GROUP_URL = "/restapi/%s/Objects/AddressGroups";
    private static final String UPDATE_ADDRESS_GROUP_URL = "/restapi/%s/Objects/AddressGroups";
    private static final String CREATE_ADDRESS_GROUP_URL = "/restapi/%s/Objects/AddressGroups";


    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("KfcPanoramaDeviceUpdate.handleRequest()");
        log.info("Device step functions started: [{}]", input.getTransactionType());

        String panoIP = input.getPanoUrl();
        String panoVersion = input.getPanoVersion();

        String apiKeyHeader = Constant.PANO_HEADER_KEY_TYPE;
        String key = input.getApiKey();

        Map<String, String> queryParams;
        ObjectMapper objectMapper = new ObjectMapper();
        RestClient rc = new RestClient(true);
        rc.getHeaders().add(apiKeyHeader, key);
        RestResponse response;
        GetResponse getResponse;

        //Get the address, if it is there, throw error
        String getAddressURL = String.format("https://" + panoIP + GET_ADDRESS_URL, panoVersion);
        String postAddressURL = String.format("https://" + panoIP + POST_ADDRESS_URL, panoVersion);
        String getAddressGroupURL = String.format("https://" + panoIP + GET_ADDRESS_GROUP_URL, panoVersion);
        String updateAddressGroupURL = String.format("https://" + panoIP + UPDATE_ADDRESS_GROUP_URL, panoVersion);
        String createAddressGroupURL = String.format("https://" + panoIP + CREATE_ADDRESS_GROUP_URL, panoVersion);

        queryParams = new HashMap<>();
        queryParams.put("location", "device-group");
        try {
            log.info("Called GET for addresses");
            //We need to make sure we are not re-adding device(s) to a cluster.  If we are, we throw an error.
            for (ClusterInfo clusterInfo: input.getDeviceInfo().getClusters() ) {
                queryParams.put("device-group", clusterInfo.getClusterName());
                response = rc.doGet(getAddressURL, queryParams);
                objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                log.debug("Serializing response [{}]", response.getMessage());
                getResponse = objectMapper.readValue(response.getMessage(), GetResponse.class);
                List<String> allAddresses = new ArrayList<>();
                for (Entry anEntry : getResponse.getResult().getEntry()) {
                    allAddresses.add(anEntry.getName());
                }
                for (Address anAddress : clusterInfo.getAddresses()) {
                    if (allAddresses.contains(anAddress.getAddressName())) {
                        //THROW ERROR!!!!!!
                    }
                }
            }
            for (ClusterInfo clusterInfo : input.getDeviceInfo().getClusters()) {
                for (Address anAddress : clusterInfo.getAddresses()) {
                    Entry putEntry = new Entry();
                    putEntry.setName(anAddress.getAddressName());
                    putEntry.setDeviceGroup(clusterInfo.getClusterName());
                    putEntry.setDescription(anAddress.getAddressDesc());
                    putEntry.setLocation("device-group");
                    putEntry.setIpNetMask(anAddress.getAddressIP());
                    PutBody putBody = new PutBody();
                    ArrayList<Entry> putEntries = new ArrayList<Entry>();
                    putEntries.add(putEntry);
                    putBody.setEntry(putEntries);
                    queryParams = new HashMap<>();
                    queryParams.put("location", "device-group");
                    queryParams.put("device-group", clusterInfo.getClusterName());
                    queryParams.put("name", anAddress.getAddressName());

                    response = rc.doPost(postAddressURL, putBody.toString(), queryParams);
                    if (response.getStatus().is2xxSuccessful()) {
                        log.info("Success -  address added [{}] for cluster [{}]", anAddress.getAddressName(), clusterInfo.getClusterName());
                        log.debug("Response [{}]", response);
                    } else {
                        throw new SystemException("Error adding address " + anAddress.getAddressName());
                    }
                    //Get the address group
                    queryParams = new HashMap<>();
                    queryParams.put("location", "device-group");
                    queryParams.put("device-group", clusterInfo.getClusterName());
                    queryParams.put("name", anAddress.getAddressGroup());

                    response = rc.doGet(getAddressGroupURL, queryParams);
                    if (response.getStatus().is2xxSuccessful()) {
                        log.info("Success -  address group [{}] found for cluster [{}]", anAddress.getAddressGroup(), clusterInfo.getClusterName());
                        log.debug("Response [{}]", response);

                        //Let's update the address group
                        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                        log.debug("Serializing response [{}]", response.getMessage());
                        getResponse = objectMapper.readValue(response.getMessage(), GetResponse.class);
                        Entry putAGEntry = getResponse.getResult().getEntry().get(0);
                        putAGEntry.getStaticElement().getMember().add(anAddress.getAddressName());

                        PutBody agPut = new PutBody();
                        ArrayList<Entry> putAGEntries = new ArrayList<>();
                        putAGEntries.add(putAGEntry);
                        agPut.setEntry(putAGEntries);
                        response = rc.doPut(updateAddressGroupURL, agPut.toString(), queryParams);
                        if (response.getStatus().is2xxSuccessful()) {
                            log.info("Success -  address [{}] added for address group [{}]  for cluster [{}]", anAddress.getAddressName(), anAddress.getAddressGroup(), clusterInfo.getClusterName());
                            log.debug("Response [{}]", getResponse);
                        } else {
                            throw new SystemException("Error adding address " + anAddress.getAddressName() + " to address group " + anAddress.getAddressGroup());
                        }
                    }
                    else if (response.getStatus().is4xxClientError()) {
                        //If the address group is 404, then add the address group
                        PutBody agPost = new PutBody();
                        Entry postAGEntry = new Entry();
                        ArrayList<String> agMemberList = new ArrayList<>();
                        agMemberList.add(anAddress.getAddressName());
                        Member agMember = new Member();
                        agMember.setMember(agMemberList);
                        postAGEntry.setStaticElement(agMember);
                        postAGEntry.setLocation("device-group");
                        postAGEntry.setLoc(clusterInfo.getClusterName());
                        postAGEntry.setName(anAddress.getAddressGroup());
                        ArrayList<Entry> postEntries = new ArrayList<>();
                        postEntries.add(postAGEntry);
                        agPost.setEntry(postEntries);

                        response = rc.doPost(createAddressGroupURL, agPost.toString(), queryParams);
                        if (response.getStatus().is2xxSuccessful()) {
                            log.info("Success -  address [{}] added for NEW address group [{}]  for cluster [{}]", anAddress.getAddressName(), anAddress.getAddressGroup(), clusterInfo.getClusterName());
                            log.debug("Response [{}]", response);
                            //Now we need to add the address group to the company's policy
                            PolicyUtil policyUtil = new PolicyUtil(panoIP, panoVersion, key);
                            com.kajeet.filteringcomponent.model.panorama.policy.Entry agEntry;
                            ArrayList<com.kajeet.filteringcomponent.model.panorama.policy.Entry> postPolicyEntries;

                            response = policyUtil.getPolicyInfo(clusterInfo.getClusterName(), clusterInfo.getCorpPolicy());
                            objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                            log.debug("Serializing response [{}]", response.getMessage());
                            com.kajeet.filteringcomponent.model.panorama.policy.GetResponse getPolicyResponse = objectMapper.readValue(response.getMessage(), com.kajeet.filteringcomponent.model.panorama.policy.GetResponse.class);

                            //Add all the address groups to the source
                            agEntry = getPolicyResponse.getResult().getEntry().get(0);
                            agEntry.getSource().getMember().add(anAddress.getAddressGroup());

                            postPolicyEntries = new ArrayList<>();
                            postPolicyEntries.add(agEntry);

                            //Let's update the policy now
                            response = policyUtil.putPolicyInfo(clusterInfo.getClusterName(), clusterInfo.getCorpPolicy(), postPolicyEntries);
                        } else {
                            throw new SystemException("Error adding address " + anAddress.getAddressName() + " to address group " + anAddress.getAddressGroup());
                        }
                    }
                    input.setStatus("SUCCESS");
                }
            }
            input.setStatus("SUCCESS");
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException jsonProcessingException) {
            jsonProcessingException.printStackTrace();
        } finally {
        // DataDog Logging:
        DDLambda ddl = new DDLambda(context);
        final Span span = GlobalTracer.get().activeSpan();
        if (span != null) {
            span.setTag(Constant.CLASS_DOT_NAME, this.getClass().getName());
            if (input != null) {
                span.setTag(Constant.TRANSACTIONID, input.getTransactionId());
                span.setTag(Constant.PANO_URL, input.getPanoUrl());
                span.setTag(Constant.SOURCE_TRANSACTION_TYPE, input.getTransactionType() != null ? input.getTransactionType().getLabel() : null);
            }
            span.setTag(Constant.ATTEMPT_DOT_NUMBER, 1);
            span.finish();
        }
    }
        return input;
    }
}