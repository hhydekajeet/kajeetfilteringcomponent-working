package com.kajeet.filteringcomponent.model.panorama.policy;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kajeet.filteringcomponent.model.panorama.Member;
import com.kajeet.filteringcomponent.model.panorama.policy.ProfileSetting;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Entry {
    @JsonProperty("@name")
    String name;
    @JsonProperty("@uuid")
    String uuid;
    @JsonProperty("@location")
    String location;
    @JsonProperty("@device-group")
    String deviceGroup;
    @JsonProperty("@loc")
    String loc;
    Member from;
    @JsonProperty("to")
    Member toMember;
    Member destination;
    Member service;
    Member application;
    String action;
    @JsonProperty("source-user")
    Member sourceUser;
    Member source;
    @JsonProperty("profile-setting")
    ProfileSetting profileSetting;
    Member category;
    @JsonProperty("hip-profiles")
    Member hipProfiles;
    @JsonProperty("log-start")
    String logStart;
    @JsonProperty("static")
    Member staticElement;
    @JsonProperty("type")
    String listType;
    @JsonProperty("list")
    Member urlList;
    Member allow;
    Member block;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(String deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public Member getFrom() {
        return from;
    }

    public void setFrom(Member from) {
        this.from = from;
    }

    public Member getToMember() {
        return toMember;
    }

    public void setToMember(Member toMember) {
        this.toMember = toMember;
    }

    public Member getDestination() {
        return destination;
    }

    public void setDestination(Member destination) {
        this.destination = destination;
    }

    public Member getService() {
        return service;
    }

    public void setService(Member service) {
        this.service = service;
    }

    public Member getApplication() {
        return application;
    }

    public void setApplication(Member application) {
        this.application = application;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Member getSourceUser() {
        return sourceUser;
    }

    public void setSourceUser(Member sourceUser) {
        this.sourceUser = sourceUser;
    }

    public Member getSource() {
        return source;
    }

    public void setSource(Member source) {
        this.source = source;
    }

    public ProfileSetting getProfileSetting() {
        return profileSetting;
    }

    public void setProfileSetting(ProfileSetting profileSetting) {
        this.profileSetting = profileSetting;
    }

    public Member getCategory() {
        return category;
    }

    public void setCategory(Member category) {
        this.category = category;
    }

    public Member getHipProfiles() {
        return hipProfiles;
    }

    public void setHipProfiles(Member hipProfiles) {
        this.hipProfiles = hipProfiles;
    }

    public String getLogStart() {
        return logStart;
    }

    public void setLogStart(String logStart) {
        this.logStart = logStart;
    }

    public Member getStaticElement() {
        return staticElement;
    }

    public void setStaticElement(Member staticElement) {
        this.staticElement = staticElement;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public Member getUrlList() {
        return urlList;
    }

    public void setUrlList(Member urlList) {
        this.urlList = urlList;
    }

    public Member getAllow() {
        return allow;
    }

    public Member getBlock() {
        return block;
    }

    public void setBlock(Member block) {
        this.block = block;
    }

    public void setAllow(Member allow) {
        this.allow = allow;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
