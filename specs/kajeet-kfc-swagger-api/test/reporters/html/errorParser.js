var _ = require('lodash');


var types = {
  ASSERTION_ERROR: {
    test: function(err) {
      return (err.name === 'AssertionError');
    },
    parse: function(err) {
      var msg = err.message.match(/^(.*)\n?/)[1];
      var assertion = { expected: err.expected, actual: err.actual };
      return new ParsedError('Assertion Error', msg, assertion, null);
    }
  },
  VALIDATION_ERROR: {
    test: function(err) {
      var re = /Response from.*failed validation/;
      return re.test(err.message);
    },
    parse: function(err) {
      var re = /(Response from.*failed validation): \[(.*)\]\s*Response:([{\[].*[}\]])/;
      var decomposedErr = err.message.match(re);
      var msg = decomposedErr[2];
      var response = decomposedErr[3];
      try {
        response = JSON.stringify( JSON.parse(response), null, 2 );
      } catch(jsonErr) {
        console.log('ReporterError: Error catched while prettifying test JSON response. \n');
        console.log(jsonErr.stack + '\n');
      }
      return new ParsedError('Validation Error', msg, null, response);
    }
  }
};


module.exports = {
  types: types,
  getType: getType,
  parse: parse
};


function getType(err) {
  var errType = _.findKey(this.types, function(type) {
    return type.test(err);
  });
  return errType || null;
}

function parse(err) {
  var typeKey = this.getType(err);
  var parsedError;

  if (typeKey) {
    parsedError = this.types[typeKey].parse(err);
  } else {
    parsedError = new ParsedError('Unknown Error', err.message, null, null);
  }

  return parsedError;
}

function ParsedError(name, message, assertion, response) {
  this.name = name;
  this.message = message;
  this.assertion = (assertion) ? { expected: assertion.expected, actual: assertion.actual } : null;
  this.response = response;
}