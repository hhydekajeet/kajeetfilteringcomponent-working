package com.kajeet.filteringcomponent.model.panorama.policy;

import com.amazonaws.util.json.Jackson;
import com.kajeet.filteringcomponent.model.panorama.policy.Entry;

import java.util.ArrayList;

public class PutBody {
    ArrayList<Entry> entry;

    public ArrayList<Entry> getEntry() {
        return entry;
    }

    public void setEntry(ArrayList<Entry> entry) {
        this.entry = entry;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
