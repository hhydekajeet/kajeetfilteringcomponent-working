package com.kajeet.filteringcomponent.common.util;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.model.CodeStatusMessage;
import com.kajeet.filteringcomponent.model.device.CorpDevices;
import com.kajeet.filteringcomponent.model.device.Devices;
import com.kajeet.filteringcomponent.model.objects.Policies;
import com.kajeet.filteringcomponent.model.objects.Policy;
import com.kajeet.filteringcomponent.model.kfc.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Response {

    private static final Logger log = LogManager.getLogger(Response.class);

    private static void setCommonAttributes (APIGatewayProxyResponseEvent response){
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        response.setHeaders(headers);
        response.setStatusCode(Integer.parseInt(Constant.SUCCESS_CODE_200));
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, Policies policies) throws IOException {
        log.debug("Response.createSuccessfulResponse(List<Policy>)");
        setCommonAttributes(response);
        response.setBody(policies.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, List<Policy> policyList) throws IOException {
        log.debug("Response.createSuccessfulResponse(List<Policy>)");
        setCommonAttributes(response);
        response.setBody(policyList.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, CustomUrlApi customUrl) throws IOException {
        log.debug("Response.createSuccessfulResponse(Policy)");
        setCommonAttributes(response);
        response.setBody(customUrl.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, Policy policy) throws IOException {
        log.debug("Response.createSuccessfulResponse(Policy)");
        setCommonAttributes(response);
        response.setBody(policy.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, CorpPolicy corpPolicy) throws IOException {
        log.debug("Response.createSuccessfulResponse(CorpPolicy)");
        setCommonAttributes(response);
        response.setBody(corpPolicy.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, CorpDevices corpDevices) throws IOException {
        log.debug("Response.createSuccessfulResponse(CorpPolicy)");
        setCommonAttributes(response);
        response.setBody(corpDevices.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, Devices devices) throws IOException {
        log.debug("Response.createSuccessfulResponse(CorpPolicy)");
        setCommonAttributes(response);
        response.setBody(devices.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, IpPoolCarrier ipPoolCarrier) throws IOException {
        log.debug("Response.createSuccessfulResponse(CorpPolicy)");
        setCommonAttributes(response);
        response.setBody(ipPoolCarrier.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, String message) throws IOException {
        log.debug("Response.createSuccessfulResponse(Message) [Result]");
        setCommonAttributes(response);
        CodeStatusMessage codeStatusMessage = new CodeStatusMessage(Constant.SUCCESS_CODE_200, Constant.SUCCESS, message);
        response.setBody(codeStatusMessage.toString());
    }

    public static void createSuccessfulResponse(APIGatewayProxyResponseEvent response, String status, String message) throws IOException {
        log.debug("Response.createSuccessfulResponse(Message) [Result]");
        setCommonAttributes(response);
        CodeStatusMessage codeStatusMessage = new CodeStatusMessage(Constant.SUCCESS_CODE_200, status, message);
        response.setBody(codeStatusMessage.toString());
    }

}

