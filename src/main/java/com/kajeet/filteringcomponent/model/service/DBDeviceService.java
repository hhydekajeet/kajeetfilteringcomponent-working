package com.kajeet.filteringcomponent.model.service;

import com.kajeet.filteringcomponent.common.ConstantSQL;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.model.device.CorpDevices;
import com.kajeet.filteringcomponent.model.device.Device;
import com.kajeet.filteringcomponent.model.device.Devices;
import com.kajeet.filteringcomponent.model.objects.AddressGroupCluster;
import com.kajeet.filteringcomponent.model.objects.OneKeyValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBDeviceService {
    private static final Logger log = LogManager.getLogger(DBDeviceService.class);

    public static Devices getDevicesByCorpId (Connection conn, String corpId) throws SystemException, InvalidInputException {
       log.debug("Getting devices for corp: {}", corpId);
       List<Device> deviceList = selectDevicesByCorpId(conn, corpId);
       Devices devices = new Devices();
       devices.setDevices(deviceList);
       return devices;
    }

    private static List<Device> selectDevicesByCorpId(Connection conn, String corpId) throws SystemException, InvalidInputException {
        String query = ConstantSQL.QUERY_DEVICES_BY_CORP_ID;
        log.debug("Query: {}", query);
        ResultSet resultSet = null;
        List<Device> deviceList = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = corpId;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            deviceList = getListFromResultSet (resultSet, new Device() );
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBDeviceService.selectDevices() threw SQLException " + e.getMessage());
        } finally {
            JdbcService.closeResultSet(resultSet);
        }
        return deviceList;
    }

    private static List<Device> getListFromResultSet (ResultSet resultSet, Device tokenDevice ) throws SQLException {
        List<Device> deviceList = new ArrayList<>();
        Device device = null;
        while (resultSet.next()) {
            device = new Device();
            device.setMdn(resultSet.getString(Device.DB_FIELD_MDN));
            device.setImei(resultSet.getString(Device.DB_FIELD_IMEI));
            device.setIpAddress(resultSet.getString(Device.DB_FIELD_IP_ADDRESS));
            device.setType(resultSet.getString(Device.DB_FIELD_TYPE));
            device.setAddressGroup(resultSet.getString(Device.DB_FIELD_ADDRESS_GROUP));
            deviceList.add(device);
        }
        return deviceList;
    }

    private static AddressGroupCluster getAddressGroupCluster (Connection conn, String ipPool, int corpId) throws SystemException, InvalidInputException {
//        private static AddressGroupCluster getAddressGroupCluster (OneKeyValue addressGroupKeyValue, OneKeyValue clusterKeyValue) {
        OneKeyValue clusterKeyValue = getClusterIdNameByIpPoolName(conn, ipPool);
        log.debug("DBDeviceService.getAddressGroupCluster() cluster_id: {}, cluster_name: {}", clusterKeyValue.getKey(), clusterKeyValue.getValue());
        OneKeyValue addressGroupKeyValue = getAddressGroupIdNameByCorpIdAndClusterId(conn, corpId, clusterKeyValue.getKey());
        log.debug("DBDeviceService.getAddressGroupCluster() address_group_id: {}, address_group_name: {}", addressGroupKeyValue.getKey(), addressGroupKeyValue.getValue());
        AddressGroupCluster addressGroupCluster = new AddressGroupCluster();
        addressGroupCluster.setAddressGroupId(addressGroupKeyValue.getKey());
        addressGroupCluster.setAddressGroupName(addressGroupKeyValue.getValue());
        addressGroupCluster.setClusterId(clusterKeyValue.getKey());
        addressGroupCluster.setClusterName(clusterKeyValue.getValue());
        return addressGroupCluster;
    }

    public static AddressGroupCluster postCorpDevices (Connection conn, CorpDevices corpDevices) throws SystemException, InvalidInputException {
        log.debug("DBDeviceService.postCorpDevices(transactionId='" + corpDevices.getTransactionId() + "')");
        // Insert into Address
/*
{
  "corpId": "string",
  "carrier": "string",
  "ipPool": "string",
  "sourceSystem": "sentinel",
  "devices": [
    {
      "mdn": "string",
      "imei": "string",
      "ipAddress": "string",
      "type": "string"
    }
  ]
}

Need address_group.id
validate carrier against ipPool
Look up corp.id
[POSTPONED] look up ip_pool.cidr from ipPool (name)
look up cluster.id from ip_pool
Find address_group.id(s) via cluster.id and corp.id where is_available=1 and pick one
[POSTPONED] Validate address against ipPool.cidr
insert into address

 */
        String carrierName = getCarrierNameByIpPoolName (conn, corpDevices.getIpPool());
        if (!(corpDevices.getCarrier().equals(carrierName))){
            throw new InvalidInputException("IP Pool " + corpDevices.getIpPool() + " does not correspond to Carrier " + corpDevices.getCarrier() + "; found " + carrierName);
        }
//[POSTPONED]        String cidr = getCidrByIpPoolName (conn, corpDevices.getIpPool());
//        validateIpAddressesPerCidr (corpDevices.getDevices(), cidr);
        int corpId = JdbcService.getIdFromName(conn, corpDevices.getCorpId(), ConstantSQL.DB_OBJECT_CORP);
        AddressGroupCluster addressGroupCluster = getAddressGroupCluster(conn, corpDevices.getIpPool(), corpId);
        insertAddresses (conn, corpDevices, addressGroupCluster.getAddressGroupId());
        return addressGroupCluster;
    }

    public static AddressGroupCluster deleteCorpDevices (Connection conn, CorpDevices corpDevices) throws SystemException, InvalidInputException {
        log.debug("DBDeviceService.deleteCorpDevices(transactionId='" + corpDevices.getTransactionId() + "')");
        // Delete from Address
/*
{
  "corpId": "string",
  "carrier": "string",
  "ipPool": "string",
  "sourceSystem": "sentinel",
  "devices": [
    {
      "mdn": "string",
      "imei": "string",
      "ipAddress": "string",
      "type": "string"
    }
  ]
}

validate carrier against ipPool
Look up corp.id
delete from into address
 */
        String carrierName = getCarrierNameByIpPoolName (conn, corpDevices.getIpPool());
        if (!(corpDevices.getCarrier().equals(carrierName))){
            throw new InvalidInputException("IP Pool " + corpDevices.getIpPool() + " does not correspond to Carrier " + corpDevices.getCarrier() + "; found " + carrierName);
        }
        int corpId = JdbcService.getIdFromName(conn, corpDevices.getCorpId(), ConstantSQL.DB_OBJECT_CORP);
        AddressGroupCluster addressGroupCluster = getAddressGroupCluster(conn, corpDevices.getIpPool(), corpId);
        int totalAddressesDeleted = deleteAddresses (conn, corpDevices, corpId);
        if (totalAddressesDeleted != corpDevices.getDevices().size()) {
            log.debug("DBDeviceServices.deleteCorpDevices attempted to delete " + corpDevices.getDevices().size() + " devices but found " + totalAddressesDeleted);
        }
        Devices devices = getDevicesByCorpId (conn, corpDevices.getCorpId());
        corpDevices.setDevices(devices.getDevices());
        return addressGroupCluster;
    }

//    public static void validateIpAddressesPerCidr (List<Device> addressList, String cidr) throws InvalidInputException {
//        // PIPE-550 THE IMPLEMENTATION OF Cidr.isValidAddressPerCidr() IS MESSY AND FRAGILE AND SHOULD BE REVIEWED AND REVISED -- H.H.
//        log.debug("DBDeviceService.validateIpAddressesPerCidr(cidr='" + cidr + "')");
//        for (Device device: addressList) {
//            try {
//              if (!Cidr.isValidAddressPerCidr(cidr, device.getIpAddress())) { // NEED TO REVIEW INTERNAL LOGIC OF Cidr.isValidAddressPerCidr()
//                  throw new InvalidInputException("IP address " + device.getIpAddress() + " is not valid for cidr " + cidr);
//              }
//            } catch (InvalidInputException x) {
//                throw new InvalidInputException ("IP address " + device.getIpAddress() + " failed validation against cidr " + cidr + ": " + x.getMessage());
//            }
//        }
//    }

    public static int insertAddresses (Connection conn, CorpDevices corpDevices, int addressGroupId) throws InvalidInputException, SystemException {
        log.debug("DBDeviceService.insertAddresses(addressGroupId='" + addressGroupId + "')");
        int totalRowsInserted = 0;
        if (conn != null) {
            String query = ConstantSQL.INSERT_ADDRESS;
            log.debug("Query: {}", query);
            String[] paramTypes = new String[7];
            String[] paramValues = new String[7];
            paramTypes[0] = JdbcService.PARAM_TYPE_INT;
            for (int x=1; x<=6; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
            paramValues[0] = ""+addressGroupId;
            int rowsAffected = 0;
            try { // NEED TO ENHANCE FOR TRANSACTION CONTROL, ROLLBACK IF ANY STATEMENTS FAIL ETC. /////////////////////
                for (Device device: corpDevices.getDevices()) {
                    paramValues[1] = device.getMdn();
                    paramValues[2] = device.getImei();
                    paramValues[3] = device.getIpAddress();
                    paramValues[4] = device.getType();
                    paramValues[5] = ConstantSQL.CREATED_BY;
                    paramValues[6] = ConstantSQL.UPDATED_BY;
                    rowsAffected = JdbcService.executeDml(conn, query, paramTypes, paramValues);
                    if (rowsAffected != 1) {
                        throw new RuntimeException("ERROR: DBDeviceService.insertAddresses() should have gotten SQL statement result of 1 but instead got " + rowsAffected);
                    }
                    totalRowsInserted++;
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
                if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_DUPLICATE)) {
                    throw new InvalidInputException(e.getMessage().substring(0, e.getMessage().indexOf(ConstantSQL.MYSQL_ERR_FOR_KEY)));
                } else {
                    throw new SystemException("ERROR: DBDeviceService.insertAddresses() threw SQLException " + e.getMessage());
                }
            }
        }
        return totalRowsInserted;
    }

    public static int deleteAddresses (Connection conn, CorpDevices corpDevices, int corpId) {
        log.debug("DBDeviceService.deleteAddresses(corpId='" + corpId + "')");
        int totalRowsDeleted = 0;
        if (conn != null) {
            String query = ConstantSQL.DELETE_ADDRESSES_BY_CORP_ID_AND_MDN;
            log.debug("Query: {}", query);
            String[] paramTypes = new String[2];
            String[] paramValues = new String[2];
            paramTypes[0] = JdbcService.PARAM_TYPE_INT;
            paramTypes[1] = JdbcService.PARAM_TYPE_STRING;
            paramValues[0] = ""+corpId;
            int rowsAffected = 0;
            try { // NEED TO ENHANCE FOR TRANSACTION CONTROL, ROLLBACK IF ANY STATEMENTS FAIL ETC? /////////////////////
                for (Device device: corpDevices.getDevices()) {
                    paramValues[1] = device.getMdn();
                    rowsAffected = JdbcService.executeDml(conn, query, paramTypes, paramValues);
                    if (rowsAffected != 1) {
                        log.debug("DBDeviceService.deleteAddresses('" + device.getMdn() + "' found no record to delete. Ignoring.");
                    }
                    totalRowsDeleted += rowsAffected;
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
                throw new RuntimeException("ERROR: DBPolicyService.putCustomUrl() threw SQLException " + e.getMessage());
            }
        }
        return totalRowsDeleted;
    }


    public static OneKeyValue getClusterIdNameByIpPoolName (Connection conn, String ipPoolName) throws SystemException, InvalidInputException
    {
        String query = ConstantSQL.QUERY_CLUSTER_ID_NAME_PER_IP_POOL;
        log.debug("Query: {}, {}", query, ipPoolName);
        String[] fieldNames = new String[2];
        fieldNames[0] = ConstantSQL.DB_CLUSTER_ID;
        fieldNames[1] = ConstantSQL.DB_CLUSTER_NAME;
        return JdbcService.getUniqueIntAndStringFieldByStringValue(conn, query, fieldNames, ipPoolName);
    }

    public static OneKeyValue getAddressGroupIdNameByCorpIdAndClusterId (Connection conn, int corpId, int clusterId) throws SystemException, InvalidInputException
    {
        String query = ConstantSQL.QUERY_AVAILABLE_ADDRESS_GROUP_BY_CORP_AND_CLUSTER;
        log.debug("Query: {}, {}, {}, ", query, corpId, clusterId);
        String[] fieldNames = new String[2];
        fieldNames[0] = ConstantSQL.ID;
        fieldNames[1] = ConstantSQL.NAME;
        String[] criteria = new String[2];
        criteria[0] = ""+corpId;
        criteria[1] = ""+clusterId;
        return JdbcService.getUniqueIntAndStringFieldByStringValues(conn, query, fieldNames, criteria);
    }

    public static int getAddressGroupIdByCorpIdAndClusterIdDEPRECATED (Connection conn, int corpId, int clusterId) throws SystemException, InvalidInputException
    {
        log.debug("DBDeviceService.getAddressGroupIdByCorpIdAndClusterId(corpId='" + corpId + "', clusterId='" + clusterId + "' )");
        String query = ConstantSQL.QUERY_AVAILABLE_ADDRESS_GROUP_BY_CORP_AND_CLUSTER;
        log.debug("Query: {}", query);

        int returnVal = ConstantSQL.MOST_UNLIKELY_ID_VAL;
        ResultSet resultSet = null;
        String[] paramTypes = new String[2];
        String[] paramValues = new String[2];
        paramTypes[0] = JdbcService.PARAM_TYPE_INT;
        paramTypes[1] = JdbcService.PARAM_TYPE_INT;
        paramValues[0] = ""+corpId;
        paramValues[1] = ""+clusterId;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            while (resultSet.next()) {
                returnVal = resultSet.getInt(ConstantSQL.ID);
                String groupName = resultSet.getString(ConstantSQL.NAME);
                log.debug("DBDeviceService.getAddressGroupIdByCorpIdAndClusterId(" +corpId + "', " + clusterId + "') found Address Group name " + groupName);
                break;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException("ERROR: DBDeviceService.getAddressGroupIdByCorpIdAndClusterId() threw SQLException " + e.getMessage());
        } finally {
            JdbcService.closeResultSet(resultSet);
        }
        if (returnVal == ConstantSQL.MOST_UNLIKELY_ID_VAL) {
            throw new InvalidInputException("No available Address Group found matching Corp and Cluster");
        }
        log.debug("DBDeviceService.getAddressGroupIdByCorpIdAndClusterId(" +corpId + "', " + clusterId + "') returning " + returnVal);
        return returnVal;
    }


    public static String getCidrByIpPoolName (Connection conn, String ipPoolName) throws SystemException, InvalidInputException
    {
        log.debug("DBDeviceService.getCidrByIpPoolName(ipPoolName='" + ipPoolName + "')");
        String carrierName = null;
        String query = ConstantSQL.QUERY_CIDR_PER_IP_POOL_NAME;
        log.debug("Query: {}", query);
        carrierName = JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.CIDR, ipPoolName);
        return carrierName;
    }

    public static String getCarrierNameByIpPoolName (Connection conn, String ipPoolName) throws SystemException, InvalidInputException
    {
        log.debug("DBDeviceService.getCarrierNameByIpPoolName(ipPoolName='" + ipPoolName + "')");
        String carrierName = null;
        String query = ConstantSQL.QUERY_CARRIER_PER_IP_POOL_NAME;
        log.debug("Query: {}", query);
        carrierName = JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.NAME, ipPoolName);
        return carrierName;
    }

}
