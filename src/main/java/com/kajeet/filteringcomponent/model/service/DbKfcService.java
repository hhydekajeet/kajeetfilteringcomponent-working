package com.kajeet.filteringcomponent.model.service;

import com.kajeet.filteringcomponent.common.ConstantSQL;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.model.TransactionStatus;
import com.kajeet.filteringcomponent.model.kfc.IpPoolCarrier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

public class DbKfcService {
    private static final Logger log = LogManager.getLogger(DbKfcService.class);

    public static TransactionStatus getTransactionStatus (Connection conn, String transactionId) throws SystemException, InvalidInputException {
        log.debug("DbKfcService.getTransactionStatus(" + transactionId + ")");
        String status = selectTransactionStatus(conn, transactionId);
        return TransactionStatus.getTransactionStatus(status);
    }

    private static String selectTransactionStatus (Connection conn, String transactionId) throws SystemException, InvalidInputException {
        log.debug("DbKfcService.selectTransactionStatus(transactionId='" + transactionId + "')");
        String status = null;
        String query = ConstantSQL.QUERY_STATUS_PER_TRANSACTION_ID;
        log.debug("Query: {}", query);
        status = JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.STATUS, transactionId);
        if (status==null) {
            log.error("Invalid/non-existent transactionId " + transactionId);
            throw new InvalidInputException("Invalid/non-existent transactionId " + transactionId);
        }
        log.debug("DbKfcService.selectTransactionStatus(transactionId='" + transactionId + "') returning " + status);
        return status;
    }

    public static IpPoolCarrier getIpPoolCarrier (Connection conn, String carrier) throws SystemException, InvalidInputException {
        log.debug("DbKfcService.getIpPool(" + carrier + ")");
        IpPoolCarrier ipPoolCarrier = new IpPoolCarrier();
        ipPoolCarrier.setCarrier(carrier);
        String pool = selectIpPoolByCarrier(conn, carrier);
        ipPoolCarrier.setIpPool(pool);
        return ipPoolCarrier;
    }

    private static String selectIpPoolByCarrier (Connection conn, String carrier) throws SystemException, InvalidInputException {
        log.debug("DbKfcService.selectIpPoolByCarrier(carrier='" + carrier + "')");
        String pool = null;
        String query = ConstantSQL.QUERY_IP_POOL_PER_CARRIER;
        log.debug("Query: {}", query);
        pool = JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.POOL, carrier);
        if (pool==null) {
            throw new InvalidInputException("No pool matches carrier " + carrier);
        }
        log.debug("DbKfcService.selectIpPoolByCarrier(carrier='" + carrier + "') returning " + pool);
        return pool;
    }




}
