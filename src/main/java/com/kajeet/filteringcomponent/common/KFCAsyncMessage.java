package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;
import com.kajeet.filteringcomponent.model.SourceTransactionType;

import java.util.ArrayList;

public class KFCAsyncMessage {

    private String transactionId;
    private SourceTransactionType transactionType;
    private PolicyInfo policyInfo;
    private DeviceInfo deviceInfo;
    private CustomUrlInfo customUrlInfo;

    //Pano info
    private String panoUrl;
    private String panoVersion;
    private String apiKey;
    private ArrayList<String> clustersToCommit;

    //Info for stepfunction(s)
    private String status;
    private String pendingType;
    private ArrayList<String> jobsToPoll;
    private ArrayList<String> jobsInError;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public SourceTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(SourceTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public PolicyInfo getPolicyInfo() {
        return policyInfo;
    }

    public void setPolicyInfo(PolicyInfo policyInfo) {
        this.policyInfo = policyInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public CustomUrlInfo getCustomUrlInfo() {
        return customUrlInfo;
    }

    public void setCustomUrlInfo(CustomUrlInfo customUrlInfo) {
        this.customUrlInfo = customUrlInfo;
    }

    public String getPanoUrl() {
        return panoUrl;
    }

    public void setPanoUrl(String panoUrl) {
        this.panoUrl = panoUrl;
    }

    public String getPanoVersion() {
        return panoVersion;
    }

    public void setPanoVersion(String panoVersion) {
        this.panoVersion = panoVersion;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public ArrayList<String> getClustersToCommit() {
        return clustersToCommit;
    }

    public void setClustersToCommit(ArrayList<String> clustersToCommit) {
        this.clustersToCommit = clustersToCommit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPendingType() {
        return pendingType;
    }

    public void setPendingType(String pendingType) {
        this.pendingType = pendingType;
    }

    public ArrayList<String> getJobsToPoll() {
        return jobsToPoll;
    }

    public void setJobsToPoll(ArrayList<String> jobsToPoll) {
        this.jobsToPoll = jobsToPoll;
    }

    public ArrayList<String> getJobsInError() {
        return jobsInError;
    }

    public void setJobsInError(ArrayList<String> jobsInError) {
        this.jobsInError = jobsInError;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }
}
