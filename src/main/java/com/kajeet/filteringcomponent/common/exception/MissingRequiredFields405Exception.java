package com.kajeet.filteringcomponent.common.exception;

import java.util.ArrayList;
import java.util.List;

public class MissingRequiredFields405Exception extends Exception {

  private static final long serialVersionUID = -916016762492026789L;

  private int code = 405;
  private List<String> fields = null;

  public MissingRequiredFields405Exception(String message) {
    super(message);
    this.fields = new ArrayList<String>();
  }

  public MissingRequiredFields405Exception(String message, List<String> fields) {
    super(message);
    this.fields = fields;
  }

  public MissingRequiredFields405Exception(String message, int code) {
    super(message);
    this.code = code;
  }
  public int getCode() { return code; }

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public String getMessageWithFields() {
    return fields != null && !fields.isEmpty()
      ? getMessage() + " " + String.join(", ", fields)
      : getMessage();
  }
}
