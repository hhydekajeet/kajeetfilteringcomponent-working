package com.kajeet.filteringcomponent.dbmodel;

public class WCustomUrl {
    public static final String NAME = "name";
    public static final String CORP_NAME = "corp_name";
    public static final String POLICY_NAME = "policy_name";
    public static final String URL_FILTER_NAME = "url_filter_name";
    public static final String URL_LIST_NAME = "url_list_name";
    public static final String SITE_ACCESS_TYPE_SHORT_NAME = "site_access_type_short_name";
    public static final String SITE_ACCESS_TYPE_LONG_NAME = "site_access_type_long_name";

    private String name;
    private String corpName;
    private String policyName;
    private String urlFilterName;
    private String urlListName;
    private String siteAccessTypeShortName;
    private String siteAccessTypeLongName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getUrlFilterName() {
        return urlFilterName;
    }

    public void setUrlFilterName(String urlFilterName) {
        this.urlFilterName = urlFilterName;
    }

    public String getUrlListName() {
        return urlListName;
    }

    public void setUrlListName(String urlListName) {
        this.urlListName = urlListName;
    }

    public String getSiteAccessTypeShortName() {
        return siteAccessTypeShortName;
    }

    public void setSiteAccessTypeShortName(String siteAccessTypeShortName) {
        this.siteAccessTypeShortName = siteAccessTypeShortName;
    }

    public String getSiteAccessTypeLongName() {
        return siteAccessTypeLongName;
    }

    public void setSiteAccessTypeLongName(String siteAccessTypeLongName) {
        this.siteAccessTypeLongName = siteAccessTypeLongName;
    }
}
