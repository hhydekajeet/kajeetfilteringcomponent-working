package com.kajeet.filteringcomponent.model.panorama.customurl;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kajeet.filteringcomponent.model.panorama.Member;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Entry {
    @JsonProperty("@name")
    String name;
    @JsonProperty("@uuid")
    String uuid;
    @JsonProperty("@location")
    String location;
    @JsonProperty("@device-group")
    String deviceGroup;
    @JsonProperty("@loc")
    String loc;
    @JsonProperty("list")
    Member urlList;
    @JsonProperty("type")
    String listType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(String deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public Member getUrlList() {
        return urlList;
    }

    public void setUrlList(Member urlList) {
        this.urlList = urlList;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
