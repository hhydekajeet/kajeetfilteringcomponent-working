#Test AO
#GET /status/{transaction_id}
# Straight
select * from vw_source_transaction where id = :transaction_id;


#GET /devices/{corp_id}
select * from vw_address where sentinel_corp = :corp_id;
/*
     join address_group ag on ag.id = ad.address_group_id
     join corp cp on cp.id = ag.corp_id 

{
  "devices": [
    {
      "mdn": "string",
      "imei": "string",
      "ipAddress": "string",
      "type": "string"
    }
  ]
}
*/



#GET /policy/customurl/{corp_id}
# REVIEW/CLARIFICATION REQUIRED


#GET /policy/{corp_id}
# UNLESS THIS IS ANOTHER CASE OF RANDOM "LIMIT 1", THE DATA MODEL AND ITS CARDINALITIES DOES NOT SUPPORT THIS.


#GET /policies
select * from vw_policy;


# PUT /policy
# NOT SUPPORTED BY THE DATA MODEL


# POST /policy
# NOT SUPPORTED BY THE DATA MODEL


# DELETE /policy/{corp_id}
# NOT SUPPORTED BY THE DATA MODEL

# POST /devices


# Because insert into vw_ip_pool uses hard-coded values, this script only works immediately after re-running the create table DDL script.
use kfcdb;
delete from ip_pool;
delete from cluster;
delete from carrier;

insert into cluster (name, panorama_url, address_limit, address_group_limit, created_by, updated_by) values ('Cluster_1', 'www.panorama_url.com', 2500, 10, 'Hyde-Dev', 'Hyde-Dev');
insert into cluster (name, panorama_url, address_limit, address_group_limit, created_by, updated_by) values ('Cluster_2', 'www.panorama_url2.com', 1760, 3, 'Hyde-Dev', 'Hyde-Dev');
insert into cluster (name, panorama_url, address_limit, address_group_limit, created_by, updated_by) values ('Redwood', '10.101.17.202', 2500, 2500, 'Hyde', 'Saini');

update ip_pool set cluster_id = 6 where id = 2;
commit;


insert into vw_carrier (name, created_by, updated_by) values ("Carrier1", "Howard", "Howard's Evil Twin");
insert into vw_carrier (name, created_by, updated_by) values ("Carrier2", "Alan", "Alan's brother");
insert into vw_carrier (name, created_by, updated_by) values ("Carrier3", "Hyde", "Dr. Jeckyl");
update carrier set name = 'Carrier1' WHERe id = 2;
update carrier set name = 'ATT' WHERe id = 3;
commit;

select * from vw_carrier;
select * from vw_cluster;
select * from vw_ip_pool vip ;



delete from cluster;
insert into cluster (name, panorama_url, address_limit, address_group_limit, exception_limit, created_by, updated_by) 
       values ("Cluster1", "panorama_url", 101, 201, 301, "Sigourney", "Digit");
insert into cluster (name, panorama_url, address_limit, address_group_limit, exception_limit, created_by, updated_by) 
       values ("Cluster2", "www.panorama_url.com", 2101, 2201, 2301, "Weaver", "Gorilla");
insert into cluster (name, panorama_url, address_limit, address_group_limit, exception_limit, created_by, updated_by) 
       values ("Cluster3", "http://www.panorama_url.com", 3101, 3201, 3301, "Goodall", "Jane");
#rollback;
commit;
      
update cluster set created_by = 'Jane', updated_by='Goodall' where id = 3;


update corp set custom_filtering = 'N';


select * from corp;
select * from vw_corp;
select * from vw_policy;
select * from vw_url_filter;
select * from vw_url_list;
select * from vw_custom_url;


select * from cluster;
select * from vw_cluster;
select * from vw_carrier;
select * from vw_ip_pool;
select * from vw_source_transaction vst ;
select * from vw_address;
select * from vw_address_group;

select * from vw_address;
update source_transaction set status = 'Cancelled' where id = 'sentinel.37c648a64c573e03bf9f6c5f59ad2ec2.20220224201250';
commit;


insert into ip_pool (cluster_id, carrier_id, pool, cidr, start_ip, end_ip, created_by, updated_by, pool_size, num_assigned) 
       values (2, 3, "Pool1", "10.10.10.0/24", "10.10.10.0", "10.10.10.255", "Frankenstein", "Monster", 42, 10001);
insert into ip_pool (cluster_id, carrier_id, pool, cidr, start_ip, end_ip, created_by, updated_by, pool_size, num_assigned) 
       values (2, 2, "Pool2", "11.1.11.0/24", "11.11.11.0", "11.11.11.255", "Hermann", "Munster", 420, 100010);
insert into ip_pool (cluster_id, carrier_id, pool, cidr, start_ip, end_ip, created_by, updated_by, pool_size, num_assigned) 
       values (2, 1, "Pool3", "22.22.22.0/24", "22.22.22.0", "22.22.22.255", "Gomez", "Addams", 402, 1000100);
commit;
select * from vw_ip_pool vip;

      
#INsert into address_group and address, then query vw_cluster
      
rollback;
alter TABLE address modify column mdn varchar(200);
    
select cluster_name, name as address_group_name from vw_address_group where corp_name = 'HOWARD_TEST_CORP_1' order by cluster_name, address_group_name


	public static final String QUERY_ALL_POLICIES           = "select * from policy";
//	public static final String QUERY_CUSTOM_URL_BY_CORP_ID  = "select name as url, corp_name as corpId, policy_name as type from vw_custom_url_policy cup where corp_name = ?";
	public static final String QUERY_CUSTOM_URL_BY_CORP_ID  = "select id, name, corp_id, corp_name, policy_name from vw_custom_url_policy where corp_name = ?";
	public static final String QUERY_POLICY_BY_CORP_ID      = "select * from vw_corp where name = ?";
   	public static final String QUERY_DEVICES_BY_CORP_ID     = "select mdn, imei, ip_address, type from vw_address where corp = ?";
	public static final String INSERT_CUSTOM_URL_BY_CORP_ID = "insert into custom_url_policy (corp_id, name) values ( (select corp_id from vw_corp where name = ?), ?)";


select * from source_transaction st 

select * from vw_address
select * from vw_address_group;

use kfcdb;
select version();

select * from corp;


select * from vw_cluster;
select * from vw_carrier;
select * from vw_address_group;
select * from ip_pool;
select * from vw_ip_pool vip ;
select * from vw_custom_url_policy;
select * from vw_policy;
select * from vw_corp;
select * from custom_url_policy;
select * from vw_custom_url_policy order by corp_name, name ;
select * from custom_url_policy_history;
select * from vw_corp;
select * from vw_address;
select * from address_group;
select * from vw_corp where name = 'SAINI_TEST_CORP_1'
select name as url, corp_name as corpId, policy_name as type from vw_custom_url_policy cup where corp_name = 'SAINI_TEST_CORP_1'
select id, name, corp_id, corp_name, policy_name from vw_custom_url_policy where corp_name = ?
select mdn, imei, ip_address, type from vw_address where corp = 'SAINI_TEST_CORP_1'
	
insert into custom_url_policy_history (custom_url_policy_id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, created_by, updated_by) 
select id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, 'filteringcomponent', 'filteringcomponent' from vw_custom_url_policy where id is not null and corp_name = 'Howard_Test_Corp_2';


update ip_pool set active_num = carrier_id;
commit;

select vip.*, crp.name corp_name 
  from vw_ip_pool vip
  join cluster cls on cls.id = vip.cluster_id 
  join address_group grp on grp.cluster_id = cls.id 
  join corp crp on crp.id = grp.corp_id 
  

update corp set policy_id = 4, custom_filtering = 'Y' where name = 'HOWARD_TEST_CORP_2';
rollback;


insert into custom_url_policy (corp_id, custom_filtering, name, created_by, updated_by) values ( (select id from vw_corp where name = 'Howard_Test_Corp_2'), 'Y', 'www.howard.com', 'CREATED_BY', 'UPDATED_BY');

select * from vw_corp;
insert into custom_url_policy (corp_id, custom_filtering, name, created_by, updated_by) values ( 5, 'Y', 'www.howard.com', 'CREATED_BY', 'UPDATED_BY');

delete from custom_url_policy where id > 62;
commit;

select * from vw_custom_url_policy;
select * from vw_address_group where corp_id = 4 and cluster_id = 2 and is_available = 1 LIMIT 1;

rollback;


select cluster_id, cluster_name from vw_ip_pool

select * from vw_pano_cluster_group_devices;
select * from vw_address;
select * from address;

delete from address where id = 7
commit;


insert into custom_url_policy (corp_id, name, created_by, updated_by) values ( (select id from vw_corp where name = ?), ?, 'kfc_app', 'kfc_app');


insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by) values (9
, '''; select ''cluster_name'' as cluster_name, ''address_group_name'' as address_group_name from (select * from address'
, 'myImei', '123.123.123.1', 'No Type?', 'HhydeTest?', 'HowardTest');





INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_1', (select id from vw_corp where name = 'Howard_TEST_CORP_1'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_2', (select id from vw_corp where name = 'Howard_TEST_CORP_2'), (select id from cluster where name = 'Cluster2'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_3', (select id from vw_corp where name = 'Howard_TEST_CORP_1'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_4', (select id from vw_corp where name = 'Howard_TEST_CORP_1'), (select id from cluster where name = 'Cluster2'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_5', (select id from vw_corp where name = 'Howard_TEST_CORP_1'), (select id from cluster where name = 'Cluster_2'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)  
    values ('QA_CORP_1-Redwood-001', (select id from vw_corp where name = 'QA_CORP_1'), (select id from cluster where name = 'Redwood'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)  
    values ('QA_CORP_2-Redwood-001', (select id from vw_corp where name = 'QA_CORP_2'), (select id from cluster where name = 'Redwood'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)  
    values ('QA_CORP_3-Redwood-001', (select id from vw_corp where name = 'QA_CORP_3'), (select id from cluster where name = 'Redwood'), 1, 'init', 'init');

commit;

select * from vw_address_group;

       select '' +crp.name + '-' + cls.name + '-001' as name, crp.id, cls.id, 'Y', 'Howard', 'Sarat'
         from corp crp, cluster cls;

       select crp.id, cls.id, 'Y', 'Howard', 'Sarat'
         from corp crp, cluster cls;


rollback;
insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by)
       values ((select id from address_group where name='Dummy_Address_Group_5'), 'mdn123', 'imei123', '10.1.2.3', 'Type unknown', 'dev', 'dev');
insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by)
       values ((select id from address_group where name='Dummy_Address_Group_5'), 'mdn456', 'imei456', '10.4.5.6', 'Type unknown', 'dev', 'dev');
commit;


select * from vw_address_group vag 

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_2', (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_4', (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_5', (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_3', (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), (select id from cluster where name = 'Cluster2'), 1, 'init', 'init');

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('Dummy_Address_Group_6', (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), (select id from cluster where name = 'Cluster2'), 1, 'init', 'init');

commit;


insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by)
       values ((select id from address_group where name='Dummy_Address_Group_2'), 'mdn321', 'imei321', '10.3.2.1', 'Type NOT known', 'dev1', 'dev1');
insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by)
       values ((select id from address_group where name='Dummy_Address_Group_2'), 'mdn654', 'imei654', '10.6.5.4', 'Type NOT known', 'dev1', 'dev1');


SAINI_TEST_CORP_1


INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering) 
VALUES ('Allow All', 'Kajeet_Allow', 'Kajeet Allow All Filtering Policy', 'init', 'init', 'Y');

delete from corp;
INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('HOWARD_TEST_CORP_1', (select id from policy where name='Open'), 'Howard', 'Howard');

INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('HOWARD_TEST_CORP_2', (select id from policy where name='Deny All'), 'Howard', 'Howard');

INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('DEV_Testing', (select id from policy where name='Allow All'), 'Howard', 'Howard');

INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('QA_CORP_1', (select id from policy where name='Minimal'), 'Howard', 'Sarat');

INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('QA_CORP_2', (select id from policy where name='Moderate'), 'Howard', 'Sarat');

INSERT INTO corp (name, policy_id , created_by, updated_by)
VALUES ('QA_CORP_3', (select id from policy where name='Strict'), 'Howard', 'Sarat');

commit;

select * from vw_policy 
select * from vw_corp vc ;
select * from vw_cluster;
select * from vw_custom_url_policy vc;
select * from custom_url_policy vc; 

insert into custom_url_policy (corp_id, name"... = "insert into custom_url_policy (co...d_by) values ( (select id from vw_corp where name = ?), ?, , created_by, updated_by)"

insert into custom_url_policy (corp_id, name"... = "insert into custom_url_policy (co...d_by) values ( (select id from vw_corp where name = ?), ?, , created_by, updated_by)"

insert into custom_url_policy (corp_id, name, custom_filtering, created_by, updated_by) values ( (select id from vw_corp where name = 'HOWARD_TEST_CORP_1'), 'Y', 'www.blah.com', 'HowardTesting', 'HowardTesting');
commit;

 + CREATED_UPDATED_VALUE_LIST + ")" = "insert into custom_url_policy (co...dated_by) values ( (select id from vw_corp where name = ?), ?, 'kfc_app', 'kfc_app')"


--  IMPLEMENTED in Dev 2022.02.28

CREATE UNIQUE INDEX policy_super_ux on policy(id, custom_filtering); -- extra column in addition to already unique id creates a point of reference against which to validate
alter table corp add custom_filtering enum('Y', 'N');
update corp set custom_filtering=(select custom_filtering from policy where id = corp.policy_id);
commit;
CREATE UNIQUE INDEX corp_super_ux on corp(id, custom_filtering);
ALTER TABLE corp drop CONSTRAINT corp_policy_fk;
ALTER TABLE corp ADD CONSTRAINT corp_policy_fk FOREIGN KEY (policy_id, custom_filtering) REFERENCES policy (id, custom_filtering);
alter table custom_url_policy add custom_filtering enum('Y'); -- ONLY YES IS PERMITTED. BUSINESS RULE ENFORCEMENT
ALTER TABLE custom_url_policy drop CONSTRAINT custom_url_policy_corp_fk;
ALTER TABLE custom_url_policy ADD CONSTRAINT custom_url_policy_corp_fk FOREIGN KEY (corp_id, custom_filtering) REFERENCES corp (id, custom_filtering);


drop trigger if exists trg_corp_xux_row;
delimiter //
CREATE TRIGGER trg_corp_xux_row 
BEFORE UPDATE ON corp
FOR EACH ROW
BEGIN
	IF NEW.policy_id != OLD.policy_id THEN
		set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
        delete from custom_url_policy where corp_id = NEW.id;
	END IF;
END;//
delimiter ;





-- drop trigger trg_corp_xux_row;
-- create trigger trg_corp_xux_row 
--        before update on corp
--        for each row
-- BEGIN
--   set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
-- END;
-- 
-- 
-- create trigger trg_corp_xux_row 
--        before update on corp
--        for each row
-- set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
-- 
-- 
-- 
-- 
--       set NEW.custom_filtering='N';
-- 
-- 
-- drop trigger trg_corp_xux_row;
-- create trigger trg_corp_xux_row 
--        before update on corp
--        for each row
-- begin
--  	if NEW.policy_id != OLD.policy_id THEN
--       set NEW.custom_filtering='N';
--  	end if;
-- end;
-- $$


-- 	  set custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);


drop trigger trg_corp_xux_row;



-- CREATE TRIGGER ins_sum BEFORE INSERT ON account
--        FOR EACH ROW SET @sum = @sum + NEW.amount;



select COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_COLUMN_NAME, REFERENCED_TABLE_NAME
from information_schema.KEY_COLUMN_USAGE
where TABLE_NAME = 'corp';



SELECT *
FROM information_schema.table_constraints
WHERE table_schema = schema()
AND table_name = 'ip_pool';


SELECT *
FROM information_schema.column_constraints
WHERE table_schema = schema()
AND table_name = 'ip_pool';


select distinct constraint_type from information_schema.table_constraints;
-- alter table ip_pool add constraint ip_pool_active_ck check (active_num=carrier_id or active_num=-1*id);

select version()


select * from vw_ip_pool;

update ip_pool set active_num = 12345 where id=1;
commit;
rollback;


drop trigger if exists trg_ip_pool_active_ins_ck;
delimiter //
CREATE TRIGGER trg_ip_pool_active_ins_ck 
BEFORE INSERT ON ip_pool
FOR EACH ROW
BEGIN
	IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
	END IF;
END;//
delimiter ;

drop trigger if exists trg_ip_pool_active_upd_ck;
delimiter //
CREATE TRIGGER trg_ip_pool_active_upd_ck 
BEFORE INSERT ON ip_pool
FOR EACH ROW
BEGIN
	IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
	END IF;
END;//
delimiter ;





-- 		ORACLE: RAISE_APPLICATION_ERROR (-20010, 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id');
-- xxxxxALTER TABLE custom_url_policy ADD CONSTRAINT custom_url_policy_corp_ck CHECK (custom_filtering='Y');




select name from vw_carrier where id = (select carrier_id from vw_ip_pool where pool = 'Pool1')




"corpId": "DEV_Testing",
"carrier": "C673885",

insert into vw_carrier (name, created_by, updated_by) values ('C673885', 'Howard', 'JamesBond');
commit;

INSERT INTO address_group (name, corp_id, cluster_id, is_available, created_by, updated_by)
VALUES ('DEV_Testing_Address_Group_1', (select id from vw_corp where name = 'DEV_Testing'), (select id from cluster where name = 'Cluster1'), 1, 'init', 'init');

commit;

use kfcdb;
select * from vw_cluster
select * from cluster
select * from vw_carrier

insert into vw_ip_pool (cluster_id, carrier_id, pool, cidr, start_ip, end_ip, created_by, updated_by, pool_size, num_assigned) 
       values (1, 4, "SomeMockIpPool", "11.12.14.0/24", "11.12.14.0", "11.12.14.127", "Frankenstein", "Monster", 128, 1001);



"ipPool": "SomeMockIpPool",
"sourceSystem": "sentinel",



select I1.index_owner, I1.index_name, I1.table_name, I1.column_name
  from dba_indexes 
 where index_owner = 'KFCDB'


SELECT * FROM 
 ( SELECT 'x' from information_schema.STATISTICS ic1
 ( SELECT ic1.table_schema
        , ic1.table_name
        , ic1.index_name
        , ic1.COLUMN_NAME column1
        , ic2.COLUMN_NAME column2
 from information_schema.STATISTICS ic1
              join information_schema.STATISTICS ic2 on ic2.table_schema = ic1.table_schema              
                                                    and ic2.table_name   = ic1.table_name    
                                                    and ic2.index_name   = ic1.index_name                                                      
             where ic1.TABLE_SCHEMA = 'kfcdb'
               and ic1.TABLE_NAME = 'ip_pool'
               and ic1.COLUMN_NAME = 'carrier_id'
               and ic2.COLUMN_NAME = 'active_num'
               and ic1.NON_UNIQUE = 0
               and ic1.INDEX_NAME <> 'PRIMARY')
 )





select policy_name from vw_corp where name = 'HOWARD_TEST_CORP_2'


select *


select count(*)  from vw_url_list 
where url_filter_name = ''





