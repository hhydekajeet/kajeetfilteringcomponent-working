package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;

public class Address {
    private String addressName;
    private String addressIP;
    private String addressDesc;
    private String addressGroup;

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressIP() {
        return addressIP;
    }

    public void setAddressIP(String addressIP) {
        this.addressIP = addressIP;
    }

    public String getAddressDesc() {
        return addressDesc;
    }

    public void setAddressDesc(String addressDesc) {
        this.addressDesc = addressDesc;
    }

    public String getAddressGroup() {
        return addressGroup;
    }

    public void setAddressGroup(String addressGroup) {
        this.addressGroup = addressGroup;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
