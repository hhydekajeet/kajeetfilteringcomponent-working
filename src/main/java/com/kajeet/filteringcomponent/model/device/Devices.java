package com.kajeet.filteringcomponent.model.device;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class Devices {
    private List<Device> devices;

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }
}
