package com.kajeet.filteringcomponent.cidr;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kajeet.filteringcomponent.common.exception.InvalidInputException;

public class Cidr {
	
	private static final Logger log = (Logger) LogManager.getLogger(Cidr.class);

	public static String PERMIT_IP = "permit ip";
	public static String HOST = "host";
	public static char CHAR_ONE = '1';
	public static char CHAR_COMMA = ',';
	public static char CHAR_PERIOD = '.';
	public static char CHAR_SPACE = ' ';
	public static String STRING_SPACE = " ";
	public static String STRING_COMMA = ",";
	public static String STRING_PERIOD = ".";
	public static String COLON_SPACE = ": ";
	public static int THREE = 3;
	public static int IP_ADDRESS_FIELDS = 4;
	public static int IP_ADDRESS_FIELD_LOWER_BOUND = 0;
	public static int IP_ADDRESS_FIELD_UPPER_BOUND = 255;
	public static String ZERO = "0";
	public static String NOTHING = "";
	public static char SLASH = '/';
	public static int MAX_BINARY_LENGTH = 32;
	public static String MESSAGE_BAD_IP_ADDRESS = "Bad IP Address";
	public static String MESSAGE_BAD_SUBNET_MASK = "Bad subnet mask";
	public static String MESSAGE_BAD_ACL_ID = "Bad ACL ID";
	public static String MESSAGE_BAD_CIDR_IP = "Bad CIDR IP address; Expected {x.x.x.x/yy}, got ";
	public static String REGEX_VALID_ACL_ID = "[a-zA-Z_-][a-zA-Z0-9_-]*";
	public static int MAX_ACL_LENGTH = 64;
	public static final String ANY = "any";
	public static final String ANY_WILDCARD = "0.0.0.0 255.255.255.255";
	
	
	  public static boolean isValidAclId (String aclId) {
	  	if (aclId.length() > MAX_ACL_LENGTH) return false;
		  log.debug("isValidAclId("+aclId+")");
		  //String regex = "^[a-zA-Z0-9-]+$";
	      Pattern pattern = Pattern.compile(REGEX_VALID_ACL_ID);
	      Matcher matcher = pattern.matcher(aclId);
		  log.debug(matcher.matches());
		  return matcher.matches();
	  }
	  

	
	// Does the opposite of getInverseSubnetMask; Takes as subnet mask and returns a number between 0 AND 32;
	public static int getYyFromInverseSubnetMask (String subnetMask) {
		log.debug(String.format("Cidr.getYyFromInverseSubnetMask %s", subnetMask));
		int yy = 0;
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] fields = mask.split(STRING_COMMA);
		String ones = "";
		for (String field: fields) {
			ones = Integer.toBinaryString(Integer.valueOf(field)).replace(ZERO, NOTHING);
			yy += ones.length();
		}
		return MAX_BINARY_LENGTH-yy;
	}
	
	
	
	public static String getInverseSubnetMask(String ipAddress) throws NumberFormatException, InvalidInputException {
		log.debug(String.format("Cidr.getInverseSubnetMask %s", ipAddress));
		//System.out.println ("ipAddress = " + ipAddress);
		byte yy = 0;
		try {
			yy = Byte.valueOf(ipAddress.substring(ipAddress.indexOf(SLASH)+1));
		} catch (NumberFormatException x) {
			throw new InvalidInputException(MESSAGE_BAD_CIDR_IP + ipAddress);
		}
		
		if (yy <0 ||yy>MAX_BINARY_LENGTH) throw new InvalidInputException("Value error! /yy value must be between 0 and 32");
		StringBuilder binaryMask = new StringBuilder();
		StringBuilder mask = new StringBuilder();
		for (byte i = 0; i<yy; i++) {
			if (i>0 && i%8==0) binaryMask.append(CHAR_COMMA);
			binaryMask.append('0');
		}
		for (byte z = yy; z<MAX_BINARY_LENGTH; z++) {
			if (z>0 && z%8==0) binaryMask.append(CHAR_COMMA);
			binaryMask.append(CHAR_ONE);
		}
		String[] fields = binaryMask.toString().split(STRING_COMMA);
		Integer[] intFields = new Integer[4]; //fields.length];
		
		for (int i=0; i<fields.length; i++) {
			intFields[i] = Integer.parseInt(fields[i], 2);
		}
		
		int countDots = 0;
		for (Integer intField: intFields) {
			countDots++;
			mask.append(intField);
			if (countDots<4) mask.append(CHAR_PERIOD);
		}
		return mask.toString().replaceAll(STRING_COMMA, STRING_PERIOD);
	}
	
	public static boolean validateIpAddressFormat (String ipAddress) throws NumberFormatException {
		log.debug(String.format("Cidr.validateIpAddressFormat %s", ipAddress));
		String address = ipAddress.replace (STRING_PERIOD, STRING_COMMA);
		String[] addressFields = address.split(STRING_COMMA);
		if (addressFields.length != IP_ADDRESS_FIELDS) return false;
		for (String field: addressFields) {
			if (Integer.valueOf(field) < IP_ADDRESS_FIELD_LOWER_BOUND || Integer.valueOf(field) > IP_ADDRESS_FIELD_UPPER_BOUND) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidSubnetMask (String subnetMask) throws InvalidInputException {
		log.debug(String.format("Cidr.isValidSubnetMask %s", subnetMask));
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] fields = mask.split(STRING_COMMA);
		if (fields.length != 4) return false; 
		int fieldValue = 0;
		for (String field: fields) {
			if (fieldValue>255 || fieldValue<0) return false;
		}		
	  return true;
	}

	public static boolean isValidIpAddress (String ipAddress, String subnetMask) throws InvalidInputException {
		  // PIPE-550 THE IMPLEMENTATION OF THIS METHOD IS MESSY AND FRAGILE AND SHOULD BE REVIEWED AND REVISED -- H.H.
		log.debug(String.format("Cidr.isValidIpAddress %s %s", ipAddress, subnetMask));
		if (!validateIpAddressFormat(ipAddress)) {
			log.error(MESSAGE_BAD_IP_ADDRESS+COLON_SPACE+ipAddress);
			throw new InvalidInputException(MESSAGE_BAD_IP_ADDRESS+COLON_SPACE+ipAddress);
		}
		if (!isValidSubnetMask(subnetMask)) {
			log.error(MESSAGE_BAD_SUBNET_MASK+COLON_SPACE+subnetMask);
			throw new InvalidInputException(MESSAGE_BAD_SUBNET_MASK+COLON_SPACE+subnetMask);
		}
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] subnetFields = mask.split(STRING_COMMA);
		ipAddress = ipAddress.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] addressFields = ipAddress.split(STRING_COMMA);
		if (subnetFields.length != 4 || addressFields.length != 4) {
			log.error("subnetFields.length = " + subnetFields.length + "; addressFields.length = " + addressFields.length);
			return false;
		}
		char[] subnetCharField = null;
		char[] addressCharField = null;
		for (int i=addressFields.length-1; i>=0; i--) {
			char[] subnetCharFieldComplete = new char[8];
			char[] addressCharFieldComplete = new char[8];
			for (int a=0; a<8; a++) {
				subnetCharFieldComplete[a] = '0';
				addressCharFieldComplete[a] = '0';
			}
			subnetCharField= Integer.toBinaryString(Integer.valueOf(subnetFields[i])).toCharArray();
			addressCharField= Integer.toBinaryString(Integer.valueOf(addressFields[i])).toCharArray();
			for (int z=0; z<subnetCharField.length; z++) { // REVERSE
				if (subnetCharField[z]=='1') subnetCharField[z]='0';
				else if (subnetCharField[z]=='0') subnetCharField[z]='1';
			}
			log.debug("subnetCharField = " + String.valueOf(subnetCharField));
			log.debug("addressCharField = " + String.valueOf(addressCharField));
			int diff = subnetCharFieldComplete.length - subnetCharField.length;
			for (int y=subnetCharField.length-1; y>=0; y--){
				subnetCharFieldComplete[y+diff] = subnetCharField[y];
			}
			log.debug("subnetCharFieldComplete = " + String.valueOf(subnetCharFieldComplete));
			diff = addressCharFieldComplete.length - addressCharField.length;
			for (int zz=addressCharField.length-1; zz>=0; zz--) {
				addressCharFieldComplete[zz+diff] = addressCharField[zz];
			}
			log.debug("addressCharFieldComplete = " + String.valueOf(addressCharFieldComplete));

//			for (int j=subnetCharField.length-1, k=addressCharField.length-1; j>=0 && k>=0; j--, k--) {
//				if (subnetCharField[j] == CHAR_ONE && addressCharField[k] == CHAR_ONE) {
//					log.error("Cidr.isValidIpAddress() failing comparing binary fields " + String.valueOf(subnetCharField) + " and " + String.valueOf(addressCharField) );
//					return false;
//				}
//			}

			for (int k=addressCharFieldComplete.length-1; k>=0; k--) {
				if (subnetCharFieldComplete[k] == CHAR_ONE && addressCharFieldComplete[k] == CHAR_ONE) {
					log.error("Cidr.isValidIpAddress() failing comparing binary fields " + String.valueOf(subnetCharFieldComplete) + " and " + String.valueOf(addressCharFieldComplete) );
					return false;
				}
			}
		}
	  return true;
	}

	public static boolean isValidAddress (String ipAddress, String networkIdWSubnetMask) throws InvalidInputException {
		// PIPE-550 THE IMPLEMENTATION OF THIS METHOD IS IN PROGRESS AND INCOMPLETE
		log.debug(String.format("Cidr.isValidAddressPerCidr (%s, %s)", ipAddress, networkIdWSubnetMask));
		String networkId = networkIdWSubnetMask.substring(0, networkIdWSubnetMask.indexOf('/'));
		String subnetDecimal = networkIdWSubnetMask.substring(networkIdWSubnetMask.indexOf('/')+1);
        String yySubnetMask = getInverseSubnetMask(networkIdWSubnetMask);
		log.debug("yySubnetMask = " + yySubnetMask);
		try {
			if (!isValidIpAddress(ipAddress, yySubnetMask)) {
				log.error(String.format("Cidr.isValidAddress (%s, %s)", ipAddress, networkIdWSubnetMask) + " throwing InvalidInputException");
				throw new InvalidInputException(Cidr.MESSAGE_BAD_IP_ADDRESS + COLON_SPACE + ipAddress );
			}
		} catch (NumberFormatException x) {
				throw new InvalidInputException(Cidr.MESSAGE_BAD_IP_ADDRESS + COLON_SPACE + networkIdWSubnetMask + COLON_SPACE);
		}
		log.debug(String.format("Cidr.isValidAddress (%s, %s)", ipAddress, networkIdWSubnetMask) + " returning true");
        return true;
	}



   public static String stripLeadingZeroes (String ipAddress) {
	  	String workingIpAddress = ipAddress.replace(STRING_PERIOD, STRING_COMMA);
	  	StringBuilder returnIpAddress = new StringBuilder();
	  	String[] fields = workingIpAddress.split(STRING_COMMA);
	  	for (String field: fields) {
			returnIpAddress.append(STRING_PERIOD+Integer.valueOf(field).toString());
		}
	    returnIpAddress.deleteCharAt(0);
	  	return returnIpAddress.toString();
   }


//	public static void stripLeadingZeroes (AclRoute route) {
//	  	String xxIpAddress = route.getSource_cidr().substring(0, route.getSource_cidr().indexOf(SLASH));
//	  	String yyCidr = route.getSource_cidr().substring(route.getSource_cidr().indexOf(SLASH));
//	  	String zzHostIpAddress = route.getDestination_host();
//		route.setSource_cidr(stripLeadingZeroes(xxIpAddress)+yyCidr);
//		route.setDestination_host(stripLeadingZeroes(zzHostIpAddress));
//  	}
//
//  	public static AclPayload validatePayloadFormat (AclPayload payload) throws InvalidInputException {
//		AclPayload returnPayload = new AclPayload();
//		List<AclRoute> routes = payload.getRoutes();
//		List<AclRoute> returnRoutes = new ArrayList<>();
//	  	for (AclRoute route: routes) {
//			if (isValidAclRoute (route)) { // may throw InvalidInputException
//				stripLeadingZeroes (route);
//				returnRoutes.add(route);
//			}
//		}
//		returnPayload.setRoutes(returnRoutes);
//		return returnPayload;
//	}

}
