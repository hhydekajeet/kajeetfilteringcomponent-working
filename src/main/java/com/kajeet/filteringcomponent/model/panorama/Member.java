package com.kajeet.filteringcomponent.model.panorama;

import com.amazonaws.util.json.Jackson;

import java.util.ArrayList;

public class Member {
    ArrayList<String> member;

    public Member(){}

    public Member(ArrayList<String> member){
        this.member = member;
    }

    public ArrayList<String> getMember() {
        return member;
    }

    public void setMember(ArrayList<String> member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
