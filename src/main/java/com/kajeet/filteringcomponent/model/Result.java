package com.kajeet.filteringcomponent.model;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class Result {
	private String code;
	private String message;
	private List<String> fields;
	
	public Result() {
		super();
	}

	public Result(String code, String message, List<String> fields) {
		this.code = code;
		this.message = message;
		this.fields = fields;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		return Jackson.toJsonString(this);
	}
}
