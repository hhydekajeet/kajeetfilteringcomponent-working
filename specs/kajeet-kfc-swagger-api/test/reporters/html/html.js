/**
 * Module dependencies.
 */

var Base = require('mocha').reporters.Base;
var ErrorParser = require('./errorParser.js');
var inherits = require('util').inherits;
var color = Base.color;
var cursor = Base.cursor;
var fs = require('fs');
var ejs = require('ejs');


/**
 * Expose `HTMLReporter`.
 */

exports = module.exports = HTMLReporter;

/**
 * Initialize a new `HTML` reporter.
 *
 * @api public
 * @param {Runner} runner
 */
function HTMLReporter(runner, options) {

  Base.call(this, runner);

  var self = this;
  var indents = 0;
  var failCount = 0;
  var n = 0;

  var tests = [];
  var pending = [];
  var failures = [];
  var passes = [];

  function indent() {
    return Array(indents).join('  ');
  }

  runner.on('start', function() {
    console.log();
  });

  runner.on('suite', function(suite) {
    ++indents;
    console.log(color('suite', '%s%s'), indent(), suite.title);
  });

  runner.on('suite end', function() {
    --indents;
    if (indents === 1) {
      console.log();
    }
  });

  runner.on('pending', function(test) {
    var fmt = indent() + color('pending', '  - %s');
    console.log(fmt, test.title);
    pending.push(test);
  });

  runner.on('pass', function(test) {
    var fmt;
    if (test.speed === 'fast') {
      fmt = indent()
        + color('checkmark', '  ' + Base.symbols.ok)
        + color('pass', ' %s');
      cursor.CR();
      console.log(fmt, test.title);
    } else {
      fmt = indent()
        + color('checkmark', '  ' + Base.symbols.ok)
        + color('pass', ' %s')
        + color(test.speed, ' (%dms)');
      cursor.CR();
      console.log(fmt, test.title, test.duration);
    }
    passes.push(test);
  });

  runner.on('fail', function(test) {
    cursor.CR();
    failCount++;
    test.id = failCount;
    console.log(indent() + color('fail', '  %d) %s'), ++n, test.title);
    failures.push(test);
  });

  runner.on('test end', function(test) {
    tests.push(test);
  });

  runner.on('end', function() {
    var obj = {
      stats: self.stats,
      tests: tests.map(clean),
      pending: pending.map(clean),
      failures: failures.map(clean),
      passes: passes.map(clean)
    };

    runner.testResults = obj;

    ejs.renderFile('./test/reporters/html/template.ejs', obj, null, function(ejsError, html){
        if (ejsError) throw ejsError;

        var filename = (options.reporterOptions && options.reporterOptions.filename) ? options.reporterOptions.filename : 'REPORT.html';

        fs.writeFileSync(filename, html, 'utf-8');

        self.epilogue();

        console.log('  HTML report was also exported.\n');
    });
  });

}

/**
 * Inherit from `Base.prototype`.
 */
inherits(HTMLReporter, Base);


/**
 * Return a simpler and more readable representation of `test`,
 * with a more detailed error object, free of cyclic properties etc.
 *
 * @api private
 * @param {Object} test
 * @return {Object}
 */
function clean(test) {
  var testError;

  if (test.state === 'failed') {
    try {
      testError = ErrorParser.parse(test.err);
    } catch(parseErr) {
      console.log('ReporterError: Error catched while parsing test errors. \n');
      console.log(parseErr.stack + '\n');
    }
  } else {
    testError = null;
  }

  return {
    id: test.id,
    title: test.title,
    fullTitle: test.fullTitle(),
    state: test.state,
    duration: test.duration,
    speed: test.speed,
    currentRetry: test.currentRetry(),
    error: testError
  };
}
