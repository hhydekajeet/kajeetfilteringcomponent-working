package com.kajeet.filteringcomponent.dbmodel;

public class VCustomUrl {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String CORP_ID = "corp_id";
    public static final String CORP_NAME = "corp_name";
    public static final String POLICY_NAME = "policy_name";
    public static final String CUSTOM_FILTERING_TYPE = "custom_filtering_type";

    private int id;
    private String name;
    private int corp_id;
    private String corp_name;
    private String policy_name;
    private String custom_filtering_type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCorp_id() {
        return corp_id;
    }

    public void setCorp_id(int corp_id) {
        this.corp_id = corp_id;
    }

    public String getCorp_name() {
        return corp_name;
    }

    public void setCorp_name(String corp_name) {
        this.corp_name = corp_name;
    }

    public String getPolicy_name() {
        return policy_name;
    }

    public void setPolicy_name(String policy_name) {
        this.policy_name = policy_name;
    }

    public String getCustom_filtering_type() {
        return custom_filtering_type;
    }

    public void setCustom_filtering_type(String custom_filtering_type) {
        this.custom_filtering_type = custom_filtering_type;
    }
}
