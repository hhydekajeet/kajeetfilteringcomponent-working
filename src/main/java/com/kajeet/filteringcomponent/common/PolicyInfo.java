package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class PolicyInfo {
    private List<ClusterInfo> clusters;
    private String fromPolicy;
    private String toPolicy;

    public List<ClusterInfo> getClusters() {
        return clusters;
    }

    public void setClusters(List<ClusterInfo> clusters) {
        this.clusters = clusters;
    }

    public String getFromPolicy() {
        return fromPolicy;
    }

    public void setFromPolicy(String fromPolicy) {
        this.fromPolicy = fromPolicy;
    }

    public String getToPolicy() {
        return toPolicy;
    }

    public void setToPolicy(String toPolicy) {
        this.toPolicy = toPolicy;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
