use scratch;
-- - BEGIN TRIGGER CREATION    -------------------------------------------------------------------------------------
drop trigger if exists trg_ip_pool_active_ins_ck;
delimiter //
CREATE TRIGGER trg_ip_pool_active_ins_ck
BEFORE INSERT ON ip_pool
FOR EACH ROW
BEGIN
if NEW.active_num is null then set NEW.active_num = -1*NEW.id; end if;
IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
END IF;
END;//
delimiter ;



drop trigger if exists trg_ip_pool_active_upd_ck;
delimiter //
CREATE TRIGGER trg_ip_pool_active_upd_ck
BEFORE UPDATE ON ip_pool
FOR EACH ROW
BEGIN
if NEW.active_num is null then set NEW.active_num = -1*NEW.id; end if;
IF NEW.active_num != NEW.carrier_id AND NEW.active_num != -1*NEW.id THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ip_pool.active_num must be = ip_pool.carrier_id or negative of ip_pool.id', MYSQL_ERRNO = 31415;
END IF;
END;//

drop trigger if exists trg_corp_xux_row;
-- delimiter //
-- CREATE TRIGGER trg_corp_xux_row 
-- BEFORE UPDATE ON corp
-- FOR EACH ROW
-- BEGIN
-- 	IF NEW.policy_id != OLD.policy_id THEN
-- 		set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
-- 		insert into custom_url_policy_history (custom_url_policy_id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, created_by, updated_by)
-- 		       select cst.id, NEW.id, NEW.name, OLD.policy_id, ply.name, ply.palo_alto_id, cst.name, 'trg_corp_xux_row', 'trg_corp_xux_row' from vw_custom_url_policy cst join policy ply on ply.id = OLD.policy_id where cst.corp_id = NEW.id;
--         delete from custom_url_policy where corp_id = NEW.id;
-- 	END IF;
-- END;//
-- delimiter ;
-- 
-- drop trigger if exists trg_corp_xux_row;
-- delimiter //
-- CREATE TRIGGER trg_corp_xux_row 
-- BEFORE UPDATE ON corp
-- FOR EACH ROW
-- BEGIN
-- 	IF NEW.policy_id != OLD.policy_id THEN
-- 		set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
--         delete from custom_url_policy where corp_id = NEW.id;
-- 	END IF;
-- END;//
-- delimiter ;


-- use kfcdb;
-- drop trigger if exists trg_corp_xux2_row;
-- 
-- delimiter //
-- CREATE TRIGGER trg_corp_xux2_row 
-- BEFORE UPDATE ON corp
-- FOR EACH ROW
-- BEGIN
-- 	IF NEW.policy_id != OLD.policy_id THEN
-- 		set NEW.custom_filtering=(select custom_filtering from policy where id = NEW.policy_id);
-- 		insert into custom_url_policy_history (custom_url_policy_id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, created_by, updated_by)
-- 		       select cst.id, NEW.id, NEW.name, OLD.policy_id, ply.name, ply.palo_alto_id, cst.name, 'trg_corp_xux_row', 'trg_corp_xux_row' 
-- 		         from custom_url_policy cst 
-- 		         join policy ply on ply.id = OLD.policy_id 
-- 		        where cst.corp_id = NEW.id;
--         delete from custom_url_policy where corp_id = NEW.id;
-- 	END IF;
-- END;//
-- delimiter ;



-- - END TRIGGER CREATION    -------------------------------------------------------------------------------------
