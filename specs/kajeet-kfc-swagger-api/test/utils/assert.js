var _ = require('lodash');
var moment = require('moment');
var AssertionError = require('assertion-error');


// Expect all fields with key to follow specified date format
function expectDateFormat(body, dateFormat, key) {
  var err;
  var resValues = _.hasKeyDeep(body, key, true);

  if (!_.isEmpty(resValues)) {
    var validDates = _.every(resValues, function (val){
      return moment(val, dateFormat, true).isValid();
    });

    if (!validDates) {
      err = new AssertionError('Invalid date format. Expected "'+key+'" fields to follow '+dateFormat+' format during expectDateFormat assertion.');
    }
  } else {
    err = new AssertionError('No "'+key+'" fields found when checking date format during expectDateFormat assertion.');
  }

  return err;
}


// Expect some (at least one) fields with key to have specified value
function expectSome(body, key, value) {
  var err;
  var resValues = _.hasKeyDeep(body, key, true);

  if (!_.isEmpty(resValues)) {
    var valid = _.some(resValues, function (val){
      return _.eq(value, val);
    });

    if (!valid) {
      err = new AssertionError('Value not found. Expected at least some "'+key+'" fields to have ['+value+'] as value during expectSome assertion.');
    }
  } else {
    err = new AssertionError('No "'+key+'" fields found during expectSome assertion.');
  }

  return err;
}


// Expect all fields with key to have specified value
function expectAll(body, key, value) {
  var err;
  var resValues = _.hasKeyDeep(body, key, true);

  if (!_.isEmpty(resValues)) {
    var valid = _.every(resValues, function (val){
      return _.eq(value, val);
    });

    if (!valid) {
      err = new AssertionError('Different value found. Expected all "'+key+'" fields to have ['+value+'] as value during expectAll assertion.');
    }
  } else {
    err = new AssertionError('No "'+key+'" fields found during expectAll assertion.');
  }

  return err;
}


module.exports = {
  expectDateFormat: expectDateFormat,
  expectSome: expectSome,
  expectAll: expectAll
}