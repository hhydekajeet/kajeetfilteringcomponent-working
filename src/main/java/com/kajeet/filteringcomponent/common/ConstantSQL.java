package com.kajeet.filteringcomponent.common;


public class ConstantSQL {

	private ConstantSQL() {
		throw new IllegalStateException("ConstantSQL class");
	}

	public static final String FLAG_YES                  = "Y";
	public static final String FLAG_NO                   = "N";
	public static final String OPEN_PAREN                = "(";
	public static final String CLOSE_PAREN               = ")";
	public static final String SINGLE_QUOTE              = "'";
	public static final String COMMA_SPACE               = ", ";
	public static final String COMMA_SPACE_SINGLE_QUOTE  = ", '";
	public static final String CLOSE_PAREN_VALUES_OPEN_PAREN  = CLOSE_PAREN + " values " + OPEN_PAREN;

	public static final String DB_CONNECTION_UNABLE                = "Unable to get a database connection";
	public static final String MYSQL_ERR_DUPLICATE                 = "duplicate";
	public static final String MYSQL_ERR_FOR_KEY                   = "for key";
	public static final String MYSQL_ERR_FK_VIOLATION              = "foreign key constraint fails";
	public static final String CUSTOM_FILTERING                    = "custom_filtering";
	public static final String ID                                  = "id";
	public static final int    MOST_UNLIKELY_ID_VAL                = -314159265;
	public static final String NAME                                = "name";
	public static final String STATUS                              = "status";
	public static final String POOL                                = "pool";
	public static final String CIDR                                = "cidr";
	public static final String DB_CLUSTER_ID                       = "cluster_id";
	public static final String DB_CLUSTER_NAME                     = "cluster_name";
	public static final String DB_CARRIER_NAME                     = "carrier_name";
	public static final String DB_POLICY_NAME                      = "policy_name";
	public static final String DB_POLICY_PALO_ALTO_ID              = "policy_palo_alto_id";
	public static final String DB_ADDRESS_GROUP_ID                 = "address_group_id";
	public static final String DB_ADDRESS_GROUP_NAME               = "address_group_name";
	public static final String DB_IP_ADDRESS                       = "ip_address";
	public static final String DB_MDN                              = "mdn";
	public static final String DB_CORP_ID                          = "corp_id";
	public static final String CREATED_BY                          = "kfc_app";
	public static final String UPDATED_BY                          = "kfc_app";
	public static final String CREATED_UPDATED_FIELD_LIST          = ", created_by, updated_by";
	public static final String CREATED_UPDATED_VALUE_LIST          = ", '" + CREATED_BY + "', '" + UPDATED_BY + SINGLE_QUOTE;

	public static final String DB_TABLE_CORP                      = "corp";
	public static final String DB_TABLE_POLICY                    = "policy";
	public static final String DB_TABLE_URL_FILTER                = "url_filter";
	public static final String DB_TABLE_URL_LIST                  = "url_list";
	public static final String DB_TABLE_CUSTOM_URL                = "custom_url";
	public static final String DB_OBJECT_CORP                      = "vw_corp";
	public static final String DB_OBJECT_POLICY                    = "vw_policy";
	public static final String DB_OBJECT_URL_FILTER                = "vw_url_filter";
	public static final String DB_OBJECT_URL_LIST                  = "vw_url_list";
	public static final String DB_OBJECT_CUSTOM_URL                = "vw_custom_url";
	public static final String DB_TABLE_SITE_ACCESS_TYPE          = "site_access_type";
	public static final String SITE_ACCESS_ALLOW                   = "Allow";
	public static final String SITE_ACCESS_DENY                    = "Deny";

	public static final String WHERE_NAME_EQUALS_BINDVAR           = " where name = ? ";
	public static final String WHERE_SHORT_NAME_EQUALS_BINDVAR     = " where short_name = ? ";
	public static final String WHERE_CORP_NAME_EQUALS_BINDVAR      = " where corp_name = ? ";
	public static final String WHERE_CORP_NAME_CUSTOM_EQ_BINDVAR   = " where corp_name_custom = ? ";
	public static final String SELECT                              = "select ";
	public static final String SELECT_STAR_FROM                    = "select * from ";
	public static final String SELECT_ID_FROM                      = "select id from ";
	public static final String SELECT_ID_FROM_CORP                 = SELECT_ID_FROM + DB_TABLE_CORP;
	public static final String SELECT_ID_FROM_CORP_SUBQUERY        = OPEN_PAREN + SELECT_ID_FROM + DB_TABLE_CORP       + WHERE_NAME_EQUALS_BINDVAR + CLOSE_PAREN;
	public static final String SELECT_ID_FROM_POLICY_SUBQUERY      = OPEN_PAREN + SELECT_ID_FROM + DB_TABLE_POLICY     + WHERE_NAME_EQUALS_BINDVAR + CLOSE_PAREN;
	public static final String SELECT_ID_FROM_SITE_ACCESS_TYPE_SUBQUERY = OPEN_PAREN + SELECT_ID_FROM + DB_TABLE_SITE_ACCESS_TYPE + WHERE_SHORT_NAME_EQUALS_BINDVAR + CLOSE_PAREN;
	public static final String SELECT_ID_FROM_URL_FILTER_SUBQUERY  = OPEN_PAREN + SELECT_ID_FROM + DB_TABLE_URL_FILTER + WHERE_NAME_EQUALS_BINDVAR + CLOSE_PAREN;
	public static final String SELECT_ID_FROM_URL_LIST_SUBQUERY    = OPEN_PAREN + SELECT_ID_FROM + DB_TABLE_URL_LIST   + WHERE_NAME_EQUALS_BINDVAR + CLOSE_PAREN;
	public static final String ONE_BIND_VAR                        = "? ";
	public static final String ONE_MORE_BIND_VAR                   = ", " + ONE_BIND_VAR;
	public static final String TWO_MORE_BIND_VARS                  = ONE_MORE_BIND_VAR + ONE_MORE_BIND_VAR;
	public static final String THREE_MORE_BIND_VARS                = TWO_MORE_BIND_VARS + ONE_MORE_BIND_VAR;
	public static final String SIX_MORE_BIND_VARS                  = THREE_MORE_BIND_VARS + THREE_MORE_BIND_VARS;
	public static final String FOUR_BIND_VARS                      = ONE_BIND_VAR + THREE_MORE_BIND_VARS;
	public static final String FIVE_BIND_VARS                      = ONE_BIND_VAR + TWO_MORE_BIND_VARS + TWO_MORE_BIND_VARS;
	public static final String SEVEN_BIND_VARS                     = ONE_BIND_VAR + SIX_MORE_BIND_VARS;
	public static final String COUNT_THEM                          = "count_them";
	public static final String COUNT_STAR_AS_COUNT_THEM            = "count(*) as " + COUNT_THEM;
	public static final String FROM                                = " from ";

	public static final String QUERY_ALL_POLICIES                  = "select * from vw_policy";
//	public static final String QUERY_CUSTOM_URL_BY_CORP_ID         = "select id, name, corp_id, corp_name, policy_name, custom_filtering_type from vw_custom_url_policy where corp_name = ?";
	public static final String QUERY_CUSTOM_URL_BY_CORP_NAME       = SELECT_STAR_FROM + DB_OBJECT_CUSTOM_URL + WHERE_CORP_NAME_EQUALS_BINDVAR;
	public static final String QUERY_CORP_BY_NAME                  = "select * from vw_corp where name = ?";
	public static final String SELECT_PALO_ALTO_ID_BY_CORP         = SELECT + DB_POLICY_PALO_ALTO_ID + FROM + DB_OBJECT_CORP + WHERE_NAME_EQUALS_BINDVAR;
	public static final String SELECT_POLICY_BY_CORP               = SELECT + DB_POLICY_NAME + FROM + DB_OBJECT_CORP + WHERE_NAME_EQUALS_BINDVAR;
	public static final String SELECT_CUSTOM_POLICY_ID_BY_CORP     = SELECT + ID + FROM + DB_OBJECT_POLICY + WHERE_CORP_NAME_CUSTOM_EQ_BINDVAR;
	public static final String SELECT_CUSTOM_POLICY_NAME_BY_CORP   = SELECT + NAME + FROM + DB_OBJECT_POLICY + WHERE_CORP_NAME_CUSTOM_EQ_BINDVAR;
	public static final String SELECT_URL_FILTER_ID_BY_CORP        = SELECT + ID + FROM + DB_OBJECT_URL_FILTER + WHERE_CORP_NAME_EQUALS_BINDVAR;
	public static final String SELECT_URL_FILTER_NAME_BY_CORP      = SELECT + NAME + FROM + DB_OBJECT_URL_FILTER + WHERE_CORP_NAME_EQUALS_BINDVAR;
	public static final String SELECT_SITE_ACCESS_TYPE_ID_BY_SHORT_NAME = SELECT + ID + FROM + DB_TABLE_SITE_ACCESS_TYPE + WHERE_SHORT_NAME_EQUALS_BINDVAR;
	public static final String SELECT_COUNT_URL_LISTS_BY_FILTER    = SELECT + COUNT_STAR_AS_COUNT_THEM + FROM + DB_OBJECT_URL_LIST + " where url_filter_name = ?";
	public static final String QUERY_DEVICES_BY_CORP_ID            = "select * from vw_address where corp = ?";

	// custom_filtering_type is deprecated
	public static final String INSERT_CUSTOM_POLICY_FOR_CORP       = "insert into policy (corp_id_custom, site_access_type_id, name, custom_filtering"
			+ CREATED_UPDATED_FIELD_LIST + CLOSE_PAREN_VALUES_OPEN_PAREN + SELECT_ID_FROM_CORP_SUBQUERY + COMMA_SPACE + SELECT_ID_FROM_SITE_ACCESS_TYPE_SUBQUERY + TWO_MORE_BIND_VARS + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;
	public static final String INSERT_URL_FILTER                   = "insert into url_filter (policy_id, corp_id, name" + CREATED_UPDATED_FIELD_LIST
			+ CLOSE_PAREN_VALUES_OPEN_PAREN + SELECT_ID_FROM_POLICY_SUBQUERY + COMMA_SPACE + SELECT_ID_FROM_CORP_SUBQUERY + ONE_MORE_BIND_VAR + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;
	public static final String INSERT_URL_LIST                     = "insert into url_list (url_filter_id, site_access_type_id, corp_id, name" + CREATED_UPDATED_FIELD_LIST
			+ CLOSE_PAREN_VALUES_OPEN_PAREN + SELECT_ID_FROM_URL_FILTER_SUBQUERY + COMMA_SPACE + SELECT_ID_FROM_SITE_ACCESS_TYPE_SUBQUERY + COMMA_SPACE + SELECT_ID_FROM_CORP_SUBQUERY + ONE_MORE_BIND_VAR + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;
//	public static final String INSERT_CUSTOM_URL_BY_CORP_ID        = "insert into custom_url_policy (corp_id, name" + CREATED_UPDATED_FIELD_LIST + CLOSE_PAREN_VALUES_OPEN_PAREN + SELECT_ID_FROM_CORP_SUBQUERY + ONE_MORE_BIND_VAR + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;
	public static final String INSERT_CUSTOM_URL_BY_LIST           = "insert into custom_url (url_list_id, corp_id, name" + CREATED_UPDATED_FIELD_LIST + CLOSE_PAREN_VALUES_OPEN_PAREN + SELECT_ID_FROM_URL_LIST_SUBQUERY + COMMA_SPACE + SELECT_ID_FROM_CORP_SUBQUERY + ONE_MORE_BIND_VAR + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;
	public static final String INSERT_CUSTOM_URL_HISTORY           = "insert into custom_url_policy_history (custom_url_policy_id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, created_by, updated_by) ";
	public static final String INSERT_CUSTOM_URL_HISTORY_BY_CORP_NAME = INSERT_CUSTOM_URL_HISTORY +
			"select id, corp_id, corp_name, policy_id, policy_name, policy_palo_alto_id, name, 'filteringcomponent', 'filteringcomponent' from vw_custom_url_policy where id is not null and corp_name = ?";
	public static final String INSERT_CUSTOM_URL_HISTORY_BY_CORP_AND_NAME = INSERT_CUSTOM_URL_HISTORY_BY_CORP_NAME + " and name = ?";
	public static final String CORP_ID_CANNOT_BE_NULL              = "Column 'corp_id' cannot be null";
	public static final String INSERT_SOURCE_TRANSACTION           = "insert into source_transaction (id, source, type, status, request_json" + CREATED_UPDATED_FIELD_LIST + CLOSE_PAREN_VALUES_OPEN_PAREN + FIVE_BIND_VARS + CREATED_UPDATED_VALUE_LIST + CLOSE_PAREN;

	public static final String QUERY_CARRIER_PER_IP_POOL_NAME      = "select name from vw_carrier where id = (select carrier_id from vw_ip_pool where pool = ?)";
	public static final String QUERY_STATUS_PER_TRANSACTION_ID     = "select status from vw_source_transaction where id = ?";
	public static final String QUERY_IP_POOL_PER_CARRIER           = "select pool from vw_ip_pool where carrier_name = ? and active_flag ='Y'";
	public static final String QUERY_CIDR_PER_IP_POOL_NAME         = "select cidr from vw_ip_pool where pool = ?";
	public static final String QUERY_CLUSTER_ID_PER_IP_POOL        = "select cluster_id from vw_ip_pool where pool = ?";
	public static final String QUERY_CLUSTER_ID_NAME_PER_IP_POOL   = "select cluster_id, cluster_name from vw_ip_pool where pool = ? limit 1";
	public static final String QUERY_AVAILABLE_ADDRESS_GROUP_BY_CORP_AND_CLUSTER = "select * from vw_address_group where corp_id = ? and cluster_id = ? and is_available = 1 LIMIT 1";
	public static final String INSERT_ADDRESS                      = "insert into address (address_group_id, mdn, imei, ip_address, type, created_by, updated_by) values (" + SEVEN_BIND_VARS + CLOSE_PAREN;
	public static final String DELETE_ADDRESSES_BY_CORP_ID_AND_MDN = "delete from address where address_group_id in (" + SELECT_ID_FROM + " address_group where corp_id = ?) and mdn = ?";
	public static final String UPDATE_CORP_POLICY                  = "update corp set policy_id = ?, custom_filtering = (select custom_filtering from policy where id = ?) " + WHERE_NAME_EQUALS_BINDVAR;
	public static final String SELECT_CLUSTERS_AND_ADDRESS_GROUPS_BY_CORP = "select cluster_name, name as address_group_name from vw_address_group where corp_name = ? order by cluster_name, address_group_name";
	public static final String SELECT_CLUSTER_GROUP_ADDRESSES_BY_CORP = "select * from vw_pano_cluster_group_devices where corp_name = ? order by cluster_name, group_name";
	public static final String SELECT_CLUSTER_GROUP_ADDRESSES_BY_CORP_AND_MDN_IN = "select * from vw_pano_cluster_group_devices where corp_name = ? and mdn in ";
	public static final String ORDER_BY_CLUSTER_AND_GROUP          = " order by cluster_name, address_group_name";
//	public static final String DELETE_CUSTOM_URL_BY_NAME           = "delete from custom_url where id = ?";
	public static final String DELETE_CUSTOM_URL_BY_CORP_AND_NAME  = "delete from custom_url where corp_id = " + SELECT_ID_FROM_CORP_SUBQUERY +" and name = ?";


}
