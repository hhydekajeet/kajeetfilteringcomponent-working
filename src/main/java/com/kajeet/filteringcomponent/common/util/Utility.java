package com.kajeet.filteringcomponent.common.util;

import com.kajeet.filteringcomponent.common.Constant;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;

public class Utility {

    public static String generateEventId(String eventSource, String awsRequestId, long timestamp) {
        String source = awsRequestId + timestamp;
        byte[] bytes = null;
        try {
            bytes = source.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        UUID uuid = UUID.nameUUIDFromBytes(bytes);
        return eventSource + "." + uuid.toString().replaceAll("-", "") + "." + (Constant.transactionDateFormat.format(new Date()));
    }

    public static Document convertStringToXMLDocument(String xmlString)
    {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
