package com.kajeet.filteringcomponent.model.objects;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class Policies {

    private List<Policy> policies;

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
