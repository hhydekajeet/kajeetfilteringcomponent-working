package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.*;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import com.kajeet.filteringcomponent.model.panorama.policy.Entries;
import com.kajeet.filteringcomponent.model.panorama.policy.Entry;
import com.kajeet.filteringcomponent.model.panorama.Member;
import com.kajeet.filteringcomponent.model.panorama.policy.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PanoramaCustomUrlUpdate {

    private static final Logger log = LogManager.getLogger(PanoramaDeviceUpdate.class.getName());
    private static final String CUSTOM_URL_LIST_URL = "/restapi/%s/Objects/CustomURLCategories";
    private static final String CREATE_SECURITY_PROFILE_URL = "/restapi/%s/Objects/URLFilteringSecurityProfiles";
    private static final String CREATE_POLICY_URL = "/restapi/%s/Policies/SecurityPreRules";
    private static final String MOVE_POLICY_URL = "/restapi/%s/Policies/SecurityPreRules:move";

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("PanoramaCustomUrlUpdate.handleRequest()");
        log.info("Custom URL Update step functions started: [{}]", input.getTransactionType());
        String panoIP = input.getPanoUrl();
        String panoVersion = input.getPanoVersion();
        String key = input.getApiKey();

        String customUrlListURL = String.format("https://" + panoIP + CUSTOM_URL_LIST_URL, panoVersion);
        String createSecurityProfileURL = String.format("https://" + panoIP + CREATE_SECURITY_PROFILE_URL, panoVersion);
        String createPolicyURL = String.format("https://" + panoIP + CREATE_POLICY_URL, panoVersion);
        String movePolicyURL = String.format("https://" + panoIP + MOVE_POLICY_URL, panoVersion);

        String apiKeyHeader = Constant.PANO_HEADER_KEY_TYPE;

        ObjectMapper objectMapper = new ObjectMapper();
        RestClient rc = new RestClient(true);
        rc.getHeaders().add(apiKeyHeader, key);
        RestResponse response;
        GetResponse getResponse;
        Entry agEntry;
        ArrayList<Entry> postEntries;

        if (input.getTransactionType().equals(SourceTransactionType.PUT_CUSTOM_URL)){
            //This is code for creating/updating the whole custom url (including security profile and policy)
            CustomUrlInfo urlInfo = input.getCustomUrlInfo();

            //Entry for creating the allowed url list
            Entry allowedCustomUrlListEntry = null;
            Map<String, String> allowedUrlListQueryParams = null;
            if (urlInfo.getAllowedUrlList() != null) {
                allowedUrlListQueryParams = new HashMap<>();
                allowedCustomUrlListEntry = new Entry();
                allowedCustomUrlListEntry.setName(urlInfo.getAllowedUrlList().getUrlListName());
                allowedUrlListQueryParams.put("name", urlInfo.getAllowedUrlList().getUrlListName());
                allowedCustomUrlListEntry.setLocation("device-group");
                allowedUrlListQueryParams.put("location", "device-group");
                ArrayList<String> customUrlList = new ArrayList<>();
                customUrlList.addAll(urlInfo.getAllowedUrlList().getUrls());
                allowedCustomUrlListEntry.setUrlList(new Member(customUrlList));
                allowedCustomUrlListEntry.setListType("URL List");
            }

            Entry blockedCustomUrlListEntry = null;
            Map<String, String> blockedUrlListQueryParams = null;

            if (urlInfo.getBlockedUrlList() != null) {
                //Entry for creating the blocked url list
                blockedUrlListQueryParams = new HashMap<>();
                blockedCustomUrlListEntry = new Entry();
                blockedCustomUrlListEntry.setName(urlInfo.getBlockedUrlList().getUrlListName());
                blockedUrlListQueryParams.put("name", urlInfo.getBlockedUrlList().getUrlListName());
                blockedCustomUrlListEntry.setLocation("device-group");
                blockedUrlListQueryParams.put("location", "device-group");
                ArrayList<String> customUrlList = new ArrayList<>();
                customUrlList.addAll(urlInfo.getBlockedUrlList().getUrls());
                blockedCustomUrlListEntry.setUrlList(new Member(customUrlList));
                blockedCustomUrlListEntry.setListType("URL List");
            }

            //Entry for creating the security profile
            Map<String, String> secProfileQueryParams = new HashMap<>();
            Entry secProfileEntry = new Entry();
            secProfileEntry.setName(urlInfo.getSecurityProfileName());
            secProfileQueryParams.put("name", urlInfo.getSecurityProfileName());
            secProfileEntry.setLocation("device-group");
            secProfileQueryParams.put("location", "device-group");

            if (urlInfo.getAllowedUrlList() != null) {
                ArrayList<String>  allowURLList = new ArrayList();
                allowURLList.add(urlInfo.getAllowedUrlList().getUrlListName());
                Member allowUrlListMember = new Member(allowURLList);
                secProfileEntry.setAllow(allowUrlListMember);
            }

            if (urlInfo.getBlockedUrlList() != null){
                ArrayList<String>  blockURLList = new ArrayList();
                blockURLList.add(urlInfo.getBlockedUrlList().getUrlListName());
                Member blockUrlListMember = new Member(blockURLList);
                secProfileEntry.setBlock(blockUrlListMember);
            }

            //Entry for creating the policy
            Map<String, String> policyQueryParams = new HashMap<>();
            Entry policyEntry = new Entry();
            policyEntry.setName(urlInfo.getPolicyName());
            policyQueryParams.put("name", urlInfo.getPolicyName());
            policyEntry.setLocation("device-group");
            policyQueryParams.put("location", "device-group");

            ArrayList<String> members = new ArrayList<>();
            members.add(urlInfo.getSecurityProfileName());
            Member urlFilters = new Member(members);
            Profiles profiles = new Profiles();
            profiles.setUrlFiltering(urlFilters);
            ProfileSetting profileSetting = new ProfileSetting();
            profileSetting.setProfiles(profiles);
            policyEntry.setProfileSetting(profileSetting);
            setPolicyCreateDefaults(policyEntry);

            //Params to move the policy to the top
            Map<String, String> movePolicyQueryParams = new HashMap<>();
            movePolicyQueryParams.put("location", "device-group");
            movePolicyQueryParams.put("name", urlInfo.getPolicyName());
            movePolicyQueryParams.put("where", "top");

            //Now we need to generate the list on each cluster
            for (ClusterInfo clusterInfo: urlInfo.getClusters()) {
                log.info("Updating cluster {}", clusterInfo.getClusterName());
                //Create the url lists allow and block
                if (urlInfo.getAllowedUrlList() != null){
                    allowedCustomUrlListEntry.setDeviceGroup(clusterInfo.getClusterName());
                    ArrayList<Entry> customUrlListEntryAL = new ArrayList<>();
                    customUrlListEntryAL.add(allowedCustomUrlListEntry);
                    Entries customUrlListEntires = new Entries();
                    customUrlListEntires.setEntries(customUrlListEntryAL);
                    allowedUrlListQueryParams.put("device-group", clusterInfo.getClusterName());
                    //Create the URL List
                    response = rc.doPost(customUrlListURL, customUrlListEntires.toString(), allowedUrlListQueryParams);
                    if (response.getStatus().is2xxSuccessful()) {
                        log.info("Success -  URL List added [{}] for cluster [{}]", urlInfo.getAllowedUrlList().getUrlListName(), clusterInfo.getClusterName());
                        log.debug("Response [{}]", response);
                    } else {
                        log.error("Error adding url list {}.\n Response Code: {}\n Message: {}", urlInfo.getAllowedUrlList().getUrlListName(), response.getStatus(), response.getMessage());
                        throw new SystemException("Error adding url list " + urlInfo.getAllowedUrlList().getUrlListName());
                    }
                }

                if (urlInfo.getBlockedUrlList() != null){
                    blockedCustomUrlListEntry.setDeviceGroup(clusterInfo.getClusterName());
                    ArrayList<Entry> customUrlListEntryAL = new ArrayList<>();
                    customUrlListEntryAL.add(blockedCustomUrlListEntry);
                    Entries customUrlListEntires = new Entries();
                    customUrlListEntires.setEntries(customUrlListEntryAL);
                    blockedUrlListQueryParams.put("device-group", clusterInfo.getClusterName());
                    //Create the URL List
                    response = rc.doPost(customUrlListURL, customUrlListEntires.toString(), blockedUrlListQueryParams);
                    if (response.getStatus().is2xxSuccessful()) {
                        log.info("Success -  URL List added [{}] for cluster [{}]", urlInfo.getAllowedUrlList().getUrlListName(), clusterInfo.getClusterName());
                        log.debug("Response [{}]", response);
                    } else {
                        log.error("Error adding url list {}.\n Response Code: {}\n Message: {}", urlInfo.getAllowedUrlList().getUrlListName(), response.getStatus(), response.getMessage());
                        throw new SystemException("Error adding url list " + urlInfo.getAllowedUrlList().getUrlListName());
                    }
                }

                //Create the security profile
                secProfileEntry.setDeviceGroup(clusterInfo.getClusterName());
                ArrayList<Entry> secProfileEntryAL = new ArrayList<>();
                secProfileEntryAL.add(secProfileEntry);
                Entries secProfileEntires = new Entries();
                secProfileEntires.setEntries(secProfileEntryAL);
                secProfileQueryParams.put("device-group", clusterInfo.getClusterName());
                response = rc.doPost(createSecurityProfileURL, secProfileEntires.toString(), secProfileQueryParams);
                if (response.getStatus().is2xxSuccessful()) {
                    log.info("Success -  Security profile added [{}] for cluster [{}]",  urlInfo.getSecurityProfileName(), clusterInfo.getClusterName());
                    log.debug("Response [{}]", response);
                } else {
                    log.error("Error adding security profile {}.\n Response Code: {}\n Message: {}", urlInfo.getSecurityProfileName(), response.getStatus(), response.getMessage());
                    throw new SystemException("Error adding security profile " + urlInfo.getSecurityProfileName());
                }
                //Create the policy
                Member source = new Member(new ArrayList<>(clusterInfo.getAddressGroups()));
                policyEntry.setSource(source);
                policyEntry.setDeviceGroup(clusterInfo.getClusterName());
                ArrayList<Entry> policyEntryAL = new ArrayList<>();
                policyEntryAL.add(policyEntry);
                Entries policyEntires = new Entries();
                policyEntires.setEntries(policyEntryAL);
                policyQueryParams.put("device-group", clusterInfo.getClusterName());
                response = rc.doPost(createPolicyURL, policyEntires.toString(), policyQueryParams);
                if (response.getStatus().is2xxSuccessful()) {
                    log.info("Success -  Custom policy added [{}] for cluster [{}]",  urlInfo.getPolicyName(), clusterInfo.getClusterName());
                    log.debug("Response [{}]", response);
                } else {
                    log.error("Error adding custom policy {}.\n Response Code: {}\n Message: {}", urlInfo.getPolicyName(), response.getStatus(), response.getMessage());
                    throw new SystemException("Error adding custom policy " + urlInfo.getPolicyName());
                }

                //Move the policy to the top of the list
                movePolicyQueryParams.put("device-group", clusterInfo.getClusterName());
                response = rc.doPost(movePolicyURL, "", movePolicyQueryParams);
                log.info("Policy {} move to top", urlInfo.getPolicyName());
                if (response.getStatus().is2xxSuccessful()) {
                    log.info("Success -  Custom policy moved [{}] for cluster [{}]",  urlInfo.getPolicyName(), clusterInfo.getClusterName());
                    log.debug("Response [{}]", response);
                } else {
                    log.error("Error adding moving policy {}.\n Response Code: {}\n Message: {}", urlInfo.getPolicyName(), response.getStatus(), response.getMessage());
                    throw new SystemException("Error moving custom policy " + urlInfo.getPolicyName());
                }

                log.info("Completed updating cluster {}", clusterInfo.getClusterName());
            }

        }
        if (input.getTransactionType().equals(SourceTransactionType.UPDATE_CUSTOM_URL)){

            for (CustomUrls customUrls: input.getCustomUrlInfo().getListsToEdit()){

                for (ClusterInfo clusterInfo : input.getCustomUrlInfo().getClusters()) {

                    //Get the url list
                    Map<String, String> customUrlParams = new HashMap<>();
                    customUrlParams.put("location", "device-group");
                    customUrlParams.put("device-group", clusterInfo.getClusterName());
                    customUrlParams.put("name", customUrls.getUrlListName());
                    response = rc.doGet(customUrlListURL, customUrlParams);
                    if (response.getStatus().is2xxSuccessful()) {
                        log.info("Success -  Custom url list {} found for cluster {}",  customUrls.getUrlListName(), clusterInfo.getClusterName());
                        log.debug("Response [{}]", response);
                    } else {
                        log.error("Error - Custom url list {} NOT found for cluster {}",  customUrls.getUrlListName(), clusterInfo.getClusterName());
                        continue;
                    }
                    //Add or remove the urls
                    com.kajeet.filteringcomponent.model.panorama.customurl.GetResponse customUrlGetResponse = null;
                    try {
                        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                        log.debug("Serializing response [{}]", response.getMessage());
                        customUrlGetResponse = objectMapper.readValue(response.getMessage(), com.kajeet.filteringcomponent.model.panorama.customurl.GetResponse.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }

                    if (customUrlGetResponse != null) {
                        com.kajeet.filteringcomponent.model.panorama.customurl.Entry customUrlEntry = customUrlGetResponse.getResult().getEntry().get(0);
                        if (customUrls.getEditType().equals(Constant.ADD)) {
                            for (String aUrl : customUrls.getUrls()) {
                                customUrlEntry.getUrlList().getMember().add(aUrl);
                            }
                        }
                        if (customUrls.getEditType().equals(Constant.DELETE)) {
                            for (String aUrl : customUrls.getUrls()) {
                                customUrlEntry.getUrlList().getMember().remove(aUrl);
                            }
                        }
                        //Update the list
                        com.kajeet.filteringcomponent.model.panorama.customurl.Entries customUrlEntries = new com.kajeet.filteringcomponent.model.panorama.customurl.Entries();
                        customUrlEntries.setEntries(new ArrayList<>());
                        customUrlEntries.getEntries().add(customUrlEntry);

                        response = rc.doPut(customUrlListURL, customUrlEntries.toString(), customUrlParams);
                        if (response.getStatus().is2xxSuccessful()) {
                            log.info("Success -  Custom url list {} on cluster {} updated",  customUrls.getUrlListName(), clusterInfo.getClusterName());
                        } else {
                            log.error("Error - Custom url list {} NOT updated for cluster {}",  customUrls.getUrlListName(), clusterInfo.getClusterName());
                        }
                    }
                }
            }
        }

        input.setStatus("SUCCESS");
        log.info("Policy step functions policy update complete: [{}]", input.getTransactionType());
        return input;
    }

    private void setPolicyCreateDefaults(Entry policyEntry){

        ArrayList<String> anyMemberItem = new ArrayList<>();
        anyMemberItem.add("any");

        Member anyMember = new Member(anyMemberItem);

        policyEntry.setFrom(anyMember);
        policyEntry.setToMember(anyMember);
        policyEntry.setDestination(anyMember);
        policyEntry.setService(anyMember);
        policyEntry.setApplication(anyMember);
        policyEntry.setAction("allow");

    }

 }
