package com.kajeet.filteringcomponent.dbmodel;

public class VPolicy {
    private int id;
    private String name;
    private String paloAltoId;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaloAltoId() {
        return paloAltoId;
    }

    public void setPaloAltoId(String paloAltoId) {
        this.paloAltoId = paloAltoId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
