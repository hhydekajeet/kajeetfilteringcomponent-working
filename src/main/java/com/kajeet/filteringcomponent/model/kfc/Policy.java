package com.kajeet.filteringcomponent.model.kfc;

import com.amazonaws.util.json.Jackson;

public class Policy {
    private String id;
    private String name;

    public Policy(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
