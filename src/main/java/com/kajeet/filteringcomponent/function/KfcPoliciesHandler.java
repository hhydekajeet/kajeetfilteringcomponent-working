package com.kajeet.filteringcomponent.function;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.datadoghq.datadog_lambda_java.DDLambda;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.*;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.MissingRequiredFields405Exception;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.*;
import com.kajeet.filteringcomponent.model.Result;
import com.kajeet.filteringcomponent.model.SourceSystem;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import com.kajeet.filteringcomponent.model.TransactionStatus;
import com.kajeet.filteringcomponent.model.kfc.CorpPolicy;
import com.kajeet.filteringcomponent.model.kfc.CustomUrlApi;
import com.kajeet.filteringcomponent.model.objects.Policies;
import com.kajeet.filteringcomponent.model.objects.Policy;
import com.kajeet.filteringcomponent.model.service.DBPolicyService;
import com.kajeet.filteringcomponent.model.service.JdbcService;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class KfcPoliciesHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final Logger log = LogManager.getLogger(KfcPoliciesHandler.class);
    public static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private static final String PARTIAL_PATH_POLICY = "/service/kfc/policy";
    private static final String PARTIAL_PATH_CUSTOM_URL = "/service/kfc/policy/customurl";
    private static final String PARTIAL_PATH_POLICIES = "/service/kfc/policies";
    private static final String BRANCH_POLICY = "POLICY";
    private static final String BRANCH_CUSTOM_URL = "CUSTOM_URL";
    private static final String BRANCH_POLICIES = "POLICIES";
    private static  String dbUser = "";
    private static  String dbPassword = "";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static String queueURL = "";

    private ObjectMapper objectMapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

    private Connection dbConnection = null;

    static {
        String secretsManager = System.getenv(Constant.KFC_SECRETS_MANAGER);
        dbUser = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_USERNAME);
        dbPassword = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PASSWORD);
        dbHost = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_HOST);
        dbPort = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PORT);
        dbName = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_DBNAME);

        log.debug("user: {}", dbUser);
        log.debug("password: {}", dbPassword);
        log.debug("host: {}", dbHost);
        log.debug("port: {}", dbPort);
        log.debug("dbname: {}", dbName);

        queueURL = System.getenv(Constant.KFC_QUEUE_URL);
        log.debug("Queue: {}", queueURL);

    }

    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        log.debug("KfcPoliciesHandler.handleRequest(): path = " + input.getPath());
        Result result = new Result(Constant.SUCCESS_CODE_200, "", new ArrayList<String>());
        String httpMethod = "";
        String branch = "";
        CorpPolicy corpPolicy = null;
        CustomUrlApi customUrl = null;

        try {
            dbConnection = getDBConnection();
            String path = input.getPath();
            branch = getBranchFromPath(path);
            log.debug("branch = " + branch);

            String bodyJson = input.getBody();
            log.debug("Body: [{}]", bodyJson);
            httpMethod = input.getHttpMethod().toString();
            log.info("Http Method [{}] ", httpMethod);
            String corpId = null;

            switch (branch) {
                case BRANCH_POLICIES:
                    switch (httpMethod) {
                        case Constant.HTTP_GET:
                            // Return all the policies in the DB
                            Policies policies = DBPolicyService.getAllPolicies(dbConnection);
                            log.info("Returning successful response");
                            Response.createSuccessfulResponse(response, policies);
                            break;
                        default:
                            // Do nothing
                            break;
                    }
                    break;
                case BRANCH_POLICY:
                    switch (httpMethod) {
                        case Constant.HTTP_GET:
                            corpId = input.getPathParameters().get(Constant.CORP_ID);
                            validateSingleInput(Constant.CORP_ID, corpId, result);
                            Policy policy = DBPolicyService.getPolicyByCorpId(dbConnection, corpId);
                            log.info("Returning successful response");
                            Response.createSuccessfulResponse(response, policy);
                            break;
                        case Constant.HTTP_POST:
                            corpPolicy = executePostCorpToPolicy(response, bodyJson, result, dbConnection, context);
                            break;
                        default:
                            // Do nothing at all
                            break;
                    }
                    break;
                case BRANCH_CUSTOM_URL:
                    switch (httpMethod) {
                        case Constant.HTTP_GET:
                            corpId = input.getPathParameters().get(Constant.CORP_ID);
                            validateSingleInput(Constant.CORP_ID, corpId, result);
                            customUrl = DBPolicyService.getCustomUrl(dbConnection, corpId);
                            log.info("Returning successful response");
                            Response.createSuccessfulResponse(response, customUrl);
                            break;
                        case Constant.HTTP_PUT:
                            customUrl = executePutCustomUrl(response, bodyJson, result, dbConnection, context);
                            break;
                        case Constant.HTTP_DELETE:
                            customUrl = executeDeleteCustomUrl(response, bodyJson, result, dbConnection, context);
                            break;
                        default:
                            // Do nothing at all
                            break;
                    }
                    break;
            }
        } catch (IOException e) {
            response.setStatusCode(Integer.parseInt(Constant.ERROR_CODE_500));
            log.error(Constant.HTTP_ERROR_CODE + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+Constant.ERROR_CODE_500);
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (InvalidInputException e) {
            response.setStatusCode(e.getCode());
            log.error(Constant.HTTP_ERROR_CODE + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (MissingRequiredFields405Exception e) {
            response.setStatusCode(e.getCode());
            log.error(Constant.HTTP_ERROR_CODE + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (SystemException e) {
            response.setStatusCode(e.getCode());
            log.error(Constant.HTTP_ERROR_CODE + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } finally { // DataDog Logging:
            DDLambda ddl = new DDLambda(input, context);
            final Span span = GlobalTracer.get().activeSpan();
            if (span != null) {
                span.setTag(Constant.CLASS_DOT_NAME, this.getClass().getName());
                span.setTag(Constant.HTTP_METHOD, httpMethod);
                span.setTag(Constant.EVENT, branch.toLowerCase(Locale.ROOT));
                if (corpPolicy != null) {
                    span.setTag(Constant.CORPID, corpPolicy.getCorpId());
                    span.setTag(Constant.POLICYID, corpPolicy.getPolicyId());
                    span.setTag(Constant.SOURCE_SYSTEM, corpPolicy.getSourceSystem());
                    span.setTag(Constant.TRANSACTIONID, corpPolicy.getTransactionId());
                } else if (customUrl != null) {
                    span.setTag(Constant.CORPID, customUrl.getCorpId());
                    span.setTag(Constant.TRANSACTIONID, customUrl.getTransactionId());
                    span.setTag(Constant.SOURCE_SYSTEM, customUrl.getSourceSystem());
                }
                span.setTag(Constant.RESPONSE, response.getBody());
                span.setTag(Constant.RESPONSE_DOT_STATUS_CODE, response.getStatusCode());
                span.setTag(Constant.ATTEMPT_DOT_NUMBER, 1);
                span.finish();
            } else {
                log.info("Span is null");
            }
            ddl.finish();
        }
        return response;
    }


    private String getBranchFromPath (String path) {
        String branch = "";
        if (path.contains(PARTIAL_PATH_CUSTOM_URL)) {
            branch = BRANCH_CUSTOM_URL;
        } else if (path.contains(PARTIAL_PATH_POLICY)) {
            branch = BRANCH_POLICY;
        } else if (path.contains(PARTIAL_PATH_POLICIES)) {
            branch = BRANCH_POLICIES;
        }
        return branch;
    }

    public static void validateSingleInput(String paramName, String paramValue, Result result) throws MissingRequiredFields405Exception {
        if (paramValue==null){
            RequestValidator.addErrorField(paramName, result);
            throw new MissingRequiredFields405Exception("Required param " + paramName + " missing.");
        }
    }

    private void validateCorpPolicy (CorpPolicy corpPolicy, Result result) throws InvalidInputException, MissingRequiredFields405Exception {
        boolean bad = false;
        try {
            if (!SourceSystem.isValid(corpPolicy.getSourceSystem())) {
                bad = true;
                throw new InvalidInputException(corpPolicy.getSourceSystem() + " is not a valid Source System.");
            }
        } catch (MissingRequiredFields405Exception x) {
            bad = true;
            RequestValidator.addErrorField(Constant.SOURCE_SYSTEM, result);
        }
        if (corpPolicy.getCorpId()==null || corpPolicy.getCorpId().length()==0){
            bad = true;
            RequestValidator.addErrorField(Constant.CORPID, result);
        }
        if (corpPolicy.getPolicyId()==null || corpPolicy.getPolicyId().length()==0){
            bad = true;
            RequestValidator.addErrorField(Constant.POLICYID, result);
        }
        if (bad) {
            result.setCode(Constant.ERROR_CODE_405);
            throw new MissingRequiredFields405Exception("One or more required field values missing.");
        }
    }

    private void validateCustomUrl (CustomUrlApi customUrl, Result result, Connection conn) throws InvalidInputException, MissingRequiredFields405Exception, SystemException {
        boolean missing = false;
        try {
            if (customUrl.getSourceSystem() == null || customUrl.getSourceSystem().length() == 0) {
                missing = true;
                RequestValidator.addErrorField(Constant.SOURCE_SYSTEM, result);
            } else if (!SourceSystem.isValid(customUrl.getSourceSystem())) {
                throw new InvalidInputException(customUrl.getSourceSystem() + " is not a valid Source System.");
            }
        } catch (MissingRequiredFields405Exception x) {
            missing = true;
            RequestValidator.addErrorField(Constant.SOURCE_SYSTEM, result);
        }
        Integer siteAccessId = DBPolicyService.getSiteAccessIdByName(conn, customUrl.getType());
        if (siteAccessId==null) {
            throw new InvalidInputException(customUrl.getType() + " is not a valid Site Access Type. Send " + ConstantSQL.SITE_ACCESS_ALLOW + " or " + ConstantSQL.SITE_ACCESS_DENY + ".");
        }

        if (customUrl.getCorpId()==null || customUrl.getCorpId().length()==0){
            missing = true;
            RequestValidator.addErrorField(Constant.CORPID, result);
        }
        if (customUrl.getUrls()==null || customUrl.getUrls().size()==0){
            missing = true;
            RequestValidator.addErrorField(Constant.URLS, result);
        }
        if (missing) {
            result.setCode(Constant.ERROR_CODE_405);
            throw new MissingRequiredFields405Exception ("One or more required field values missing.");
        }

        // CUSTOM_FILTERING_TYPE IS DEPRECATED; REFACTOR THIS TEST
//        String policy = JdbcService.getUniqueStringFieldByStringValue(dbConnection, ConstantSQL.SELECT_POLICY_BY_CORP, ConstantSQL.DB_POLICY_NAME, customUrl.getCorpId());
//        if (!Constant.isValidFilteringPolicy(policy)) {
//            throw new InvalidInputException ("Invalid Policy for Custom Url Filtering: " + policy);
//        }
    }

    private KFCAsyncMessage prepQueueMessage (Connection conn, CorpPolicy corpPolicy, String transactionId, SourceTransactionType transactionType, String fromPolicyPaloAltoId, String toPolicyPaloAltoId) throws SystemException {
        log.debug("KfcPoliciesHandler.prepQueueMessage()");
        KFCAsyncMessage queueMessage = new KFCAsyncMessage();
        PolicyInfo policyInfo = new PolicyInfo();

        policyInfo.setFromPolicy(fromPolicyPaloAltoId);
        policyInfo.setToPolicy(toPolicyPaloAltoId);
        queueMessage.setTransactionId(transactionId);
        queueMessage.setTransactionType(transactionType);
        List<ClusterInfo> clusterInfoList = DBPolicyService.getClusterInfoListByCorp( conn, corpPolicy.getCorpId());
        policyInfo.setClusters(clusterInfoList);
        queueMessage.setPolicyInfo(policyInfo);
        log.debug("KfcPoliciesHandler.prepQueueMessage() returning \r\n" + queueMessage.toString());
        return queueMessage;
    }

    private String getPaloAltoIdByCorp (Connection conn, String corpId) throws  SystemException, InvalidInputException {
        return JdbcService.getUniqueStringFieldByStringValue(dbConnection, ConstantSQL.SELECT_PALO_ALTO_ID_BY_CORP, ConstantSQL.    DB_POLICY_PALO_ALTO_ID, corpId);
    }


    private CorpPolicy executePostCorpToPolicy (APIGatewayProxyResponseEvent response, String bodyJson, Result result, Connection conn, Context context) throws IOException, SystemException, InvalidInputException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.executePostCorpToPolicy()\r\n" + bodyJson);
        CorpPolicy corpPolicy = null;
        try {
            corpPolicy = objectMapper.readValue(bodyJson, CorpPolicy.class);
        } catch (Exception x) {
            log.error(x.getMessage());
            throw new InvalidInputException(Constant.INVALID_REQUEST_BODY + bodyJson);
        }
        String transactionId = Utility.generateEventId(corpPolicy.getSourceSystem(), context.getAwsRequestId(), new Date().getTime());
        log.debug("KfcPoliciesHandler.executePostCorpToPolicy() transactionId = '" + transactionId + "', corpId = '" + corpPolicy.getCorpId() + "', policyId = '" + corpPolicy.getPolicyId() + "' ");
        corpPolicy.setTransactionId(transactionId);
        validateCorpPolicy (corpPolicy, result);
        String fromPolicyPaloAltoId = getPaloAltoIdByCorp (conn, corpPolicy.getCorpId());
        JdbcService.postSourceTransaction(conn, transactionId, corpPolicy.getSourceSystem(), SourceTransactionType.PUT_CORP_POLICY, TransactionStatus.pending.getLabel(), corpPolicy.toString());
        corpPolicy=DBPolicyService.postCorpPolicy (conn, corpPolicy);
        String toPolicyPaloAltoId = getPaloAltoIdByCorp (conn, corpPolicy.getCorpId());
        KFCAsyncMessage queueMessage = prepQueueMessage(conn, corpPolicy, transactionId, SourceTransactionType.PUT_CORP_POLICY, fromPolicyPaloAltoId, toPolicyPaloAltoId);
        postMessageToQueue(queueMessage.toString());
        Response.createSuccessfulResponse(response, corpPolicy);
        return corpPolicy;
    }

    private CustomUrlApi customUrlCommonHousekeeping (String bodyJson, Result result, Connection conn, Context context, SourceTransactionType transactionType) throws InvalidInputException, SystemException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.customUrlHousekeeping()" + bodyJson);
        CustomUrlApi customUrl = null;
        try {
            customUrl = objectMapper.readValue(bodyJson, CustomUrlApi.class);
        } catch (Exception x) {
            log.error(x.getMessage());
            throw new InvalidInputException("Invalid request body: " + bodyJson);
        }
        if (customUrl==null) throw new InvalidInputException("Unable to marshall CustomUrl object from request body: " + bodyJson);
        String transactionId = Utility.generateEventId(customUrl.getSourceSystem(), context.getAwsRequestId(), new Date().getTime());
        customUrl.setTransactionId(transactionId);
        validateCustomUrl (customUrl, result, dbConnection);
        JdbcService.postSourceTransaction(conn, transactionId, customUrl.getSourceSystem(), transactionType, TransactionStatus.pending.getLabel(), customUrl.toString());
        return customUrl;
    }

    private CustomUrlApi executePutCustomUrl (APIGatewayProxyResponseEvent response, String bodyJson, Result result, Connection conn, Context context) throws IOException, InvalidInputException, SystemException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.executePutCustomUrl()\r\n" + bodyJson);
        CustomUrlApi customUrl = customUrlCommonHousekeeping (bodyJson, result, conn, context, SourceTransactionType.PUT_CUSTOM_URL);
        log.debug("KfcPoliciesHandler.executePutCustomUrl() customUrl = \r\n{}" + customUrl);
        DBPolicyService.putCustomUrl (conn, customUrl);
//        KFCAsyncMessage queueMessage = prepQueueMessage(conn, customUrl, transactionId, SourceTransactionType.PUT_CUSTOM_URL);
        postMessageToQueue(customUrl.getTransactionId()); // Need to send payload customized for this use case
        Response.createSuccessfulResponse(response, customUrl);
        return customUrl;
    }

    private CustomUrlApi executeDeleteCustomUrl (APIGatewayProxyResponseEvent response, String bodyJson, Result result, Connection conn, Context context) throws IOException, InvalidInputException, SystemException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.executeDeleteCustomUrl()\r\n" + bodyJson);
        CustomUrlApi customUrl = customUrlCommonHousekeeping (bodyJson, result, conn, context, SourceTransactionType.DELETE_CUSTOM_URL);
        DBPolicyService.deleteCustomUrls (conn, customUrl);
        JdbcService.commitTransaction(conn);
//        KFCAsyncMessage queueMessage = prepQueueMessage(conn, customUrl, transactionId, SourceTransactionType.PUT_CUSTOM_URL);
        postMessageToQueue(customUrl.getTransactionId()); // Need to send payload customized for this use case
        Response.createSuccessfulResponse(response, customUrl);
        return customUrl;
    }

    private Connection getDBConnection() throws SystemException {
        if (dbConnection != null){
            return dbConnection;
        }
        String dbURL = "jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName;

        try {
            dbConnection = JdbcService.getConnection(dbURL,dbUser, dbPassword, false);
        }
        catch (SQLException x) {
            log.error("Error getting a connection to the DB");
            throw new SystemException(ConstantSQL.DB_CONNECTION_UNABLE);
    }
        return dbConnection;
    }

    private String postMessageToQueue(String message){
        log.debug("postMessageToQueue('" +message+"')");
        return SQSUtil.sendSQSMessage(queueURL, message);
    }

}
