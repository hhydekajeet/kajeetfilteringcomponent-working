package com.kajeet.filteringcomponent.model.panorama;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AsyncResult {

    @JsonProperty("@status")
    String status;

    @JsonProperty("@code")
    String code;
    String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
