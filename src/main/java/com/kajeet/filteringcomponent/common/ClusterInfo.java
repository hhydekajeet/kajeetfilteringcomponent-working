package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class ClusterInfo {

    String clusterName;
    List<String> addressGroups;
    List<Address> addresses;
    String corpPolicy;

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public List<String> getAddressGroups() {
        return addressGroups;
    }

    public void setAddressGroups(List<String> addressGroups) {
        this.addressGroups = addressGroups;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public String getCorpPolicy() {
        return corpPolicy;
    }

    public void setCorpPolicy(String corpPolicy) {
        this.corpPolicy = corpPolicy;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
