package com.kajeet.filteringcomponent.common;


import java.text.SimpleDateFormat;

public class Constant {

	private Constant() {
		throw new IllegalStateException("Constant class");
	}

	public static final String STACKNAME = "KajeetFilteringComponentService";

	public static final String KFC_SECRETS_MANAGER = "KFCSecretsManager";
	public static final String PANO_KFC_SECRETS_MANAGER = "PanoSecretsManager";
	public static final String IS_PING = "isPing";
	public static final String LOG_LEVEL = "logLevel";
	public static final String KFC_QUEUE = "kfcQueue";
	public static final String KFC_QUEUE_URL = "kfcQueueUrl";
	public static final String DEVICE_UPDATE_MACHINE_ARN = "deviceUpdateMachineArn";
	public static final String POLICY_UPDATE_MACHINE_ARN = "policyUpdateMachineArn";
	public static final String CUSTOM_URL_MACHINE_ARN = "customUrlUpdateMachineArn";

	public static final String SECRET_USERNAME = "username";
	public static final String SECRET_PASSWORD = "password";
	public static final String SECRET_HOST = "host";
	public static final String SECRET_PORT = "port";
	public static final String SECRET_DBNAME = "dbname";
	public static final String SECRET_PANO_API_URL = "apiUrl";
	public static final String SECRET_PANO_API_VERSION = "apiVersion";
	public static final String SECRET_PANO_API_KEY = "apiKey";


	public static final String CLASS_DOT_NAME = "class.name";
	public static final String ATTEMPT_DOT_NUMBER = "attempt.number";
	public static final String RESPONSE = "response";
	public static final String RESPONSE_DOT_STATUS_CODE = "response.statusCode";
	public static final String ENABLED = "enabled";
	public static final String DISABLED = "disabled";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SUCCESS = "success";
	public static final String ERROR = "ERROR";
	public static final String SUCCESS_CODE_200 = "200";
	public static final String ERROR_CODE_500 = "500";
	public static final String ERROR_CODE_405 = "405";
	public static final String ERROR_CODE_404 = "404";
	public static final String ERROR_CODE_400 = "400";
	public static final String INVALID_REQUEST_BODY = "Invalid request body: ";

	public static final String TRANSACTIONID = "transactionId";
	public static final String TRANSACTION_ID = "transaction_id";
	public static final String CORP_ID = "corp_id";
	public static final String CORPID = "corpId";
	public static final String SOURCE_SYSTEM = "sourceSystem";
	public static final String URLS = "urls";
	public static final String DEVICES = "devices";
	public static final String MDN = "mdn";
	public static final String POLICYID = "policyId";
	public static final String TYPE = "type";
	public static final String CARRIER = "carrier";
	public static final String MDN_IMEI_ADDRESS = "mdn/imei/ipAddress";


	public static final String HTTP_METHOD = "httpMethod";
	public static final String HTTP_GET = "GET";
	public static final String HTTP_PUT = "PUT";
	public static final String HTTP_POST = "POST";
	public static final String HTTP_DELETE = "DELETE";
	public static final String HTTP_ERROR_CODE = "HTTP Error code ";
	public static final String EVENT = "event";
	public static final String TO_POLICY = "toPolicy";
	public static final String PANO_URL = "panoUrl";
	public static final String SOURCE_TRANSACTION_TYPE = "sourceTransactionType";

	public static final String PANO_HEADER_KEY_TYPE = "X-PAN-KEY";
	public static final String ADD = "ADD";
	public static final String DELETE = "DELETE";

	public static final String DATETIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
	public final static SimpleDateFormat transactionDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final String ZONED_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	public static final String ENVIRONMENT = "ENVIRONMENT";
	public static final String JAVA_TOOL_OPTIONS = "JAVA_TOOL_OPTIONS";
	public static final String DATADOG_JAVA_AGENT_JAR = "-javaagent:\"/opt/java/lib/dd-java-agent.jar\"";
	public static final String DD_LOGS_INJECTION = "DD_LOGS_INJECTION";
	public static final String DD_JMXFETCH_ENABLED = "DD_JMXFETCH_ENABLED";
	public static final String DD_TRACE_ENABLED = "DD_TRACE_ENABLED";
	public static final String DD_ENV = "DD_ENV";
	public static final String DD_SERVICE = "DD_SERVICE";

//	public static boolean isValidFilteringPolicy (String test) {
//		if ("Open".equals(test) || "Deny All".equals(test)) {
//			return true;
//		} else {
//			return false;
//		}
//	}

}
