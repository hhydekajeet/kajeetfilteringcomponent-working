package com.kajeet.filteringcomponent.common.client.http.rest;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class RestClient {
	private static final Logger log = LogManager.getLogger(RestClient.class.getName());

	private static final String HTTP_AUTHORIZATION_HEADER = "Authorization";
	private static final String HTTP_BASIC_AUTH = "Basic";
	private static final String HTTP_OAUTH2 = "Bearer";
	private final SimpleDateFormat datetimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.S");

	private RestTemplate rest;
	private HttpHeaders headers;

	private final int connTimeout = 20000;
	private final int readTimeout = 20000;

	public RestClient() {
		init(connTimeout, readTimeout);
	}

	public RestClient(int connTimeout, int readTimeout) {
		init(connTimeout, readTimeout);
	}

	public RestClient(boolean useSSL){
		if(useSSL) {
			initSSL(connTimeout, readTimeout, null);
		} else {
			init(connTimeout, readTimeout);
		}
	}

	public RestClient(int connTimeout, int readTimeout, boolean useSSL, String certificatePassword) {
	    if(useSSL) {
	        initSSL(connTimeout, readTimeout, certificatePassword);
	    } else {
	        init(connTimeout, readTimeout);
        }
	}

	private void init(int connTimeout, int readTimeout) {
		log.debug("REST client initializing");

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(connTimeout);
		requestFactory.setConnectionRequestTimeout(connTimeout);
		requestFactory.setReadTimeout(readTimeout);

		this.rest = new RestTemplate(requestFactory);
		this.headers = new HttpHeaders();
//		this.headers.setContentType(MediaType.APPLICATION_JSON);

	}

    private void initSSL(int connTimeout, int readTimeout, String certificatePassword) {

		//This is for KFC DEV
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		SSLConnectionSocketFactory socketFactory = null;
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());

			socketFactory = new SSLConnectionSocketFactory(sc, NoopHostnameVerifier.INSTANCE);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}

		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		requestFactory.setConnectTimeout(connTimeout);
		requestFactory.setConnectionRequestTimeout(connTimeout);
		requestFactory.setReadTimeout(readTimeout);

		this.rest = new RestTemplate(requestFactory);
		this.headers = new HttpHeaders();
		this.headers.setContentType(MediaType.APPLICATION_JSON);

    }

	public HttpHeaders getHeaders() {
		return headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doGet(String url) {
		return doGet(url, null);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doGet(String url, Map<String, String> queryParams) {

		HttpEntity<String> requestEntity = new HttpEntity<>("", getHeaders());
		if (queryParams != null) {
			url = urlWithQueryParams(url, queryParams);
		}

		return invoke(HttpMethod.GET, url, requestEntity);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doPost(String url, String json) {
		return doPost(url, json, null);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doPost(String url, String json, Map<String, String> queryParams) {

		HttpEntity<String> requestEntity = new HttpEntity<>(json, getHeaders());

		if (queryParams != null) {
			url = urlWithQueryParams(url, queryParams);
		}

		return invoke(HttpMethod.POST, url, requestEntity);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doPut(String url, String json) {
		return doPut(url, json, null);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doPut(String url, String json, Map<String, String> queryParams) {

		HttpEntity<String> requestEntity = new HttpEntity<>(json, getHeaders());
		if (queryParams != null) {
			url = urlWithQueryParams(url, queryParams);
		}
		return invoke(HttpMethod.PUT, url, requestEntity);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doDelete(String url) {

		HttpEntity<String> requestEntity = new HttpEntity<>("", getHeaders());
		return invoke(HttpMethod.DELETE, url, requestEntity);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doDelete(String url, String json) {

		HttpEntity<String> requestEntity = new HttpEntity<>(json, getHeaders());
		return invoke(HttpMethod.DELETE, url, requestEntity);
	}

	public com.kajeet.filteringcomponent.common.client.http.rest.RestResponse doUpload(String url, byte[] bytes) {

		HttpEntity<byte[]> requestEntity = new HttpEntity<>(bytes, getHeaders());
		return invoke(HttpMethod.POST, url, requestEntity);
	}

	public void setBasicAuth(String username, String password) {
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64Utils.encode(auth.getBytes());
		String authHeader = HTTP_BASIC_AUTH + " " + new String(encodedAuth);

		this.headers.set(HTTP_AUTHORIZATION_HEADER, authHeader);
	}

	// TODO: use encryption library to enc/dec apikey
	public void setBasicAuth(String apikey) {
		String authHeader = HTTP_BASIC_AUTH + " " + apikey;
		this.headers.set(HTTP_AUTHORIZATION_HEADER, authHeader);
	}

	public void setCustomAuthHeader(String token) {
		this.headers.set(HTTP_AUTHORIZATION_HEADER, token);
	}
	

	public void setOAuth2(String token) {
		String authHeader = HTTP_OAUTH2 + " " + token;

		this.headers.set(HTTP_AUTHORIZATION_HEADER, authHeader);
	}

	private com.kajeet.filteringcomponent.common.client.http.rest.RestResponse invoke(HttpMethod method, String url, HttpEntity<?> requestEntity) {

		com.kajeet.filteringcomponent.common.client.http.rest.RestResponse response = new com.kajeet.filteringcomponent.common.client.http.rest.RestResponse();
		Long startTime = System.currentTimeMillis();
		try {
			ResponseEntity<String> responseEntity = rest.exchange(url, method, requestEntity, String.class);
			response.setHeaders(responseEntity.getHeaders());
			response.setStatus(responseEntity.getStatusCode());
			if (responseEntity.hasBody())
				response.setMessage(responseEntity.getBody());

		} catch (HttpStatusCodeException e) {
			// return this statuscode
			response.setStatus(e.getStatusCode());
			response.setMessage(e.getResponseBodyAsString());
		} catch (RestClientException e) {
			// Some other error occured, not related to an actual status code
			// return whatever we need to in this case
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setMessage(e.getLocalizedMessage());
		}
		logResponseTime(method.name(), url, startTime);

		if (log.isEnabled(Level.INFO)) {
			String body = requestEntity.hasBody() ? Objects.requireNonNull(requestEntity.getBody()).toString() : "";
			log.info(String.format("REST client invoke:  method=%s  url='%s'  body='%s'  response: %s", method.name(),
					url, body, response.toString()));
		}

		return response;
	}

	private void logResponseTime(String method, String url, Long startTime) {
		try {
			if (startTime != null) {
				long currentTime = System.currentTimeMillis();
				long executionTime = currentTime - startTime;
				log.info(String.format(
						"REST client logResponseTime:  method[%s]  url[%s]  startTime[%s]  endTime[%s] responseTime[%s ms]",
						method, url.replaceAll("username=(.*)&|password=(.*)", ""),
						datetimeFormat.format(new Date(startTime)), datetimeFormat.format(new Date(currentTime)),
						executionTime));
			}
		} catch (Exception e) {
		}
	}

	public static void trustSelfSignedSSL() {
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			X509TrustManager tm = new X509TrustManager() {

				public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			ctx.init(null, new TrustManager[]{tm}, null);
			SSLContext.setDefault(ctx);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private String urlWithQueryParams(String url, Map<String, String> queryParams){

		int paramNum = 0;
		for (String paramKey : queryParams.keySet()){
			String paramValue = queryParams.get(paramKey);

			if (paramNum == 0) {
				url = url + "?";
				paramNum++;
			}
			else {
				url = url + "&";
			}

			url = url +paramKey+"="+paramValue;
		}
		log.debug("URL with query params params: [{}]", url);

		return url;
	}
}