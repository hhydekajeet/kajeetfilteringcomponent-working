package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PanoramaCommitChanges {

    private static final Logger log = LogManager.getLogger(PanoramaCommitChanges.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("PanoramaCommitChanges.handleRequest()");

        log.info("Committing changes", input.getTransactionType());

        String panoIP = input.getPanoUrl();
        String key = input.getApiKey();
        String commandType = "commit";
        String command = "<commit></commit>";

        String getURL = String.format("https://"+panoIP+"/api/");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("type", commandType);
        queryParams.put("cmd", command);
        queryParams.put("key", key);

        RestClient rc = new RestClient(true);
        log.info("Calling api to commit changes");

        RestResponse response = rc.doGet(getURL, queryParams);
        if(response.getStatus().is2xxSuccessful()){
            log.debug("Success response: [{}]", response.getMessage());

            Document doc = Utility.convertStringToXMLDocument( response.getMessage() );
            Element resultElement = (Element) doc.getElementsByTagName("result").item(0);
            String jobId = resultElement.getElementsByTagName("job").item(0).getTextContent();
            log.info("Commit Job Id: [{}]", jobId);
            input.setStatus("YES");
            ArrayList<String> jobsToPoll = new ArrayList<>();
            jobsToPoll.add(jobId);
            input.setJobsToPoll(jobsToPoll);
        }
        else{
            throw new SystemException("Error committing changes");
        }
        return input;
    }
}
