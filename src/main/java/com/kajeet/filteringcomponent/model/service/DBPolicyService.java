package com.kajeet.filteringcomponent.model.service;

import com.kajeet.filteringcomponent.common.ConstantSQL;
import com.kajeet.filteringcomponent.common.ClusterInfo;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.dbmodel.VCustomUrl;
import com.kajeet.filteringcomponent.model.kfc.CorpPolicy;
import com.kajeet.filteringcomponent.model.kfc.CustomUrlApi;
import com.kajeet.filteringcomponent.dbmodel.WCustomUrl;
import com.kajeet.filteringcomponent.model.mapping.CorpPolicyMapping;
import com.kajeet.filteringcomponent.model.objects.Policies;
import com.kajeet.filteringcomponent.model.mapping.PolicyMapping;
import com.kajeet.filteringcomponent.model.objects.Policy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBPolicyService {
    private static final Logger log = LogManager.getLogger(DBPolicyService.class);

    public static Policies getAllPolicies (Connection conn) {
        Policies policies = new Policies();
        log.debug("Getting all policies from DB");
        List<Policy> policyList = getAllPoliciesFromDB(conn);
        policies.setPolicies(policyList);
        return policies;
    }

    public static Policy getPolicyByCorpId (Connection conn, String corpId) throws InvalidInputException, SystemException {
       log.debug("Getting policy for corp: {}", corpId);
       Policy policy = getPolicyForCorpFromDB(conn, corpId);
       return policy;
    }

    private static List<Policy> getAllPoliciesFromDB(Connection conn) {
        List<Policy> allPolicies = new ArrayList<>();
        Policy policy = null;
        String query = ConstantSQL.QUERY_ALL_POLICIES;
        log.debug("Query: {}", query);
        ResultSet resultSet = null;
        try {
            resultSet = JdbcService.getResultSet(conn, query);
            while (resultSet.next()) {
                policy = getPolicyFromRS(resultSet);
                allPolicies.add(policy);
            }
            log.debug("Number of Policies: {}", allPolicies.size());
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException("ERROR: Unable to close the database connection: " + e.getMessage());
        }
        return allPolicies;
    }

    private static Policy getPolicyForCorpFromDB(Connection conn, String corpId ) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.getPolicyForCorpFromDB('" + corpId + "')");
        Policy policy = null;
        String query = ConstantSQL.QUERY_CORP_BY_NAME;
        log.debug("Query: {}", query);
        ResultSet resultSet = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = corpId;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            while (resultSet.next()) {
                policy = getCorpPolicyFromRS(resultSet);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBPolicyService.getPolicyForCorpFromDB('" + corpId + "') threw SQLException: " + e.getMessage());
        }
        if (policy == null){
            log.debug("Corp: "+corpId+" - No policy found");
            throw new InvalidInputException("No policy found for corp " + corpId);
        }
        return policy;
    }

    private static Policy getPolicyFromRS(ResultSet resultSet) throws SQLException {
        log.debug("DBPolicyService.getPolicyFromRS()");
        Policy policy = new Policy();
        policy.setId(resultSet.getInt(PolicyMapping.ID.getDbColumnName()));
        policy.setName(resultSet.getString(PolicyMapping.NAME.getDbColumnName()));
        policy.setDescription(resultSet.getString(PolicyMapping.DESCRIPTION.getDbColumnName()));
        policy.setPaloAltoId(resultSet.getString(PolicyMapping.PALO_ALTO_ID.getDbColumnName()));
        policy.setCustom_filtering(resultSet.getString(PolicyMapping.CUSTOMER_FILTERING.getDbColumnName()));
        policy.setCreated_by(resultSet.getString(PolicyMapping.CREATED_BY.getDbColumnName()));
        policy.setCreatedOn(resultSet.getTimestamp(PolicyMapping.CREATED_ON.getDbColumnName()).toLocalDateTime());
        policy.setUpdated_by(resultSet.getString(PolicyMapping.UPDATED_BY.getDbColumnName()));
        policy.setUpdatedOn(resultSet.getTimestamp(PolicyMapping.UDATED_ON.getDbColumnName()).toLocalDateTime());
        return policy;
    }

//    private static List<VCustomUrl> selectVCustomUrls(Connection conn, String corpId) {
//        log.debug("DBPolicyService.selectVCustomUrls({})", corpId);
//        String query = ConstantSQL.QUERY_CUSTOM_URL_BY_CORP_ID;
//        log.debug("Query: {}", query);
//        ResultSet resultSet = null;
//        List<VCustomUrl> vCustomUrlList = null;
//        String[] paramTypes = new String[1];
//        String[] paramValues = new String[1];
//        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
//        paramValues[0] = corpId;
//        try {
//            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
//            vCustomUrlList = getListFromResultSet (resultSet, new VCustomUrl() );
//        } catch (SQLException e) {
//            log.error(e.getMessage());
//            throw new RuntimeException("ERROR: DBPolicyService.selectVCustomUrls() threw SQLException " + e.getMessage());
//        } finally {
//            JdbcService.closeResultSet(resultSet);
//        }
//        return vCustomUrlList;
//    }

    private static List<WCustomUrl> selectCustomUrls(Connection conn, String corpId) {
        log.debug("DBPolicyService.selectVCustomUrls({})", corpId);
        String query = ConstantSQL.QUERY_CUSTOM_URL_BY_CORP_NAME;
        log.debug("Query: {}", query);
        ResultSet resultSet = null;
        List<WCustomUrl> customUrlList = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = corpId;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            customUrlList = getListFromResultSet (resultSet, new WCustomUrl() );
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException("ERROR: DBPolicyService.selectVCustomUrls() threw SQLException " + e.getMessage());
        } finally {
            JdbcService.closeResultSet(resultSet);
        }
        return customUrlList;
    }

//    public static CustomUrl getCustomUrl (Connection conn, String corpId) {
//        log.debug("DBPolicyService.getCustomUrl({})", corpId);
//        CustomUrl customUrl = new CustomUrl();
//        String url = null;
//        List<String> urlList = new ArrayList<>();
//        if (conn != null) {
//            List<VCustomUrl> customUrlList = selectVCustomUrls(conn, corpId);
//            for (VCustomUrl vCustomUrl: customUrlList) {
//                customUrl.setCorpId(vCustomUrl.getCorp_name());
//                customUrl.setType(vCustomUrl.getCustom_filtering_type());
//                url = vCustomUrl.getName();
//                if (url != null) urlList.add(url);
//            }
//            customUrl.setUrls(urlList);
//        }
//        return customUrl;
//    }

    public static CustomUrlApi getCustomUrl (Connection conn, String corpId) {
        log.debug("DBPolicyService.getCustomUrl({})", corpId);
        CustomUrlApi customUrlApi = new CustomUrlApi();
        String url = null;
        List<String> urlList = new ArrayList<>();
        if (conn != null) {
            List<WCustomUrl> customUrlList = selectCustomUrls(conn, corpId);
            for (WCustomUrl customUrl: customUrlList) {
                customUrlApi.setCorpId(customUrl.getCorpName());
                customUrlApi.setType(customUrl.getSiteAccessTypeShortName());
                url = customUrl.getName();
                if (url != null) urlList.add(url);
            }
            customUrlApi.setUrls(urlList);
        }
        return customUrlApi;
    }

//    private static List<VCustomUrl> getListFromResultSet (ResultSet resultSet, VCustomUrl vCustomUrlToken ) throws SQLException {
//        log.debug("DBPolicyService.getListFromResultSet()");
//        VCustomUrl vCustomUrl = null;
//        List<VCustomUrl> allVCustomUrls = new ArrayList<>();
//        while (resultSet.next()) {
//            vCustomUrl = new VCustomUrl();
//            vCustomUrl.setId(resultSet.getInt(VCustomUrl.ID));
//            vCustomUrl.setName(resultSet.getString(VCustomUrl.NAME));
//            vCustomUrl.setCorp_id(resultSet.getInt(VCustomUrl.CORP_ID));
//            vCustomUrl.setCorp_name(resultSet.getString(VCustomUrl.CORP_NAME));
//            vCustomUrl.setPolicy_name(resultSet.getString(VCustomUrl.POLICY_NAME));
//            vCustomUrl.setCustom_filtering_type(resultSet.getString(VCustomUrl.CUSTOM_FILTERING_TYPE));
//            allVCustomUrls.add(vCustomUrl);
//        }
//        return allVCustomUrls;
//    }

    private static List<WCustomUrl> getListFromResultSet (ResultSet resultSet, WCustomUrl customUrlToken ) throws SQLException {
        log.debug("DBPolicyService.getListFromResultSet(CustomUrl)");
        WCustomUrl customUrl = null;
        List<WCustomUrl> allCustomUrls = new ArrayList<>();
        while (resultSet.next()) {
            customUrl = new WCustomUrl();
            customUrl.setName(resultSet.getString(WCustomUrl.NAME));
            customUrl.setCorpName(resultSet.getString(WCustomUrl.CORP_NAME));
            customUrl.setPolicyName(resultSet.getString(WCustomUrl.POLICY_NAME));
            customUrl.setUrlFilterName(resultSet.getString(WCustomUrl.URL_FILTER_NAME));
            customUrl.setUrlListName(resultSet.getString(WCustomUrl.URL_LIST_NAME));
            customUrl.setSiteAccessTypeShortName(resultSet.getString(WCustomUrl.SITE_ACCESS_TYPE_SHORT_NAME));
            customUrl.setSiteAccessTypeLongName(resultSet.getString(WCustomUrl.SITE_ACCESS_TYPE_LONG_NAME));
            allCustomUrls.add(customUrl);
        }
        return allCustomUrls;
    }

//    public static Integer getUrlFilterIdByCorpName (Connection conn, String corpId) throws InvalidInputException, SystemException {
//        log.debug("DBPolicyService.getUrlFilterIdByCorpName({})", corpId);
//        String query = ConstantSQL.SELECT_URL_FILTER_ID_BY_CORP;
//        log.debug("Query: {}", query);
//        return JdbcService.getUniqueIntFieldByStringValue(conn, query, ConstantSQL.ID, corpId);
//    }

    public static String getUrlFilterNameByCorpName (Connection conn, String corpId) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.getUrlFilterNameByCorpName({})", corpId);
        String query = ConstantSQL.SELECT_URL_FILTER_NAME_BY_CORP;
        log.debug("Query: {}", query);
        return JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.NAME, corpId);
    }

//    public static Integer getCustomPolicyIdByCorpName (Connection conn, String corpId) throws InvalidInputException, SystemException {
//        log.debug("DBPolicyService.getCustomPolicyIdByCorpName({})", corpId);
//        String query = ConstantSQL.SELECT_CUSTOM_POLICY_ID_BY_CORP;
//        log.debug("Query: {}", query);
//        return JdbcService.getUniqueIntFieldByStringValue(conn, query, ConstantSQL.ID, corpId);
//    }

    public static String getCustomPolicyNameByCorpName (Connection conn, String corpId) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.getCustomPolicyNameByCorpName({})", corpId);
        String query = ConstantSQL.SELECT_CUSTOM_POLICY_NAME_BY_CORP;
        log.debug("Query: {}", query);
        return JdbcService.getUniqueStringFieldByStringValue(conn, query, ConstantSQL.NAME, corpId);
    }

    public static Integer getSiteAccessIdByName (Connection conn, String siteAccessName) {
        log.debug("DBPolicyService.getSiteAccessIdByName({})", siteAccessName);
        String query = ConstantSQL.SELECT_SITE_ACCESS_TYPE_ID_BY_SHORT_NAME;
        log.debug("Query: {}", query);
        Integer siteAccessId = null;
        try {
            siteAccessId = JdbcService.getUniqueIntFieldByStringValue(conn, query, ConstantSQL.ID, siteAccessName);
        } catch (Exception x) {
            String errorMessage = "ERROR: DBPolicyService.insertCustomPolicyByCorpName() threw SQLException " +x.getMessage();
            log.error(errorMessage);
            throw new RuntimeException(errorMessage);
        }
        return siteAccessId;
    }

    public static int countUrlListsByFilter (Connection conn, String urlFilterName) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.countUrlListsByFilter({})", urlFilterName);
        String query = ConstantSQL.SELECT_COUNT_URL_LISTS_BY_FILTER;
        log.debug("Query: {}", query);
        return JdbcService.getUniqueIntFieldByStringValue(conn, query, ConstantSQL.COUNT_THEM, urlFilterName);
    }

    public static String insertCustomPolicyByCorpName (Connection conn, CustomUrlApi customUrl) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.insertCustomPolicyByCorpName({})", customUrl);
        String insertQuery = ConstantSQL.INSERT_CUSTOM_POLICY_FOR_CORP;
        log.debug("Query: {}", insertQuery);
        String customPolicyName = customUrl.getCorpId()+ "-Custom-Policy";
        String[] paramTypes = new String[4];
        String[] paramValues = new String[4];
        for (int x=0; x<4; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = customUrl.getCorpId();
        paramValues[1] = customUrl.getType();
        paramValues[2] = customPolicyName;
        paramValues[3] = ConstantSQL.FLAG_YES;
//        paramValues[4] = customUrl.getType(); // custom_filtering_type is deprecated, replaced by site_access_type

        int rowsAffected = 0;
        boolean somethingWentWrong = false;
        String errorMessage = "";
        try {
            rowsAffected = JdbcService.executeDml(conn, insertQuery, paramTypes, paramValues);
            if (rowsAffected != 1) {
                JdbcService.rollbackTransaction(conn);
                errorMessage = "DBPolicyService.insertCustomPolicyByCorpName () should have gotten SQL statement result of 1 but instead got " + rowsAffected;
                log.error(errorMessage);
                throw new RuntimeException(errorMessage);
            }
        } catch (SQLException e) {
            somethingWentWrong = true;
            log.error("DBPolicyService.insertCustomPolicyByCorpName() threw exception: {} ", e.getMessage());
            if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_DUPLICATE)) {
                errorMessage = "Duplicate Policy: " + customPolicyName;
            } else {
                errorMessage = "ERROR: DBPolicyService.insertCustomPolicyByCorpName() threw SQLException " +e.getMessage();
            }
        }
        if (somethingWentWrong) {
            JdbcService.rollbackTransaction(conn);
            throw new InvalidInputException(errorMessage);
        } else {
            JdbcService.commitTransaction(conn);
        }
        return customPolicyName;
    }

    public static String insertUrlFilter (Connection conn, String customPolicyName, CustomUrlApi customUrl) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.insertUrlFilter({})", customUrl);
        String insertQuery = ConstantSQL.INSERT_URL_FILTER;
        log.debug("Query: {}", insertQuery);
        String urlFilterName = customPolicyName + "-URL-Filter";
        String[] paramTypes = new String[3];
        String[] paramValues = new String[3];
        for (int x=0; x<3; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = customPolicyName;
        paramValues[1] = customUrl.getCorpId();
        paramValues[2] = urlFilterName;
//        paramValues[3] = ConstantSQL.FLAG_YES;

        int rowsAffected = 0;
        boolean somethingWentWrong = false;
        String errorMessage = "";
        try {
            rowsAffected = JdbcService.executeDml(conn, insertQuery, paramTypes, paramValues);
            if (rowsAffected != 1) {
                JdbcService.rollbackTransaction(conn);
                throw new RuntimeException("ERROR: DBPolicyService.insertUrlFilter () should have gotten SQL statement result of 1 but instead got " + rowsAffected);
            }
        } catch (SQLException e) {
            somethingWentWrong = true;
            log.error("DBPolicyService.insertUrlFilter() threw exception: {} ", e.getMessage());
            if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_DUPLICATE)) {
                errorMessage = "Duplicate URL Filter: " + urlFilterName;
            } else {
                errorMessage = "ERROR: DBPolicyService.insertUrlFilter() threw SQLException " +e.getMessage();
            }
        }
        if (somethingWentWrong) {
            JdbcService.rollbackTransaction(conn);
            throw new InvalidInputException(errorMessage);
        } else {
            JdbcService.commitTransaction(conn);
        }
        return urlFilterName;
    }

    public static String insertUrlList (Connection conn, String urlFilterName, CustomUrlApi customUrl) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.insertUrlList({})", customUrl);
        int countLists = countUrlListsByFilter(conn, urlFilterName);

        String insertQuery = ConstantSQL.INSERT_URL_LIST;
        log.debug("Query: {}", insertQuery);
        String urlListName = urlFilterName + "-List-" + ++countLists;
        String[] paramTypes = new String[4];
        String[] paramValues = new String[4];
        for (int x=0; x<4; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = urlFilterName;
        paramValues[1] = customUrl.getType();
        paramValues[2] = customUrl.getCorpId();
        paramValues[3] = urlListName;

        int rowsAffected = 0;
        boolean somethingWentWrong = false;
        String errorMessage = "";
        try {
            rowsAffected = JdbcService.executeDml(conn, insertQuery, paramTypes, paramValues);
            if (rowsAffected != 1) {
                JdbcService.rollbackTransaction(conn);
                throw new RuntimeException("ERROR: DBPolicyService.insertUrlList () should have gotten SQL statement result of 1 but instead got " + rowsAffected);
            }
        } catch (SQLException e) {
            somethingWentWrong = true;
            log.error("DBPolicyService.insertUrlList() threw exception: {} ", e.getMessage());
            if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_DUPLICATE)) {
                errorMessage = "Duplicate URL Filter: " + urlFilterName;
            } else {
                errorMessage = "ERROR: DBPolicyService.insertUrlList() threw SQLException " +e.getMessage();
            }
        }
        if (somethingWentWrong) {
            JdbcService.rollbackTransaction(conn);
            throw new InvalidInputException(errorMessage);
        } else {
            JdbcService.commitTransaction(conn);
        }
        return urlListName;
    }

    private static CustomUrlApi insertCustomUrls (Connection conn, String urlListName, CustomUrlApi customUrl) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.insertCustomUrls({})", customUrl);
        if (conn != null) {
            String query = ConstantSQL.INSERT_CUSTOM_URL_BY_LIST;
            log.debug("Query: {}", query);
            String[] paramTypes = new String[3];
            String[] paramValues = new String[3];
            for (int x=0; x<3; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
            paramValues[0] = urlListName;
            paramValues[1] = customUrl.getCorpId();
            int rowsAffected = 0;
            boolean somethingWentWrong = false;
            StringBuilder errorMessage = new StringBuilder();
            for (String url: customUrl.getUrls()) {
                try {
                    paramValues[2] = url;
                    log.debug("ulrListName: {}, corpId: {}, url: {}", paramValues[0], paramValues[1], paramValues[2] );
                    rowsAffected = JdbcService.executeDml(conn, query, paramTypes, paramValues);
                    if (rowsAffected != 1) {
                        JdbcService.rollbackTransaction(conn);
                        throw new RuntimeException("ERROR: DBPolicyService.insertCustomUrls() should have gotten SQL statement result of 1 but instead got " + rowsAffected);
                    }
                } catch (SQLException e) {
                    somethingWentWrong = true; // But continue iterating for now.
                    log.error("DBPolicyService.insertCustomUrls() threw exception: {} ", e.getMessage());
                    if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_DUPLICATE)) {
                        errorMessage.append("; Duplicate Url: " + url);
                    } else if (e.getMessage().toLowerCase().contains(ConstantSQL.MYSQL_ERR_FK_VIOLATION) && e.getMessage().toLowerCase().contains(ConstantSQL.CUSTOM_FILTERING)) {
                        errorMessage.append("; Custom Urls not permitted for corp " + customUrl.getCorpId() + " -- check Policy");
                    } else {
                        errorMessage.append("; ERROR: DBPolicyService.insertCustomUrls() threw SQLException " +e.getMessage());
                        break;
                    }
                }
            }
            if (somethingWentWrong) {
                JdbcService.rollbackTransaction(conn);
                throw new InvalidInputException(errorMessage.toString().substring(2));
            } else {
                JdbcService.commitTransaction(conn);
            }
        }
        return customUrl;

    }

    public static CustomUrlApi putCustomUrl (Connection conn, CustomUrlApi customUrl) throws InvalidInputException, SystemException {
        log.debug("DBPolicyService.putCustomUrl({})", customUrl);
        /*
        PIPE-762
        Look up UrlFilter for Corp
          If it doesn't exist
            Look up custom Policy for Corp
              If it doesn't exist, create/insert custom Policy
            Create/insert UrlFilter
        Create UrlList under corp, custom Policy and UrlFilter, set siteAccessType
        Create/insert customUrls
         */
        String urlFilterName = getUrlFilterNameByCorpName (conn, customUrl.getCorpId());
        if (urlFilterName==null) {
            String customPolicyName = getCustomPolicyNameByCorpName (conn, customUrl.getCorpId());
            if (customPolicyName==null){
                customPolicyName = insertCustomPolicyByCorpName (conn, customUrl);
            }
            urlFilterName = insertUrlFilter (conn, customPolicyName, customUrl);
        }
        String urlListName = insertUrlList (conn, urlFilterName, customUrl);
        return insertCustomUrls (conn, urlListName, customUrl);
    }


    private static Policy getCorpPolicyFromRS(ResultSet resultSet) throws SQLException {
        Policy policy = new Policy();
        policy.setId(resultSet.getInt(CorpPolicyMapping.POLICY_ID.getDbColumnName()));
        policy.setName(resultSet.getString(CorpPolicyMapping.POLICY_NAME.getDbColumnName()));
        policy.setPaloAltoId(resultSet.getString(CorpPolicyMapping.PALO_ALTO_ID.getDbColumnName()));
        policy.setDescription(resultSet.getString(CorpPolicyMapping.POLICY_DESCRIPTION.getDbColumnName()));
        policy.setCustom_filtering(resultSet.getString(CorpPolicyMapping.CUSTOMER_FILTERING.getDbColumnName()));
        policy.setCreated_by(resultSet.getString(PolicyMapping.CREATED_BY.getDbColumnName()));
        policy.setCreatedOn(resultSet.getTimestamp(PolicyMapping.CREATED_ON.getDbColumnName()).toLocalDateTime());
        policy.setUpdated_by(resultSet.getString(PolicyMapping.UPDATED_BY.getDbColumnName()));
        policy.setUpdatedOn(resultSet.getTimestamp(PolicyMapping.UDATED_ON.getDbColumnName()).toLocalDateTime());
        return policy;
    }

    private static int copyCorpCustomUrlsToHistory (Connection conn, CorpPolicy corpPolicy) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.copyCorpCustomUrlsToHistory({})", corpPolicy.getCorpId());
        String insertHistoryDml = ConstantSQL.INSERT_CUSTOM_URL_HISTORY_BY_CORP_NAME;
        log.debug("Query: {}", insertHistoryDml);
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = corpPolicy.getCorpId();
        int rowsAffected = 0;
        try {
            rowsAffected = JdbcService.executeDml(conn, insertHistoryDml, paramTypes, paramValues);
//            JdbcService.coMmmitTransaction(conn); // DO NOT COMMIT TRANSACTION HERE
        } catch (SQLException e) {
            JdbcService.rollbackTransaction(conn);
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBPolicyService.copyCorpCustomUrlsToHistory() threw SQLException " + e.getMessage());
        }
        log.debug("DBPolicyService.copyCorpCustomUrlsToHistory({}) returning {}", corpPolicy.getCorpId(), rowsAffected);
        return rowsAffected;
    }

    private static int copyCustomUrlsToHistory (Connection conn, CustomUrlApi customUrl) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.copyCustomUrlsToHistory({})", customUrl.getCorpId());
        String insertHistoryDml = ConstantSQL.INSERT_CUSTOM_URL_HISTORY_BY_CORP_AND_NAME;
        log.debug("Query: {}", insertHistoryDml);
        String[] paramTypes = new String[2];
        String[] paramValues = new String[2];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramTypes[1] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = customUrl.getCorpId();
        int rowsAffected = 0;
        try {
            for (String url: customUrl.getUrls()) {
                paramValues[1] = url;
                rowsAffected += JdbcService.executeDml(conn, insertHistoryDml, paramTypes, paramValues);
//            JdbcService.coMmmitTransaction(conn); // DO NOT COMMIT TRANSACTION HERE
            }
        } catch (SQLException e) {
            JdbcService.rollbackTransaction(conn);
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBPolicyService.copyCustomUrlsToHistory() threw SQLException " + e.getMessage());
        }
        log.debug("DBPolicyService.copyCustomUrlsToHistory({}) returning {}", customUrl.getCorpId(), rowsAffected);
        return rowsAffected;
    }

    public static int deleteCustomUrls (Connection conn, CorpPolicy corpPolicy) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.deleteCustomUrls({})", corpPolicy.getCorpId());
        copyCorpCustomUrlsToHistory(conn, corpPolicy);
        List<WCustomUrl> wCustomUrlList = selectCustomUrls(conn, corpPolicy.getCorpId());
        String deleteDml = ConstantSQL.DELETE_CUSTOM_URL_BY_CORP_AND_NAME;
        log.debug("Query: {}", deleteDml);
        String[] paramTypes = new String[2];
        String[] paramValues = new String[2];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramTypes[1] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = corpPolicy.getCorpId();
        int totalRowsAffected = 0;
        int rowsAffected = 0;
        for (WCustomUrl wCustomUrl: wCustomUrlList) {
            if (wCustomUrl.getName()==null) continue;
            paramValues[1] = wCustomUrl.getName();
            try {
                rowsAffected = JdbcService.executeDml(conn, deleteDml, paramTypes, paramValues);
                totalRowsAffected += rowsAffected;
                if (rowsAffected != 1) {
                    JdbcService.rollbackTransaction(conn);
                    throw new InvalidInputException("ERROR: DBPolicyService.deleteCustomUrls() should have gotten SQL statement result of 1 but instead got " + rowsAffected + "; Corp '" + corpPolicy.getCorpId() + "' and/or Policy'" + corpPolicy.getPolicyId() + "' do not exist. transactionId = " + corpPolicy.getTransactionId());
                }
//                JdbcService.comMmitTransaction(conn); // DO NOT COMMIT TRANSACTION HERE
            } catch (SQLException e) {
                JdbcService.rollbackTransaction(conn);
                log.error(e.getMessage());
                throw new SystemException("ERROR: DBPolicyService.postCorpPolicy() threw SQLException " + e.getMessage());
            }
        }
        int count_recs = 0;
        log.debug("DBPolicyService.deleteCustomUrls({}) returning {}", corpPolicy.getCorpId(), totalRowsAffected);
        return totalRowsAffected;
    }

    public static int deleteCustomUrls (Connection conn, CustomUrlApi customUrl) throws SystemException, InvalidInputException {
        log.debug("DBPolicyService.deleteCustomUrls({})", customUrl.getCorpId());
        copyCustomUrlsToHistory (conn, customUrl);
        String deleteDml = ConstantSQL.DELETE_CUSTOM_URL_BY_CORP_AND_NAME;
        log.debug("Query: {}", deleteDml);
        String[] paramTypes = new String[2];
        String[] paramValues = new String[2];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramTypes[1] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = customUrl.getCorpId();
        int totalRowsAffected = 0;
        int rowsAffected = 0;
        int errorCount = 0;
        boolean errorsOccurred = false;
        String errorUrlList = "";
        for (String url: customUrl.getUrls()) {
            if (url==null) continue;
            try {
                paramValues[1] = url;
                rowsAffected = JdbcService.executeDml(conn, deleteDml, paramTypes, paramValues);
                totalRowsAffected += rowsAffected;
                if (rowsAffected != 1) {
                    errorsOccurred = true;
                    errorCount++;
                    errorUrlList += ", " + url;
                    if (errorCount > 9) break;
                }
            } catch (SQLException e) {
                JdbcService.rollbackTransaction(conn);
                log.error(e.getMessage());
                throw new SystemException("ERROR: DBPolicyService.postCorpPolicy() threw SQLException " + e.getMessage());
            }
        }
        if (errorsOccurred) {
            JdbcService.rollbackTransaction(conn);
            String PLURAL = (errorCount>1?"s":"");
            String LIMIT = (errorCount>9?" (Gave up after 10; may be more)":"");
            String EXISTENCE = (errorCount>1?" do not exist.":" does not exist.");
            throw new InvalidInputException("Invalid Delete request: URL" + PLURAL + errorUrlList.substring(1) + EXISTENCE + LIMIT);
        }
        log.debug("DBPolicyService.deleteCustomUrls([customUrl] corp = {}) returning {}", customUrl.getCorpId(), totalRowsAffected);
        return totalRowsAffected;
    }

    public static CorpPolicy postCorpPolicy (Connection conn, CorpPolicy corpPolicy) throws InvalidInputException, SystemException {
        log.debug("[public] DBPolicyService.postCorpPolicy('" + corpPolicy.getCorpId() + "', " + corpPolicy.getPolicyId() + "'); transactionId = " + corpPolicy.getTransactionId());
        Policy policy = getPolicyForCorpFromDB(conn, corpPolicy.getCorpId());
        if (policy.getName().equals(corpPolicy.getPolicyId())) {
            throw new InvalidInputException("Policy for corp " + corpPolicy.getCorpId() + " is already " + corpPolicy.getPolicyId());
        }
        deleteCustomUrls (conn, corpPolicy);
        return postCorpPolicy (conn, corpPolicy, (String)null);
    }


    private static CorpPolicy postCorpPolicy (Connection conn, CorpPolicy corpPolicy, String dummy) throws InvalidInputException, SystemException {
        log.debug("[private] DBPolicyService.postCorpPolicy(corp {}, policy {})", corpPolicy.getCorpId(), corpPolicy.getPolicyId());
        String updateDml = ConstantSQL.UPDATE_CORP_POLICY;
        log.debug("Query: {}", updateDml);
        int policyId = JdbcService.getIdFromName(conn, corpPolicy.getPolicyId(), ConstantSQL.DB_OBJECT_POLICY);
        if (policyId<0) {
            throw new InvalidInputException("Policy " + corpPolicy.getPolicyId() + "' does not exist.");
        }
        String[] paramTypes = new String[3];
        String[] paramValues = new String[3];
        paramTypes[0] = JdbcService.PARAM_TYPE_INT;
        paramTypes[1] = JdbcService.PARAM_TYPE_INT;
        paramTypes[2] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = ""+policyId;
        paramValues[1] = ""+policyId;
//        paramValues[1] = custom_filtering;
        paramValues[2] = corpPolicy.getCorpId();
        int rowsAffected = 0;
        try {
            rowsAffected = JdbcService.executeDml(conn, updateDml, paramTypes, paramValues);
            if (rowsAffected != 1) {
                JdbcService.rollbackTransaction(conn);
                throw new InvalidInputException("ERROR: DBPolicyService.postCorpPolicy() should have gotten SQL statement result of 1 but instead got " + rowsAffected + "; Corp '" + corpPolicy.getCorpId() + "' and/or Policy'" + corpPolicy.getPolicyId() + "' do not exist. transactionId = " + corpPolicy.getTransactionId());
            }
            JdbcService.commitTransaction(conn);
        } catch (SQLException e) {
            JdbcService.rollbackTransaction(conn);
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBPolicyService.postCorpPolicy() threw SQLException " + e.getMessage());
        }
        Policy policy = getPolicyForCorpFromDB(conn, corpPolicy.getCorpId());
        if (policy == null || (!policy.getName().equals(corpPolicy.getPolicyId()))) {
            throw new SystemException("Something went wrong attempting to update policy for corp: Expected '" + corpPolicy.getPolicyId() + "' but got '" + (policy==null?"null":policy.getName()) + "; . transactionId = " + corpPolicy.getTransactionId());
        }
        return corpPolicy;
    }

    public static List<ClusterInfo> getClusterInfoListByCorp (Connection conn, String corpId) throws SystemException {
        log.debug("DBPolicyService.getClusterInfoListByCorp('" + corpId + "'");
        List<ClusterInfo> clusterInfoList = new ArrayList<>();
        String query = ConstantSQL.SELECT_CLUSTERS_AND_ADDRESS_GROUPS_BY_CORP;
        log.debug("Query: {}", query);
        ResultSet resultSet = null;
        String clusterName = null;
        String previousClusterName = null;
        String addressGroupName = null;
        ClusterInfo clusterInfo = null;
        List<String> addressGroups = null;
        int recCount = 0;

        try {
            resultSet = JdbcService.getResultSet(conn, query, JdbcService.PARAM_TYPE_STRING, corpId);
            while (resultSet.next()) {
                recCount++;
                clusterName = resultSet.getString(ConstantSQL.DB_CLUSTER_NAME);
                if (previousClusterName==null || (!previousClusterName.equals(clusterName))) { // Processing the first record of a Cluster
                    // Close out previous Cluster
                    if (addressGroups != null && clusterInfo != null) {
                        clusterInfo.setAddressGroups(addressGroups);
                        clusterInfoList.add(clusterInfo);
                    }
                    // END Close out previous Cluster

                    clusterInfo = new ClusterInfo();
                    addressGroups = new ArrayList<>();
                    previousClusterName = clusterName;
                    clusterInfo.setClusterName(clusterName);
                }
                addressGroups.add(resultSet.getString(ConstantSQL.DB_ADDRESS_GROUP_NAME));
            }
            if (recCount==0) {
                addressGroups = new ArrayList<>();
                clusterInfo = new ClusterInfo();
            }
            clusterInfo.setAddressGroups(addressGroups);
            clusterInfoList.add(clusterInfo);
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: DBPolicyService.getClusterInfoListByCorp('" + corpId + "') threw SQLException: " + e.getMessage());
        }
        return clusterInfoList;
    }

}
