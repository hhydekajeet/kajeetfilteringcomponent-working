package com.kajeet.filteringcomponent.model.service;

import com.kajeet.filteringcomponent.common.ConstantSQL;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import com.kajeet.filteringcomponent.model.objects.OneKeyValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Map;

public class JdbcService {
    private static final Logger log = LogManager.getLogger(JdbcService.class);
    public static final String PARAM_TYPE_INT = "int";
    public static final String PARAM_TYPE_STRING = "String";


    public static Connection getConnection(String url, String user, String password) throws SQLException {
        return getConnection(url, user, password, true);
    }
    public static Connection getConnection(String url, String user, String password, boolean autocommit) throws SQLException {
        Connection conn = null;
        try {
          conn = DriverManager.getConnection(url, user, password);
          log.debug("Got connection to the DB");
        } catch (SQLException x) {
            log.error("JdbcService.getConnection() FAILED! " + x.getMessage());
            throw x;
        }
        conn.setAutoCommit(autocommit);
        return conn;
    }

    public static int executeDml (Connection conn, String query, String[] paramTypes, String[] paramValues) throws SQLException {
        PreparedStatement preparedStmt = prepareStatement (conn, query, paramTypes, paramValues);
        return preparedStmt.executeUpdate();
    }

    private static PreparedStatement prepareStatement (Connection conn, String query, String[] paramTypes, String[] paramValues) throws SQLException {
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        if (paramTypes != null && paramValues != null) {
            if (validArrays(paramTypes, paramValues)) {
                for (int i = 0; i < paramTypes.length; i++) {
                    switch (paramTypes[i]) {
                        case PARAM_TYPE_INT:
                            preparedStmt.setInt(i + 1, Integer.parseInt(paramValues[i]));
                            break;
                        case PARAM_TYPE_STRING:
                            preparedStmt.setString(i + 1, paramValues[i]);
                            break;
                    }
                }
            }
        }
        return preparedStmt;
    }


    public static ResultSet getResultSet (Connection conn, String query) throws SQLException {
        return getResultSet (conn, query, (String[])null, (String[])null);
    }

    public static ResultSet getResultSet (Connection conn, String query, String[] paramTypes, String[] paramValues) throws SQLException {
        PreparedStatement preparedStmt = prepareStatement (conn, query, paramTypes, paramValues);
        ResultSet resultSet = preparedStmt.executeQuery();
        return resultSet;
    }

    public static ResultSet getResultSet (Connection conn, String query, String paramType, String paramValue) throws SQLException {
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = paramType;
        paramValues[0] = paramValue;
        PreparedStatement preparedStmt = prepareStatement (conn, query, paramTypes, paramValues);
        ResultSet resultSet = preparedStmt.executeQuery();
        return resultSet;
    }

    private static boolean validArrays (String[] array1, String[] array2){
        if (array1 != null && array2 != null)
            if(array1.length>0 && array2.length>0)
                if(array1.length==array2.length)
                    return true;
        return false;
    }

    public static int getIdFromName (Connection conn, String nameVal, String dbObject) {
        String query = "select id from " + dbObject + " where name = ?";
        int returnId = -1;
        ResultSet resultSet = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = nameVal;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            while (resultSet.next()) {
                returnId = resultSet.getInt(ConstantSQL.ID);
                break;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException("ERROR: JdbcService.getIdFromName() threw SQLException " + e.getMessage());
        } finally {
            closeResultSet(resultSet);
        }
        return returnId;
    }

    public static String getUniqueStringFieldByStringValue (Connection conn, String query, String fieldName, String criterion) throws SystemException, InvalidInputException {
        // The query should return a single row (more might cause result to be indeterminate)
        //    and MUST return the named field
        //    and MUST define one and only one "field equals" criterion/WHERE clause condition
        log.debug("JdbcService.getUniqueStringFieldByStringValue()");
        log.debug("query = " + query + "; -- ?='" + criterion+"'");
//        if (!(query.toLowerCase().contains(fieldName.toLowerCase()))) {
//            throw new InvalidInputException ("ERROR: JdbcService.getOneField() threw Exception: Query '" + query + "' does not mention " + fieldName + ".");
//        }
        String returnVal = null;
        ResultSet resultSet = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = criterion;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            while (resultSet.next()) {
                returnVal = resultSet.getString(fieldName);
                break;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: JdbcService.getUniqueStringFieldByStringValue() threw SQLException " + e.getMessage());
        } finally {
            closeResultSet(resultSet);
        }
        log.debug("JdbcService.getUniqueStringFieldByStringValue() returning '" + returnVal + "'");
        return returnVal;
    }

    public static Integer getUniqueIntFieldByStringValue (Connection conn, String query, String fieldName, String criterion) throws SystemException, InvalidInputException {
        // The query should return a single row (more will be ignored)
        //    and MUST return the named field
        //    and MUST define one and only one "field equals" criterion/WHERE clause condition
//        if (!(query.toLowerCase().contains(fieldName.toLowerCase()))) {
//            throw new InvalidInputException("ERROR: JdbcService.getUniqueIntFieldByStringValue() threw Exception: Query '" + query + "' does not mention " + fieldName + ".");
//        }
        ResultSet resultSet = null;
        String[] paramTypes = new String[1];
        String[] paramValues = new String[1];
        paramTypes[0] = JdbcService.PARAM_TYPE_STRING;
        paramValues[0] = criterion;
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, paramValues);
            while (resultSet.next()) {
                return resultSet.getInt(fieldName);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: JdbcService.getUniqueIntFieldByStringValue() threw SQLException " + e.getMessage());
        } finally {
            closeResultSet(resultSet);
        }
        return null;
    }

    public static OneKeyValue getUniqueIntAndStringFieldByStringValue (Connection conn, String query, String[] fieldNames, String criterion) throws SystemException, InvalidInputException {
        String[] criteria = new String[1];
        criteria[0] = criterion;
        return getUniqueIntAndStringFieldByStringValues (conn, query, fieldNames, criteria);
    }

        public static OneKeyValue getUniqueIntAndStringFieldByStringValues (Connection conn, String query, String[] fieldNames, String[] criteria) throws SystemException, InvalidInputException {
        // The query should return a single row (more will be ignored)
        //    and MUST return BOTH the (2) named fields, the first of which must be convertible to int
        //    and MUST define exactly as many "field equals" conditions as there are elements in the criteria array
        if (fieldNames == null || fieldNames.length != 2 || fieldNames[0] == null || fieldNames[1] == null) {
            throw new SystemException("ERROR: JdbcService.getUniqueIntAndStringFieldByStringValue() got a bad input 'fieldNames'");
        }
        ResultSet resultSet = null;
        String[] paramTypes = new String[criteria.length];
        for (int i=0; i<criteria.length; i++) paramTypes[i] = JdbcService.PARAM_TYPE_STRING;
        OneKeyValue keyValue = new OneKeyValue();
        try {
            resultSet = JdbcService.getResultSet(conn, query, paramTypes, criteria);
            while (resultSet.next()) {
                keyValue.setKey(resultSet.getInt(fieldNames[0]));
                keyValue.setValue(resultSet.getString(fieldNames[1]));
                break;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new SystemException("ERROR: JdbcService.getUniqueIntAndStringFieldByStringValues() threw SQLException " + e.getMessage());
        } finally {
            closeResultSet(resultSet);
        }
        return keyValue;
    }

    public static void closeResultSet(ResultSet resultSet)  {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
//                throw new MethodNotAllowed405Exception("ERROR: JdbcService.closeResultSet() threw SQLException " + e.getMessage());
            }
        }
    }

    public static int postSourceTransaction (Connection conn, String transactionId, String source, SourceTransactionType type, String status, String requestJson) throws SystemException {
        log.debug("JdbcService.postSourceTransaction ({},{},{},{})", transactionId, source, type, status);
        int rowsAffected = 0;
        if (conn != null) {
            String query = ConstantSQL.INSERT_SOURCE_TRANSACTION;
            log.debug("Query: {}", query);
            String[] paramTypes = new String[5];
            String[] paramValues = new String[5];
            for (int x=0; x<5; x++) paramTypes[x] = JdbcService.PARAM_TYPE_STRING;
            paramValues[0] = transactionId;
            paramValues[1] = source;
            paramValues[2] = type.getLabel();
            paramValues[3] = status;
            paramValues[4] = requestJson;
            try {
                rowsAffected = JdbcService.executeDml(conn, query, paramTypes, paramValues);
                if (rowsAffected != 1) {
                    throw new SystemException("ERROR: JdbcService.postSourceTransaction() should have gotten SQL statement result of 1 but instead got " + rowsAffected);
                }
            } catch (SQLException e) {
                rollbackTransaction(conn);
                log.error(e.getMessage());
                e.printStackTrace();
                throw new SystemException("ERROR: JdbcService.postSourceTransaction() threw SQLException " + e.getMessage());
            }
        }
        return rowsAffected;
    }

    public static void commitTransaction(Connection conn) throws SystemException {
        log.debug("JdbcService.commitTransaction()");
        try {
            conn.commit();
        } catch (SQLException x) {
            throw new SystemException("Unable to commit database transaction.");

        }
    }

    public static void rollbackTransaction(Connection conn) {
        log.debug("JdbcService.rollbackTransaction()");
        try {
            conn.rollback();
        } catch (SQLException x) {
            throw new RuntimeException("Unable to roll back database transaction. THIS SHOULD NEVER HAPPEN!!!");

        }
    }

}
