package com.kajeet.filteringcomponent.model.device;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class CorpDevices {
    private String corpId;
    private String carrier;
    private String ipPool;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sourceSystem;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionId;
    private List<Device> devices;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getIpPool() {
        return ipPool;
    }

    public void setIpPool(String ipPool) {
        this.ipPool = ipPool;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
