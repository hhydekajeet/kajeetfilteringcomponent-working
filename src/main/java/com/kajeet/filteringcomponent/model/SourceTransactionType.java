package com.kajeet.filteringcomponent.model;

public enum SourceTransactionType {
    PUT_CUSTOM_URL("PUT Customurl"),
    ADD_CUSTOM_URL("ADD Customurl"),
    DELETE_CUSTOM_URL("DELETE Customurl"),
    POST_DEVICES("POST Devices"),
    DELETE_DEVICES("DELETE Devices"),
    PUT_CORP_POLICY("PUT CorpPolicy"),
    UPDATE_CUSTOM_URL("add/delete Customurl");

    public final String label;

    private SourceTransactionType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static boolean isValid(String test) {

        for (SourceTransactionType type : SourceTransactionType.values()) {
            if (type.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
