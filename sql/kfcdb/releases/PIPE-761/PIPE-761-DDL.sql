# --PIPE-761-DDL.sql
# Incremental structural database changes to support Custom Urls and Panorama/Palo Alto

use kfcdb; -- Do not execute until PIPE-762

Alter table policy add column corp_id_custom int null; 
Alter table policy add column site_access_type_id int null; 
Alter table policy modify column description varchar(2000) null default null; 

Alter table policy add CONSTRAINT policy_corp_custom_fk foreign key (corp_id_custom) REFERENCES corp (id);

create unique index policy_corp_custom_super_ux on policy (id, corp_id_custom, custom_filtering);


create or replace view vw_policy
as
select p.id
     , p.name
     , p.site_access_type_id
     , s.short_name as site_access_type_short_name
     , s.long_name as site_access_type_long_name
     , p.corp_id_custom
     , if(p.corp_id_custom is null, 'Y', 'N') as CORE_FLAG
     , c.name corp_name_custom
     , p.palo_alto_id
     , p.description
     , p.custom_filtering
     , p.custom_filtering_type
     , p.created_on
     , p.updated_on
     , p.created_by
     , p.updated_by
  from policy p
  left join corp c on c.id = p.corp_id_custom
  left join site_access_type s on s.id = p.site_access_type_id
order by p.name
;


-- DROP TABLE IF EXISTS site_access_type;


CREATE TABLE IF NOT EXISTS site_access_type
(
    id            int         NOT NULL PRIMARY KEY,
    short_name    varchar(10) NOT NULL,
    long_name     varchar(40) NOT NULL,
    description   varchar(2000) NULL default null,
    created_on    datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50) NOT NULL,
    updated_by    varchar(50) NOT NULL
);

CREATE UNIQUE INDEX site_access_type_short_name_ux ON site_access_type (short_name);


INSERT INTO site_access_type (id, short_name, long_name, created_by, updated_by) VALUES (1, 'Allow', 'Allow/Open', 'Initial Population', 'Initial Population');
INSERT INTO site_access_type (id, short_name, long_name, created_by, updated_by) VALUES (2, '	', 'Block/Deny', 'Initial Population', 'Initial Population');
update policy set site_access_type_id = 1 where name = 'Open';
update policy set site_access_type_id = 2 where name = 'Deny All';
COMMIT;

Alter table policy add CONSTRAINT policy_site_access_type_fk foreign key (site_access_type_id) REFERENCES site_access_type (id);



DROP TABLE IF EXISTS custom_url;
DROP TABLE IF EXISTS url_filter;
DROP TABLE IF EXISTS url_list;


CREATE TABLE IF NOT EXISTS url_filter
(
    id            int           NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          varchar(50)   NOT NULL,
    policy_id     int           NOT NULL,
    corp_id       int           NOT NULL,
    custom_filtering 		    enum('Y') default 'Y' not null,
    description   varchar(2000) NULL default null,
    created_on    datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50)   NOT NULL,
    updated_by    varchar(50)   NOT NULL
);

CREATE UNIQUE INDEX url_filter_name_ux ON url_filter (name);
ALTER TABLE url_filter ADD CONSTRAINT url_filter_corp_fk FOREIGN KEY (corp_id, custom_filtering) REFERENCES corp (id, custom_filtering);
ALTER TABLE url_filter ADD CONSTRAINT url_filter_policy_fk FOREIGN KEY (policy_id, corp_id, custom_filtering) REFERENCES policy (id, corp_id_custom, custom_filtering);

CREATE OR REPLACE VIEW vw_url_filter
as
select f.id
     , f.name
     , f.policy_id
     , p.name as policy_name
     , f.corp_id
     , c.name as corp_name
     , f.custom_filtering
     , f.description
     , f.created_on, f.updated_on, f.created_by, f.updated_by
  from url_filter f
  join policy p on p.id = f.policy_id 
  join corp c on c.id = f.corp_id
 order by f.name;


CREATE TABLE IF NOT EXISTS url_list
(
    id            int           NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          varchar(50)   NOT NULL,
    url_filter_id int           not null,
    corp_id       int           not null,
    site_access_type_id int not null,
    custom_filtering 		    enum('Y') default 'Y' not null,
    description   varchar(2000) NULL default null,
    created_on    datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50)   NOT NULL,
    updated_by    varchar(50)   NOT NULL
);

CREATE UNIQUE INDEX url_list_name_ux ON url_list (name);
create unique index url_list_corp_super_ux on url_list (id, corp_id);

ALTER TABLE url_list ADD CONSTRAINT url_list_corp_fk FOREIGN KEY (corp_id, custom_filtering) REFERENCES corp (id, custom_filtering);
ALTER TABLE url_list ADD CONSTRAINT url_list_site_access_type_fk FOREIGN KEY (site_access_type_id) REFERENCES site_access_type (id);


CREATE OR REPLACE VIEW vw_url_list
as
select l.id
     , l.name
     , s.short_name as site_access_type_short_name
     , s.long_name as site_access_type_long_name
     , l.url_filter_id
     , f.name as url_filter_name
     , f.policy_name
     , l.corp_id
     , c.name as corp_name
     , l.custom_filtering
     , l.description
     , l.created_on, l.updated_on, l.created_by, l.updated_by
  from url_list l
  join site_access_type s on s.id = l.site_access_type_id
  join vw_url_filter f on f.id = l.url_filter_id 
  join corp c on c.id = l.corp_id
 order by l.name;



-- DROP TABLE custom_url_policy

create table IF NOT EXISTS custom_url
(
    id            int           NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          varchar(50)   not null,
    url_list_id   int           not NULL,
    corp_id       int           not null,
    description   varchar(2000) NULL default null,
    created_on    datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_on    datetime      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50)   NOT NULL,
    updated_by    varchar(50)   NOT NULL
);


CREATE UNIQUE INDEX custom_url_corp_url_ux ON custom_url(corp_id, name);
alter table custom_url add constraint custom_url_list_fk foreign key (url_list_id, corp_id) references url_list (id, corp_id) on delete restrict on update restrict;


CREATE OR REPLACE VIEW vw_custom_url
as
select url.id
     , url.name
     , url.url_list_id
     , lst.name url_list_name
     , url.corp_id
     , lst.corp_name
     , lst.policy_name 
     , lst.url_filter_name 
     , lst.site_access_type_short_name
     , lst.site_access_type_long_name
     , url.description
     , url.created_on, url.updated_on, url.created_by, url.updated_by
  from custom_url url
  join vw_url_list lst on lst.id = url.url_list_id 
 order by url.name;




	



