package com.kajeet.filteringcomponent.common.client.http.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

/**
 * Rest Response
 * 
 * @author Lawrence Matta
 */
public class RestResponse {
	private HttpHeaders headers;
	private HttpStatus status;
	private String message = "";
	
	public HttpHeaders getHeaders() {
		return headers;
	}
	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
    public String toString() {
        return String.format("status='%d'  message='%s'", this.getStatus().value(), this.getMessage());
    }

}
