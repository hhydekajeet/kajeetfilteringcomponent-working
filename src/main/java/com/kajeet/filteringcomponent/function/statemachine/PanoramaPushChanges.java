package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.kajeet.filteringcomponent.common.ClusterInfo;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.Utility;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PanoramaPushChanges {

    private static final Logger log = LogManager.getLogger(PanoramaPushChanges.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("PanoramaPushChanges.handleRequest()");

        log.info("Pushing changes", input.getTransactionType());

        String panoIP = input.getPanoUrl();
        String key = input.getApiKey();
        String commandType = "commit";
        String command = "<commit-all><shared-policy><device-group><entry name=\"%s\"/></device-group></shared-policy></commit-all>";

        String getURL = String.format("https://"+panoIP+"/api/");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("type", commandType);
        queryParams.put("action", "all");
        queryParams.put("key", key);

        RestClient rc = new RestClient(true);
        log.info("Calling api to push changes");
        ArrayList<String> jobsToPoll = new ArrayList<>();
        if (input.getClustersToCommit() == null){
            log.debug("Must be 1st time pushing changes");
            ArrayList<String> allClusters = new ArrayList<>();
            List<ClusterInfo> clusterList = null;
            if (input.getCustomUrlInfo() != null){
                clusterList = input.getCustomUrlInfo().getClusters();
            }
            if (input.getDeviceInfo() != null){
                clusterList = input.getDeviceInfo().getClusters();
            }
            if (input.getPolicyInfo() != null){
                clusterList = input.getPolicyInfo().getClusters();
            }
            for (ClusterInfo aCluster: clusterList){
                allClusters.add(aCluster.getClusterName());
            }
            input.setClustersToCommit(allClusters);
        }

        for (String clusterName : input.getClustersToCommit()) {
            queryParams.put("cmd", String.format(command, clusterName));
            RestResponse response = rc.doGet(getURL, queryParams);
            if (response.getStatus().is2xxSuccessful()) {
                log.debug("Success response: [{}]", response.getMessage());
                Document doc = Utility.convertStringToXMLDocument(response.getMessage());
                Element resultElement = (Element) doc.getElementsByTagName("result").item(0);
                String jobId = resultElement.getElementsByTagName("job").item(0).getTextContent();
                log.info("Commit Job Id: [{}]", jobId);
                input.setStatus("YES");
                jobsToPoll.add(jobId);
            } else {
                throw new SystemException("Error committing changes");
            }
        }
        input.setJobsToPoll(jobsToPoll);
        return input;
    }
}