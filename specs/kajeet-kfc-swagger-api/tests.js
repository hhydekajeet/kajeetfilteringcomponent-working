'use strict'

/**
 * To run these tests do:
 * $ mocha tests.js
 */

var SwaggerParser = require('swagger-parser');
var parser = new SwaggerParser();
var hippie = require('hippie-swagger');
var request = require('request-promise');
var _ = require('lodash');
var yargs = require('yargs');
var fs = require('fs');
var path = require('path');
var utils = require('./test/utils');
var assert = utils.assert;

var logger = require('./test/utils/logger');

_.mixin(require('./test/utils/lodash-mixins'));


// Configure command line arguments and usage
yargs.usage('Usage: mocha tests.js --delay [options]');
yargs.example('mocha tests.js --delay -e config.json');
yargs.alias('p', 'path').describe('p', 'Only test provided endpoint.');
yargs.alias('e', 'env').describe('e', 'Configuration used for tests.').demand('e');
yargs.alias('j', 'jwt').describe('j', 'Use provided JWT token and skip authentication.');
yargs.alias('t', 'timeout').describe('t', 'Set timeout for tests. Defaults to 50000.');
yargs.demand('delay').describe('delay', 'Required by mocha.');
yargs.boolean('configOnly').describe('configOnly', 'Only test configured endpoints.');
var argv = yargs.argv;

var endpointArg = argv.path || false;
var jwt = argv.jwt || null;
var testTimeout = argv.timeout || 50000;
var configOnly = argv.configOnly || null;
var envConfig = argv.env;


// Load configuration for tests
fs.readFile(envConfig, function (err, data) {
  if (err) throw new Error('Error: Could not load '+envConfig+'. ' + err);

  // Parse configuration file
  try {
    var environment = JSON.parse(data);
  } catch (err) {
    throw new Error('Error: Could not parse JSON in '+envConfig+'. ' + err);
  }

  // Dereference swagger spec and obtain a full object including definitions
  parser.dereference(path.join(__dirname, './modesto-arterra-events-api.json'), function (err, spec) {
    if (err) throw new Error('Error: Could not dereference Swagger file. ' + err);
    
    // Generate tests from spec and run validation tests
    var tests = utils.generateTestsFromSpec(spec, environment, endpointArg, configOnly);
    runSpecValidationTests(spec, environment, tests);
  });
});


function runSpecValidationTests(swaggerSpec, environment, tests) {
  // Get api base url from environment
  var api = environment.apiBaseUrl;

  /**
   * Mocha Tests
   */
  describe('Modesto Arterra Events API', function () {

    // Swagger spec validation
    describe('Validate response based on spec:', function () {

      // Authenticate before all tests to obtain JWT if not provided
      if (!jwt) {
        before('Authenticate before all tests to obtain JWT', function(done) {
          this.timeout(testTimeout);

          var auth = environment.authentication;

          if (auth && auth.path && auth.method && auth.body) {
            request({
                method: auth.method,
                uri: api + auth.path,
                headers: {
                  'User-Agent': 'Chrome'
                },
                body: auth.body,
                json: true
              })
              .then(function(res) {
                jwt = res.token;
              })
              .catch(function(err) {
                if (err) logger.error('Authentication failed. ' + err.name + ' ' + err.message);
              })
              .finally(done);

          } else {
            throw new Error('Cannot authenticate. Missing authentication settings in environment.');
          }
        });
      }

      // Loop through each test case and construct mocha it block
      _(tests).forEach(function(test) {
        it(test.name, function(done) {
          this.timeout(testTimeout);

          var resCode = test.responseCode;
          var request = hippie(swaggerSpec);

          // Configure general request options
          request
            .url(api + test.endpoint.path)
            .method(test.endpoint.method)
            .header('User-Agent', 'Chrome')
            .header('Authorization', 'Bearer ' + jwt);

          // Configure headers if defined in test
          if (test.headers) {
            _(test.headers).forEach(function (headerValue, headerName) {
              request.header(headerName, headerValue);
            });
          }

          // Configure params and body if defined in test
          if (test.pathParams) request.pathParams(test.pathParams);
          if (test.queryParams) request.qs(test.queryParams);
          if (test.body) request.send(test.body).json();

          // Parse response before tests to sanitize properties with null values
          request.parser(function(body, fn) {
            if (_.isEmpty(body)) body = '{}';
            var res = utils.sanitizeNullValues(JSON.parse(body), resCode, test.endpoint);
            fn(false, res);
          });

          // Add request status code assertion
          request.expectStatus(resCode);

          // Set test assertions, if defined
          if (test.assertions) {
            // Assert expectHeader
            if (test.assertions.expectHeader) {
              _(test.assertions.expectHeader).forEach(function (headerValue, headerName) {
                request.expectHeader(headerName, headerValue);
              });
            }

            // Assert expectValue
            if (test.assertions.expectValue) {
              _(test.assertions.expectValue).forEach(function (val, key) {
                request.expectValue(key, val);
              });
            }

            // Assert expectDateFormat
            if (test.assertions.expectDateFormat) {
              _(test.assertions.expectDateFormat).forEach(function (dateFormat, key) {
                request.expect(function(res, body, next) {
                  var err = assert.expectDateFormat(body, dateFormat, key);
                  next(err);
                });
              });
            }

            // Assert expectSome
            if (test.assertions.expectSome) {
              _(test.assertions.expectSome).forEach(function (val, key) {
                request.expect(function(res, body, next) {
                  var err = assert.expectSome(body, key, val);
                  next(err);
                });
              });
            }

            // Assert expectAll
            if (test.assertions.expectAll) {
              _(test.assertions.expectAll).forEach(function (val, key) {
                request.expect(function(res, body, next) {
                  var err = assert.expectAll(body, key, val);
                  next(err);
                });
              });
            }
          }

          // Send the request
          request.end(function(err, res, body){
            //logger.info(body, null);
            if (err) throw err;
            done();
          });
        });
      });
    });

  });


  run();

}
