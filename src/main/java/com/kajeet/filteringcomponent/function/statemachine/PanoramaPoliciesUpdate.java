package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.datadoghq.datadog_lambda_java.DDLambda;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.ClusterInfo;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.function.statemachine.util.PolicyUtil;
import com.kajeet.filteringcomponent.model.panorama.policy.Entry;
import com.kajeet.filteringcomponent.model.panorama.policy.GetResponse;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;

public class PanoramaPoliciesUpdate {

    private static final Logger log = LogManager.getLogger(PanoramaDeviceUpdate.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("KfcPanoramaDeviceUpdate.handleRequest()");
        log.info("Policy step functions started: [{}]", input.getTransactionType());
        String panoIP = input.getPanoUrl();
        String panoVersion = input.getPanoVersion();
        String key = input.getApiKey();

        PolicyUtil policyUtil = new PolicyUtil(panoIP, panoVersion, key);

        String apiKeyHeader = Constant.PANO_HEADER_KEY_TYPE;

        ObjectMapper objectMapper = new ObjectMapper();
        RestClient rc = new RestClient(true);
        rc.getHeaders().add(apiKeyHeader, key);
        RestResponse response;
        GetResponse getResponse;
        Entry agEntry;
        ArrayList<Entry> postEntries;

        for (ClusterInfo clusterInfo: input.getPolicyInfo().getClusters()){
            try {
                if (input.getPolicyInfo().getToPolicy() != null && !input.getPolicyInfo().getToPolicy().isEmpty()) {

                    response = policyUtil.getPolicyInfo(clusterInfo.getClusterName(), input.getPolicyInfo().getToPolicy());
                    objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                    log.debug("Serializing response [{}]", response.getMessage());
                    getResponse = objectMapper.readValue(response.getMessage(), GetResponse.class);

                    //Add all the address groups to the source
                    agEntry = getResponse.getResult().getEntry().get(0);
                    for (String addressGroup : clusterInfo.getAddressGroups()) {
                        agEntry.getSource().getMember().add(addressGroup);
                    }

                    postEntries = new ArrayList<>();
                    postEntries.add(agEntry);

                    //Let's update the policy now
                    response = policyUtil.putPolicyInfo(clusterInfo.getClusterName(), input.getPolicyInfo().getToPolicy(), postEntries);

                }
                //Now REMOVE the address groups from the previous policy!
                if (input.getPolicyInfo().getFromPolicy() != null && !input.getPolicyInfo().getFromPolicy().isEmpty()) {
                    response = policyUtil.getPolicyInfo(clusterInfo.getClusterName(), input.getPolicyInfo().getFromPolicy());
                    objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                    log.debug("Serializing response [{}]", response.getMessage());
                    getResponse = objectMapper.readValue(response.getMessage(), GetResponse.class);

                    ArrayList<String> currentAgList = getResponse.getResult().getEntry().get(0).getSource().getMember();
                    for (String addressGroup : clusterInfo.getAddressGroups()) {
                        if (currentAgList.contains(addressGroup)) {
                            currentAgList.remove(addressGroup);
                        }
                    }
                    agEntry = getResponse.getResult().getEntry().get(0);
                    postEntries = new ArrayList<>();
                    postEntries.add(agEntry);
                    //Let's update the policy now
                    response = policyUtil.putPolicyInfo(clusterInfo.getClusterName(), input.getPolicyInfo().getFromPolicy(), postEntries);
                }
            }
            catch(IOException ioException){
                log.error("IOException calling GET: [{}]", ioException.getStackTrace());
            } finally {
                // DataDog Logging:
                DDLambda ddl = new DDLambda(context);
                final Span span = GlobalTracer.get().activeSpan();
                if (span != null) {
                    span.setTag(Constant.CLASS_DOT_NAME, this.getClass().getName());
                    if (input != null) {
                        span.setTag(Constant.TRANSACTIONID, input.getTransactionId());
                        span.setTag(Constant.TO_POLICY, input.getPolicyInfo().getToPolicy());
                        span.setTag(Constant.PANO_URL, input.getPanoUrl());
                        span.setTag(Constant.SOURCE_TRANSACTION_TYPE, input.getTransactionType() != null ? input.getTransactionType().getLabel() : null);
                    }
                    span.setTag(Constant.ATTEMPT_DOT_NUMBER, 1);
                    span.finish();
                }
            }
        }

        input.setStatus("SUCCESS");
        log.info("Policy step functions policy update complete: [{}]", input.getTransactionType());
        return input;
    }

 }
