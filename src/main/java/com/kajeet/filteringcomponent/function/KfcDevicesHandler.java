package com.kajeet.filteringcomponent.function;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.datadoghq.datadog_lambda_java.DDLambda;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.*;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.MissingRequiredFields405Exception;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.*;
import com.kajeet.filteringcomponent.model.Result;
import com.kajeet.filteringcomponent.model.SourceSystem;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import com.kajeet.filteringcomponent.model.device.CorpDevices;
import com.kajeet.filteringcomponent.model.device.Device;
import com.kajeet.filteringcomponent.model.device.Devices;
import com.kajeet.filteringcomponent.model.objects.AddressGroupCluster;
import com.kajeet.filteringcomponent.model.objects.Policy;
import com.kajeet.filteringcomponent.model.service.DBDeviceService;
import com.kajeet.filteringcomponent.model.service.DBPolicyService;
import com.kajeet.filteringcomponent.model.service.JdbcService;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KfcDevicesHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final String NA = "N/A";
    private static final Logger log = LogManager.getLogger(KfcDevicesHandler.class);
    public static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private String SPACE = " ";
    private ObjectMapper objectMapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

    private static  String dbUser = "";
    private static  String dbPassword = "";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static String queueURL = "";
    private Connection dbConnection = null;


    static {
        String secretsManager = System.getenv(Constant.KFC_SECRETS_MANAGER);

        dbUser = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_USERNAME);
        dbPassword = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PASSWORD);
        dbHost = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_HOST);
        dbPort = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PORT);
        dbName = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_DBNAME);

        log.debug("user: {}", dbUser);
        log.debug("password: {}", dbPassword);
        log.debug("host: {}", dbHost);
        log.debug("port: {}", dbPort);
        log.debug("dbname: {}", dbName);

        queueURL = System.getenv(Constant.KFC_QUEUE_URL);
        log.debug("Queue: {}", queueURL);
    }
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        log.trace("KfcDevicesHandler.handleRequest(): path = " + input.getPath());
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        Result result = new Result(Constant.SUCCESS_CODE_200, "", new ArrayList<String>());
        String httpMethod = "";
        dbConnection = getDBConnection();
        Devices devices = null;
        CorpDevices corpDevices = null;
        String path = "";

        try {
            path = input.getPath();
            String bodyJson = input.getBody();
            log.debug("Body: [{}]", bodyJson);
            httpMethod = input.getHttpMethod().toString();
            log.info("Http Method [{}] ", httpMethod);

            switch (httpMethod) {
                case Constant.HTTP_GET:
                    String corpId = input.getPathParameters().get(Constant.CORP_ID);
                    KfcPoliciesHandler.validateSingleInput(Constant.CORP_ID, corpId, result);
                    devices = DBDeviceService.getDevicesByCorpId(dbConnection, corpId);
                    log.info("Returning successful response");
                    Response.createSuccessfulResponse(response, devices);
                    break;
                case Constant.HTTP_POST:
                    corpDevices = executePostDevices (response, bodyJson, result, dbConnection, context);
                    break;
                case Constant.HTTP_DELETE:
                    corpDevices = executeDeleteDevices (response, bodyJson, result, dbConnection, context);
                   break;
                default:
                    // Do nothing
                    break;
            }
            return response;

        } catch (IOException e) {
            log.error(e);
            response.setStatusCode(Integer.parseInt(Constant.ERROR_CODE_500));
            response.setBody(e.getMessage());
            result.setCode(""+Constant.ERROR_CODE_500);
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (InvalidInputException e) {
            log.error(e);
            response.setStatusCode(e.getCode());
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (MissingRequiredFields405Exception e) {
            response.setStatusCode(e.getCode());
            log.error(Constant.HTTP_ERROR_CODE + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (SystemException e) {
            log.error(e);
            response.setStatusCode(e.getCode());
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } finally { // DataDog Logging:
            DDLambda ddl = new DDLambda(input, context);
            final Span span = GlobalTracer.get().activeSpan();
            if (span != null) {
                span.setTag(Constant.CLASS_DOT_NAME, this.getClass().getName());
                span.setTag(Constant.HTTP_METHOD, httpMethod);
                span.setTag(Constant.EVENT, path);
                if (corpDevices != null) {
                    span.setTag(Constant.CORPID, corpDevices.getCorpId());
                    span.setTag(Constant.CARRIER, corpDevices.getCarrier());
                    span.setTag(Constant.SOURCE_SYSTEM, corpDevices.getSourceSystem());
                }
                if (devices != null && devices.getDevices()!=null) {
                    span.setTag(Constant.MDN_IMEI_ADDRESS, "Multiple(" + (devices.getDevices().size())+")");
                }
                if (corpDevices != null && corpDevices.getDevices()!=null) {
                    span.setTag(Constant.MDN_IMEI_ADDRESS, "Multiple(" + (corpDevices.getDevices().size())+")");
                }
                span.setTag(Constant.RESPONSE, response.getBody());
                span.setTag(Constant.RESPONSE_DOT_STATUS_CODE, response.getStatusCode());
                span.setTag(Constant.TRANSACTIONID, (corpDevices!=null?corpDevices.getTransactionId():null));
                span.setTag(Constant.ATTEMPT_DOT_NUMBER, 1);
                span.finish();
            } else {
                log.info("Span is null");
            }
            ddl.finish();
        }
    }

    private void validateCorpDevices (CorpDevices corpDevices, Result result) throws InvalidInputException, MissingRequiredFields405Exception {
//        sentinel, catalyst, aaa
        boolean bad = false;
        try {
            if (!SourceSystem.isValid(corpDevices.getSourceSystem())) {
                bad = true;
                throw new InvalidInputException(corpDevices.getSourceSystem() + " is not a valid Source System.");
            }
        } catch (MissingRequiredFields405Exception x) {
            bad = true;
            RequestValidator.addErrorField(Constant.SOURCE_SYSTEM, result);
        }
        if (corpDevices.getCorpId()==null || corpDevices.getCorpId().length()==0){
            bad = true;
            RequestValidator.addErrorField(Constant.CORPID, result);
        }
        if (corpDevices.getDevices()==null || corpDevices.getDevices().size()==0){
            bad = true;
            RequestValidator.addErrorField(Constant.DEVICES, result);
        } else {
            for (Device device: corpDevices.getDevices()){
                if (device==null || device.getMdn()==null || device.getMdn().length()==0) {
                    bad = true;
                    RequestValidator.addErrorField(Constant.MDN, result);
                    break;
                }
            }
        }
        if (bad) {
            result.setCode(Constant.ERROR_CODE_405);
            throw new MissingRequiredFields405Exception("One or more required field values missing.");
        }
    }

    private KFCAsyncMessage prepQueueMessage (CorpDevices corpDevices, String transactionId, SourceTransactionType transactionType, AddressGroupCluster addressGroupCluster, Policy policy) {
        log.debug("KfcDevicesHandler.prepQueueMessage() transactionId: {}", transactionId);
        KFCAsyncMessage queueMessage = new KFCAsyncMessage();
        queueMessage.setTransactionId(transactionId);
        queueMessage.setTransactionType(transactionType);
        DeviceInfo deviceInfo = new DeviceInfo();
        List<ClusterInfo> clusterInfoList = new ArrayList<>();
        ClusterInfo clusterInfo = new ClusterInfo();
        clusterInfo.setClusterName(addressGroupCluster.getClusterName());
        List<Address> addressList = new ArrayList<>();
        for (Device device: corpDevices.getDevices()) {
            Address address = new Address();
            address.setAddressGroup(addressGroupCluster.getAddressGroupName());
            address.setAddressIP(device.getIpAddress());
            address.setAddressName(device.getMdn());
            address.setAddressDesc(null); /////
            addressList.add(address);
        }
        clusterInfo.setAddresses(addressList);
        if (policy!=null) clusterInfo.setCorpPolicy(policy.getPaloAltoId());
        clusterInfoList.add(clusterInfo);

        deviceInfo.setClusters(clusterInfoList);
        queueMessage.setDeviceInfo(deviceInfo);
        return queueMessage;
    }

    private CorpDevices executePostDevices (APIGatewayProxyResponseEvent response, String bodyJson, Result result, Connection conn, Context context) throws IOException, InvalidInputException, SystemException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.executePostDevices()\r\n" + bodyJson);
        CorpDevices corpDevices = null;
        try {
            corpDevices = objectMapper.readValue(bodyJson, CorpDevices.class);
        } catch (Exception x) {
            log.error(x.getMessage());
            throw new InvalidInputException(Constant.INVALID_REQUEST_BODY + bodyJson);
        }
        validateCorpDevices (corpDevices, result);
        String transactionId = Utility.generateEventId(corpDevices.getSourceSystem(), context.getAwsRequestId(), new Date().getTime());
        Policy policy = DBPolicyService.getPolicyByCorpId(conn, corpDevices.getCorpId());
        corpDevices.setTransactionId(transactionId);
        JdbcService.postSourceTransaction(conn, transactionId, corpDevices.getSourceSystem(), SourceTransactionType.POST_DEVICES, "Created", corpDevices.toString());
        AddressGroupCluster addressGroupCluster = DBDeviceService.postCorpDevices(conn, corpDevices);
        KFCAsyncMessage message = prepQueueMessage(corpDevices, corpDevices.getTransactionId(), SourceTransactionType.POST_DEVICES, addressGroupCluster, policy);
        log.debug("KfcPoliciesHandler.executePostDevices() sending KfcDevicesAsyncMessage:\r\n" + message);
        SQSUtil.sendSQSMessage(queueURL, message.toString());
        Response.createSuccessfulResponse(response, corpDevices);
        return corpDevices;
    }

    private CorpDevices executeDeleteDevices (APIGatewayProxyResponseEvent response, String bodyJson, Result result, Connection conn, Context context) throws IOException, InvalidInputException, SystemException, MissingRequiredFields405Exception {
        log.debug("KfcPoliciesHandler.executeDeleteDevices()\r\n" + bodyJson);
        CorpDevices corpDevices = objectMapper.readValue(bodyJson, CorpDevices.class);
        validateCorpDevices (corpDevices, result);
        String transactionId = Utility.generateEventId(corpDevices.getSourceSystem(), context.getAwsRequestId(), new Date().getTime());
        corpDevices.setTransactionId(transactionId);
        JdbcService.postSourceTransaction(conn, transactionId, corpDevices.getSourceSystem(), SourceTransactionType.DELETE_DEVICES, "Created", corpDevices.toString());
        AddressGroupCluster addressGroupCluster = DBDeviceService.deleteCorpDevices(conn, corpDevices);
        Policy policy = DBPolicyService.getPolicyByCorpId(conn, corpDevices.getCorpId());
        KFCAsyncMessage message = prepQueueMessage(corpDevices, corpDevices.getTransactionId(), SourceTransactionType.DELETE_DEVICES, addressGroupCluster, policy);
        log.debug("KfcPoliciesHandler.executeDeleteDevices() sending KfcDevicesAsyncMessage:\r\n" + message);
        SQSUtil.sendSQSMessage(queueURL, message.toString());
        Response.createSuccessfulResponse(response, corpDevices);
        return corpDevices;
    }

    private Connection getDBConnection(){
        if (dbConnection != null){
            return dbConnection;
        }
        String dbURL = "jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName;
        try {
            dbConnection = JdbcService.getConnection(dbURL,dbUser, dbPassword);
        }
        catch (SQLException x) {
            log.error("Error getting a connection to the DB");
        }
        return dbConnection;
    }

    private String postMessageToQueue(String message){
        log.debug("postMessageToQueue('" +message+"')");
        return SQSUtil.sendSQSMessage(queueURL, message);
    }

}
