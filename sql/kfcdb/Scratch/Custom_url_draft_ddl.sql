﻿/*
created: 3/18/2022
modified: 3/25/2022
model: mysql 8.0
database: mysql 8.0
*/

-- create tables section -------------------------------------------------

-- table policy

create table policy
(
  id int not null,
  name varchar(50) not null,
  custom_filtering char(1),
  corp_id_custom int,
  site_access_type_id int
)
;

create index policy_name_ux on policy (corp_id_custom)
;

create index ix_relationship1 on policy (site_access_type_id)
;

create unique index policy_corp_custom_super_ux on policy (id, corp_id_custom, custom_filtering)
;

create unique index policy_custom_filtering_super_ux on policy (id, custom_filtering)
;

alter table policy add primary key (id)
;

alter table policy add unique name (name)
;

-- table corp

create table corp
(
  id int not null,
  name varchar(50) not null,
  custom_filtering char(1)
)
;

create index corp_policy_fk on corp (custom_filtering, id)
;

create unique index corp_custom_filtering_super_ux on corp (custom_filtering, id)
;

alter table corp add primary key (id)
;

alter table corp add unique name (name)
;

-- table site_access_type

create table site_access_type
(
  id int not null,
  name varchar(50) not null
)
;

alter table site_access_type add primary key (id)
;

alter table site_access_type add unique name (name)
;

-- table url_list

create table url_list
(
  id int not null,
  name varchar(50) not null,
  url_filter_id int not null,
  corp_id int not null,
  site_access_type_id int not null,
  custom_filtering char(1)
)
;

create index ix_relationship11 on url_list (site_access_type_id)
;

create index ix_url_list_url_filter_fk on url_list (url_filter_id, corp_id, custom_filtering)
;

create index ix_url_list_corp_fk on url_list (custom_filtering, corp_id)
;

alter table url_list add primary key (id)
;

alter table url_list add unique name (name)
;

-- table url_filter

create table url_filter
(
  id int not null,
  name varchar(50) not null,
  policy_id int,
  custom_filtering char(1),
  corp_id int
)
;

create index ix_relationship14 on url_filter (corp_id, custom_filtering)
;

create unique index url_filter_corp_super_ux on url_filter (id, corp_id, custom_filtering)
;

create index ix_relationship2 on url_filter (policy_id, corp_id, custom_filtering)
;

alter table url_filter add primary key (id)
;

alter table url_filter add unique name (name)
;

-- table custom_url

create table custom_url
(
  id int not null,
  name varchar(50) not null,
  url_list_id int not null
)
;

alter table custom_url add primary key (id)
;

alter table custom_url add unique name (name)
;

-- create foreign keys (relationships) section -------------------------------------------------

alter table corp add constraint corp_policy_fk foreign key (id, custom_filtering) references policy (id, custom_filtering) on delete restrict on update restrict
;

alter table custom_url add constraint custom_url_list_fk foreign key (url_list_id) references url_list (id) on delete restrict on update restrict
;

alter table url_list add constraint url_list_corp_fk foreign key (custom_filtering, corp_id) references corp (custom_filtering, id) on delete restrict on update restrict
;

alter table url_list add constraint url_list_site_access_fk foreign key (site_access_type_id) references site_access_type (id) on delete restrict on update restrict
;

alter table url_list add constraint url_list_url_filter_fk foreign key (url_filter_id, corp_id, custom_filtering) references url_filter (id, corp_id, custom_filtering) on delete restrict on update restrict
;

alter table url_filter add constraint url_filter_corp_fk foreign key (custom_filtering, corp_id) references corp (custom_filtering, id) on delete restrict on update restrict
;

alter table policy add constraint policy_corp_custom_fk foreign key (corp_id_custom) references corp (id) on delete restrict on update restrict
;

alter table policy add constraint policy_site_access_type_fk foreign key (site_access_type_id) references site_access_type (id) on delete restrict on update restrict
;

alter table url_filter add constraint url_filter_policy_validated_fk foreign key (policy_id, corp_id, custom_filtering) references policy (id, corp_id_custom, custom_filtering) on delete restrict on update restrict
;


