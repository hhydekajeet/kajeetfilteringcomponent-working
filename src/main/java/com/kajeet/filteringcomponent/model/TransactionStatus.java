package com.kajeet.filteringcomponent.model;

public enum TransactionStatus {
    pending("Pending"), processing("Processing"), completed("Completed"), cancelled("Cancelled"), unknown("Unknown");
    public final String label;

    private TransactionStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static boolean isValid(String test) {

        for (TransactionStatus type : TransactionStatus.values()) {
            if (type.name().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static TransactionStatus getTransactionStatus (String label) {
        for (TransactionStatus type : TransactionStatus.values()) {
            if (type.getLabel().equals(label)) {
                return type;
            }
        }
        return unknown;
    }

}
