package com.kajeet.filteringcomponent.model.device;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Device {
    public static final String DB_FIELD_MDN = "mdn";
    public static final String DB_FIELD_IMEI = "imei";
    public static final String DB_FIELD_IP_ADDRESS = "ip_address";
    public static final String DB_FIELD_TYPE = "type";
    public static final String DB_FIELD_ADDRESS_GROUP = "address_group_name";

    private String mdn;
    private String imei;
    private String ipAddress;
    private String type;

    @JsonIgnore
    private String addressGroup;

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddressGroup() {
        return addressGroup;
    }

    public void setAddressGroup(String addressGroup) {
        this.addressGroup = addressGroup;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
