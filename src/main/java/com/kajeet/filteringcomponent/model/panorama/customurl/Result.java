package com.kajeet.filteringcomponent.model.panorama.customurl;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Result {
    @JsonProperty("@total-count")
    String totalCount;
    @JsonProperty("@count")
    String count;
    ArrayList<Entry> entry;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList<Entry> getEntry() {
        return entry;
    }

    public void setEntry(ArrayList<Entry> entry) {
        this.entry = entry;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
