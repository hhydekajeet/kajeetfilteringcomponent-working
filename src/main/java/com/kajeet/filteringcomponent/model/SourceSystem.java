package com.kajeet.filteringcomponent.model;

import com.kajeet.filteringcomponent.common.exception.MissingRequiredFields405Exception;

public enum SourceSystem {
    sentinel("sentinel"), catalyst("catalyst"), aaa("aaa");
    public final String label;

    private SourceSystem(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static boolean isValid(String test) throws MissingRequiredFields405Exception {
        if (test==null || test.length()==0) {
            throw new MissingRequiredFields405Exception("Missing required field sourceSystem");
        }
        for (SourceSystem type : SourceSystem.values()) {
            if (type.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
