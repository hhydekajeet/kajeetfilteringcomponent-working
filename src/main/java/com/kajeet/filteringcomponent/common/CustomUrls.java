package com.kajeet.filteringcomponent.common;

import java.util.List;

public class CustomUrls {

    private String urlListName;
    private List<String> urls;
    String editType;

    public String getUrlListName() {
        return urlListName;
    }

    public void setUrlListName(String urlListName) {
        this.urlListName = urlListName;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String getEditType() {
        return editType;
    }

    public void setEditType(String editType) {
        this.editType = editType;
    }
}
