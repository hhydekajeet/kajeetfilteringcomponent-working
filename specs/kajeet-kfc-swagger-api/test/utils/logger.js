var _ = require('lodash');
var util = require('util');
var chalk = require('chalk');


var config = {
  disableWarnings: true
};

function log(prepend, x, v, color) {  
  if (_.isNull(v) || _.isNumber(v)) {
    if (color) {
      console.log(
        chalk[color](prepend),
        chalk[color]( util.inspect(x, false, v) )
      );
    } else {
      console.log(
        prepend,
        util.inspect(x, false, v)
      );
    }
  } else {
    if (color) {
      console.log(
        chalk[color](prepend),
        chalk[color](x)
      );
    } else {
      console.log(
        prepend,
        x
      );
    }
  }
}

module.exports.debug = function(x, v) {
  log('DEBUG:', x, v);
};

module.exports.info = function(x, v) {
  log('INFO:', x, v, 'blue');
};

module.exports.warn = function(x, v) {
  if (!config.disableWarnings) {
    log('WARN:', x, v, 'yellow');
  }
};

module.exports.error = function(x, v) {
  log('ERROR:', x, v, 'red');
};