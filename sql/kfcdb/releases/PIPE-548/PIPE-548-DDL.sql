use kfcdb;

drop table ip_pool;
drop table carrier;
drop table address;
drop table custom_url_policy;
drop table corp_policy_history;
drop table address_group;
drop table source_transaction;
drop table panorama_transaction;
drop table cluster;
drop table corp;
drop table policy;


create table carrier
(
    id         int         not null auto_increment primary key,
    name       varchar(50) not null,
    created_on DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by varchar(50) not null,
    updated_by varchar(50) not null
);

create table cluster
(
    id                          int         not null auto_increment primary key,
    name                        varchar(50) not null,
    panorama_url                varchar(50) NOT NULL,
    address_limit               int         not null,
    address_group_limit         int         not null,
    exception_limit             int         null,
    created_on                  DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on                  DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by                  varchar(50) not null,
    updated_by                  varchar(50) not null
);

create table ip_pool
(
    id           int         not null auto_increment primary key,
    cluster_id   int         not null,
    carrier_id   int         not null,
    pool         varchar(50) not null,
    cidr         varchar(50) not null,
    start_ip     varchar(50) not null,
    end_ip       varchar(50) not null,
    pool_size    int         not null,
    num_assigned int         not null,
    created_on   DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on   DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50) not null,
    updated_by   varchar(50) not null
);

create table policy
(
    id           int          not null auto_increment primary key,
    name         varchar(50)  not null,
    palo_alto_id varchar(50)  not null,
    description  varchar(100) not null,
    created_on   DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on   DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50)  not null,
    updated_by   varchar(50)  not null
);

create table corp
(
    id            int         not null auto_increment primary key,
    name          varchar(50) not null,
    policy_id     int         null,
    created_on    DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on    DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50) not null,
    updated_by    varchar(50) not null
);

create table address_group
(
    id            int         not null auto_increment primary key,
    name          varchar(50) not null,
    cluster_id    int         not null,
    corp_id       int         not null,
#    address_count int,
    is_available  int         not null,
    created_on    DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on    DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by    varchar(50) not null,
    updated_by    varchar(50) not null
);

create table address
(
    id               int         not null auto_increment primary key,
    address_group_id int         not null,
    mdn              varchar(50) not null,
    imei             varchar(50) not null,
    ip_address       varchar(50) not null,
    type             varchar(50) not null,
    created_on       DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on       DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by       varchar(50) not null,
    updated_by       varchar(50) not null
);


/*
create table address_group_policy
(
    id                      int         not null auto_increment primary key,
    policy_id               int         not null,
    address_group_id        int         not null,
    source_transaction_id varchar(100)         not null,
    created_on              DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on              DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) not null,
    updated_by              varchar(50) not null
);
*/



create table corp_policy_history
(
    id                      int         not null auto_increment primary key,
    corp_id                 int         not null,
    policy_id               int         not null,
    source_transaction_id   varchar(100)         not null,
    created_on              DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on              DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) not null,
    updated_by              varchar(50) not null
);

create table custom_url_policy
(
    id                      int         not null auto_increment primary key,
    corp_id                 int         not null,
    name                    varchar(50) not null,
    created_on              DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on              DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by              varchar(50) not null,
    updated_by              varchar(50) not null
);

create table source_transaction
(
    id           varchar(100)  not null primary key,
    source       varchar(50)   not null,
    type         varchar(50)   not null,
    status       varchar(50)   not null,
    request_json varchar(1000) not null,
    created_on   DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on   DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by   varchar(50)   not null,
    updated_by   varchar(50)   not null
);

create table panorama_transaction
(
    id         int         not null auto_increment primary key,
    type       varchar(50) not null,
    status     varchar(50) not null,
    created_on DATETIME not null DEFAULT CURRENT_TIMESTAMP,
    updated_on DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by varchar(50) not null,
    updated_by varchar(50) not null
);

#alter table address
#    add constraint address_pk primary key (id);
#alter table address_group
#    add constraint address_group_pk primary key (id);
#alter table carrier
#    add constraint carrier_pk primary key (id);
#alter table cluster
#    add constraint cluster_pk primary key (id);
#alter table carrier
#    add constraint carrier_pk primary key (id);
#alter table corp
#    add constraint corp_pk primary key (id);
#alter table custom_url_policy
#    add constraint custom_url_policy_pk primary key (id);
#alter table panorama_transaction
#   add constraint panorama_transaction_pk primary key (id);
#alter table source_transaction
#    add constraint source_transaction_pk primary key (id);
#alter table ip_pool
#    add constraint ip_pool_pk primary key (id);


alter table ip_pool
    add constraint ip_pool_cluster_fk foreign key (cluster_id) references cluster (id);

alter table ip_pool
    add constraint ip_pool_carrier_fk foreign key (carrier_id) references carrier (id);

alter table address_group
    add constraint address_group_cluster_fk foreign key (cluster_id) references cluster (id);

alter table address_group
    add constraint address_group_corp_fk foreign key (corp_id) references corp (id);

alter table address
    add constraint address_address_group_fk foreign key (address_group_id) references address_group (id);

/*
alter table address_group_policy
    add constraint address_group_policy_policy_fk foreign key (policy_id) references policy (id);

alter table address_group_policy
    add constraint address_group_policy_address_group_fk foreign key (address_group_id) references address_group (id);

alter table address_group_policy
    add constraint address_group_policy_source_transaction_fk foreign key (source_transaction_id) references source_transaction (id);
*/

alter table corp
    add constraint corp_policy_fk foreign key (policy_id) references policy (id);
alter table custom_url_policy
    add constraint custom_url_policy_corp_fk foreign key (corp_id) references corp (id);
   
  
   
#drop index ip_pool_ux on ip_pool;   
create unique index ip_pool_ux on ip_pool (pool);   
# alter table ip_pool drop constraint ip_pool_uk;
# alter table ip_pool add constraint ip_pool_uk unique (pool);

create unique index carrier_ux on carrier(name);
create unique index corp_ux on corp(name);
create unique index cluster_ux on cluster(name);
create unique index address_mdn_ux on address(mdn);
create unique index address_imei_ux on address(imei);
create unique index custom_url_policy_ux on custom_url_policy(name);
create unique index address_group_ux on address_group(name);
   
   
   
