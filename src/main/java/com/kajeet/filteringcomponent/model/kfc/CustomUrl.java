package com.kajeet.filteringcomponent.model.kfc;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class CustomUrl {
    private String corpId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sourceSystem;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionId;

    private List<String> urls;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
