package com.kajeet.filteringcomponent.common.exception;

public class NotFound404Exception extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 376198983288592290L;
  
  private int code = 404;

  public NotFound404Exception(String message) {
    super(message);
  }

  public NotFound404Exception(String message, int code) {
    super(message);
    this.code = code;
  }

  public int getCode() {
    return code;
  }

}
