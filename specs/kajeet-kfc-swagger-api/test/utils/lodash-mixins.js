var _ = require('lodash');


/**
 * Deep version of _.pickBy
 * Creates an object composed of the object properties predicate returns
 * truthy for. The predicate is invoked with two arguments: (value, key).
 */
function pickByDeep(collection, predicate, thisArg){
  predicate = _.iteratee(predicate, thisArg);

  return _.transform(collection, function(memo, val, key) {
    var include;
    if (_.isObject(val)) {
      val = pickByDeep(val, predicate);
      include = !_.isEmpty(val);
    } else {
      include = predicate(val, key);
    }
    if (include) {
      _.isArray(collection) ? memo.push(val) : memo[key] = val;
    }
  });
}


/**
 * Deep version of _.omitBy
 * The opposite of _.pickByDeep; this method creates an object composed of the own
 * and inherited enumerable string keyed properties of object that predicate doesn’t 
 * return truthy for. The predicate is invoked with two arguments: (value, key).
 */
function omitByDeep(collection, predicate, thisArg){
  predicate = _.iteratee(predicate, thisArg);

  return _.transform(collection, function(memo, val, key) {
    var include;
    if (_.isObject(val)) {
      val = omitByDeep(val, predicate);
      include = !_.isEmpty(val);
    } else {
      include = !predicate(val, key);
    }
    if (include) {
      _.isArray(collection) ? memo.push(val) : memo[key] = val;
    }
  });
}


/**
 * Recursively checks if key exists in a collection.
 * By default returns an array with all the objects that contained the provided key. 
 * If a truthy third parameter is passed, it returns an array with the values of each
 * ocurrence of the provided key in the collection.
 */
function hasKeyDeep(obj, key, valueOnly) {
  if (_.has(obj, key))
      return valueOnly ? [obj[key]] : [obj];

  var res = [];
  _.forEach(obj, function(v) {
      if (_.isObject(v) && (v = hasKeyDeep(v, key, valueOnly)).length)
          res.push.apply(res, v);
  });
  return res;
}


/**
 * Alternative to _.cloneDeepWith
 * This method is similar to _.cloneDeepWith, except that the customizer is invoked 
 * with up to three arguments: (value, index|key, path). The path argument of the
 * customizer provides the object path of the current iterated value.
 */
function cloneDeepTransform(collection, predicate, thisArg, _path) {
  predicate = _.iteratee(predicate, thisArg);

  return _.transform(collection, function(memo, val, key) {
    var newValue;
    var path;

    // Keep track of value's object path
    if (_.isArray(collection)) {
      if (_path) {
        path = _path + '[' + key + ']';
      } else {
        path = '[' + key + ']';
      }
    } else {
      if (_path) {
        path = _path + '.' + key;
      } else {
        path = key;
      }
    }

    if (_.isObject(val)) {
      newValue = cloneDeepTransform(val, predicate, undefined, path);
    } else {
      var predicateResult = predicate(val, key, path);
      newValue = (_.isUndefined(predicateResult)) ? val : predicateResult;
    }
    _.isArray(collection) ? memo.push(newValue) : memo[key] = newValue;
  });
}

module.exports = {
  pickByDeep: pickByDeep,
  omitByDeep: omitByDeep,
  hasKeyDeep: hasKeyDeep,
  cloneDeepTransform: cloneDeepTransform
}