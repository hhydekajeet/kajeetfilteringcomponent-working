package com.kajeet.filteringcomponent.common.util;

import com.kajeet.filteringcomponent.common.Constant;
//import com.kajeet.filteringcomponent.model.CommandEnum;
import com.kajeet.filteringcomponent.model.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class RequestValidator {

    private static final Logger log = LogManager.getLogger(RequestValidator.class);
//
//    public static void validateImei(String imei, Result result) {
//        log.debug("Validating IMEI: [{}]", imei);
//
//        if (imei == null || imei.length() < 14 || imei.length() > 15) {
//            result.setCode(Constant.ERROR_CODE_405);
//            addInvalidMessage("imei " + imei + " is not valid.", result);
//            addErrorField(Constant.IMEI, result);
//        }
//    }
//
//    public static void validateDeviceTypeQueryParam(String deviceType, Result result) {
//        log.debug("Validating device type: [{}]", deviceType);
//
//        if (deviceType != null) {
//            if (!deviceType.equals(Constant.DEVICE_TYPE_ORBIC)) {
//                result.setCode(Constant.ERROR_CODE_405);
//                addInvalidMessage(deviceType + " is not supported.", result);
//                addErrorField(Constant.DEVICE_TYPE, result);
//            }
//        }
//        else{
//            result.setCode(Constant.ERROR_CODE_405);
//            addInvalidMessage(Constant.DEVICE_TYPE + " missing.", result);
//            addErrorField(Constant.DEVICE_TYPE, result);
//        }
//    }
//
//    public static void validateTransactionIdQueryParam(String transactionId, Result result) {
//        log.debug("Validating transaction id: [{}]", transactionId);
//        if (transactionId != null) {
//            return;
//        }
//        else{
//            result.setCode(Constant.ERROR_CODE_405);
//            addInvalidMessage(Constant.TRANSACTION_ID + " missing.", result);
//            addErrorField(Constant.TRANSACTION_ID, result);
//        }
//    }
//
//    public static void validateDeviceType(String deviceType, Result result) {
//        log.debug("Validating device type: [{}]", deviceType);
//
//        if (deviceType != null) {
//            if (!deviceType.equals(Constant.DEVICE_TYPE_ORBIC)) {
//                result.setCode(Constant.ERROR_CODE_405);
//                addInvalidMessage(deviceType + " is not supported.", result);
//                addErrorField("deviceType", result);
//            }
//        }
//        else{
//            result.setCode(Constant.ERROR_CODE_405);
//            addInvalidMessage("deviceType missing.", result);
//            addErrorField("deviceType", result);
//        }
//    }
//
//    public static void validateCommand(String command, Result result) {
//        log.debug("Validating command: [{}]", command);
//
//        if (command != null) {
//            Boolean validCommand = false;
//            for(CommandEnum aCommand : CommandEnum.values()){
//                if (aCommand.getCommandString().equals(command)){
//                    validCommand = true;
//                }
//            }
//            if (!validCommand) {
//                result.setCode(Constant.ERROR_CODE_405);
//                addInvalidMessage(command + " is not supported.", result);
//                addErrorField("command", result);
//            }
//        }
//        else{
//            result.setCode(Constant.ERROR_CODE_405);
//            addInvalidMessage("command missing.", result);
//            addErrorField("command", result);
//        }
//    }
//
    public static void validateTransactionId(String transactionId, Result result) {
        log.debug("Validating transaction id: [{}]", transactionId);
        if (transactionId != null && transactionId.length()>0) {
            return;
        }
        else{
            result.setCode(Constant.ERROR_CODE_405);
            addInvalidMessage(Constant.TRANSACTION_ID + " missing.", result);
            addErrorField("transactionId", result);
        }
    }

    public static void addInvalidMessage(String message, Result result){
        if (result.getMessage() != null && !result.getMessage().isEmpty()) {
            String currentMessage = result.getMessage();
            result.setMessage(currentMessage + System.lineSeparator() + message);
        }
        else{
            result.setMessage(message);
        }
    }

    public static void addErrorField(String field, Result result){
        if (result.getFields() != null){
            result.getFields().add(field);
        }
        else{
            ArrayList fields = new ArrayList<String>();
            fields.add(field);
            result.setFields(fields);
        }
    }
}
