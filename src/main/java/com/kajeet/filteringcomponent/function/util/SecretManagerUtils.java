package com.kajeet.filteringcomponent.function.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;
import software.amazon.awssdk.services.secretsmanager.model.SecretsManagerException;
import software.amazon.awssdk.services.secretsmanager.model.UpdateSecretRequest;

import java.io.IOException;
import java.util.HashMap;

public class SecretManagerUtils {

    private static final Logger log = LogManager.getLogger(SecretManagerUtils.class);

    private static HashMap secretMap;

    private static String getSecret(String secretName) {
        String secretString;
        String region = System.getenv("AWS_REGION");
        SecretsManagerClient secretsClient = SecretsManagerClient.builder()
                .httpClient(ApacheHttpClient.create())
                .region(Region.of(region))
                .build();
        secretString = getValue(secretsClient, secretName);
        secretsClient.close();

        return secretString;
    }

    public static String getValue(SecretsManagerClient secretsClient,String secretName) {
        String secretString = null;
        try {
            GetSecretValueRequest valueRequest = GetSecretValueRequest.builder()
                    .secretId(secretName)
                    .build();

            GetSecretValueResponse valueResponse = secretsClient.getSecretValue(valueRequest);
            secretString = valueResponse.secretString();
        } catch (SecretsManagerException e) {
            log.error(e.getMessage());
        }
        return secretString;
    }

    public static void getSecretMap(String secretName) {
        final String secretString = getSecret(secretName);
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            secretMap = objectMapper.readValue(secretString, HashMap.class);
            log.debug("Got the secrets map: {}", secretMap.keySet().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getSecretValueByName(String secretName, String key) {
        boolean secretsLoaded = secretMap!=null;

        if (!secretsLoaded){
            getSecretMap(secretName);
        }

        String secret = null;

        secret = secretMap.get(key).toString();
        log.debug("Got the value {} for key {}", secret, key);
        return secret;
    }

    public static void updateSecret(String secretKey, String secretValue) {
        Region region = Region.US_EAST_1;
        SecretsManagerClient secretsClient = SecretsManagerClient.builder()
                .region(region)
                .build();

        updateSecretByName(secretsClient, secretKey, secretValue);
        secretsClient.close();
    }

    public static void updateSecretByName(SecretsManagerClient secretsClient, String secretName, String secretValue) {

        try {
            UpdateSecretRequest secretRequest = UpdateSecretRequest.builder()
                    .secretId(secretName)
                    .secretString(secretValue)
                    .build();

            secretsClient.updateSecret(secretRequest);
        } catch (SecretsManagerException e) {
            log.error(e.awsErrorDetails().errorMessage());
            throw new RuntimeException(String.format("Unable to update Secret for [%s]", secretName));
        }
    }
}
