#!/usr/bin/env bash
#if [[ $# -ge 2 ]]; then
#    export CDK_DEPLOY_ACCOUNT=$1
#    export CDK_DEPLOY_REGION=$2
#    shift; shift
#    npx cdk deploy "$@"
#    exit $?
#else
#    echo 1>&2 "Provide account and region as first two args."
#    echo 1>&2 "Additional args are passed through to cdk deploy."
#    exit 1
#fi
cdk destroy -c application.name='FilteringComponentService'\
          -c environment='dev'\
          -c loggingLevel='DEBUG'\
          -c dd.layer.arn='arn:aws:lambda:us-east-1:464622532012:layer:dd-trace-java:3'\
          -c vpc.id='vpc-011995629ebc7c5e0' \
          -c db.subnet.id.1='subnet-07c5c436e4846c83d' \
          -c db.subnet.id.2='subnet-0b3ec9961fdcfa35b' \
          -c vpc.subnet.id.1='subnet-07c5c436e4846c83d' \
          -c vpc.subnet.id.2='subnet-0b3ec9961fdcfa35b' \
          -c dd.env='sentinel_dev'
