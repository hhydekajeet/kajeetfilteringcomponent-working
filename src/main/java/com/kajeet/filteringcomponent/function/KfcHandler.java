package com.kajeet.filteringcomponent.function;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.MockResponse;
import com.kajeet.filteringcomponent.common.exception.InvalidInputException;
import com.kajeet.filteringcomponent.common.exception.MethodNotAllowed405Exception;
import com.kajeet.filteringcomponent.common.exception.MissingRequiredFields405Exception;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.SecretManagerUtils;
import com.kajeet.filteringcomponent.model.Result;
import com.kajeet.filteringcomponent.model.TransactionStatus;
import com.kajeet.filteringcomponent.model.kfc.IpPoolCarrier;
import com.kajeet.filteringcomponent.common.util.Response;
import com.kajeet.filteringcomponent.model.service.DbKfcService;
import com.kajeet.filteringcomponent.model.service.JdbcService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class KfcHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final String NA = "N/A";
    private static final Logger log = LogManager.getLogger(KfcHandler.class);
    public static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private String SPACE = " ";
    private static final String PARTIAL_PATH_STATUS = "/service/kfc/status";
    private static final String PARTIAL_PATH_IP_POOL = "/service/kfc/ippool";
    private static final String BRANCH_STATUS = "STATUS";
    private static final String BRANCH_IP_POOL = "IP_POOL";
    private static  String dbUser = "";
    private static  String dbPassword = "";
    private static  String dbHost = "";
    private static  String dbPort = "";
    private static  String dbName = "";
    private static String queueURL = "";

    private ObjectMapper objectMapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    private Connection dbConnection = null;

    static {
        String secretsManager = System.getenv(Constant.KFC_SECRETS_MANAGER);
        dbUser = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_USERNAME);
        dbPassword = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PASSWORD);
        dbHost = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_HOST);
        dbPort = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PORT);
        dbName = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_DBNAME);

        log.debug("user: {}", dbUser);
        log.debug("password: {}", dbPassword);
        log.debug("host: {}", dbHost);
        log.debug("port: {}", dbPort);
        log.debug("dbname: {}", dbName);

        queueURL = System.getenv(Constant.KFC_QUEUE_URL);
        log.debug("Queue: {}", queueURL);

    }


    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        log.debug("KfcPoliciesHandler.handleRequest(): path = " + input.getPath());
        Result result = new Result(Constant.SUCCESS_CODE_200, "", new ArrayList<String>());
        String httpMethod = "";
        String branch = "";
        dbConnection = getDBConnection();

        try {
            String path = input.getPath();
            branch = getBranchFromPath (path);
            log.debug("branch = " + branch);

            httpMethod = input.getHttpMethod().toString();
            log.info("Http Method [{}] ", httpMethod);

            switch (branch) {
                case BRANCH_STATUS:
                    switch (httpMethod) {
                        case Constant.HTTP_GET:
                            String transactionId = input.getPathParameters().get(Constant.TRANSACTION_ID);
                            KfcPoliciesHandler.validateSingleInput(Constant.TRANSACTION_ID, transactionId, result);
                            log.info("Returning successful response");
                            TransactionStatus status = DbKfcService.getTransactionStatus(dbConnection, transactionId);
                            Response.createSuccessfulResponse(response, status.getLabel(), "Transaction ID: " + transactionId);
                            break;
                        default:
                            // Do nothing
                            break;
                    }
                    break;
                case BRANCH_IP_POOL:
                    switch (httpMethod) {
                        case Constant.HTTP_GET:
                            String carrier = input.getPathParameters().get(Constant.CARRIER);
                            KfcPoliciesHandler.validateSingleInput(Constant.CARRIER, carrier, result);
                            // IpPoolCarrier ipPoolCarrier = MockResponse.getMockIpPoolCarrier(carrier); // TEMPORARY MOCKUP TEMPORARY MOCKUP TEMPORARY MOCKUP TEMPORARY MOCKUP
                            IpPoolCarrier ipPoolCarrier = DbKfcService.getIpPoolCarrier(dbConnection, carrier);
                            log.info("Returning successful response");
                            Response.createSuccessfulResponse(response, ipPoolCarrier);
                            break;
                        default:
                            // Do nothing at all
                            break;
                    }
                    break;
            }
            return response;
        } catch (IOException e) {
            log.error(e);
            response.setStatusCode(Integer.parseInt(Constant.ERROR_CODE_500));
            response.setBody(e.getMessage());
            return response;
        } catch (InvalidInputException e) {
            log.error(e);
            response.setStatusCode(e.getCode());
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (MissingRequiredFields405Exception e) {
            response.setStatusCode(e.getCode());
            log.error("HTTP Error code " + response.getStatusCode() + ": " + e);
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } catch (SystemException e) {
            log.error(e);
            response.setStatusCode(e.getCode());
            response.setBody(e.getMessage());
            result.setCode(""+e.getCode());
            result.setMessage(e.getMessage());
            return response.withBody(result.toString());
        } finally {
            // NO DATADOG LOGGING -- YET -- BECAUSE ONLY HTTP GETs
        }
    }


    private String getBranchFromPath (String path) {
        String branch = "";
        if (path.contains(PARTIAL_PATH_STATUS)) {
            branch = BRANCH_STATUS;
        } else if (path.contains(PARTIAL_PATH_IP_POOL)) {
                branch = BRANCH_IP_POOL;
        }
        return branch;
    }


    private Connection getDBConnection(){
        if (dbConnection != null){
            return dbConnection;
        }
        String dbURL = "jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName;

        try {
            dbConnection = JdbcService.getConnection(dbURL,dbUser, dbPassword);
        }
        catch (SQLException x) {
            log.error("Error getting a connection to the DB");
        }
        return dbConnection;
    }

}
