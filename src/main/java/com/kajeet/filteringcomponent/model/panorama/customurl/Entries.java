package com.kajeet.filteringcomponent.model.panorama.customurl;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Entries {
    @JsonProperty("entry")
    ArrayList<Entry> entries;

    public ArrayList<Entry> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<Entry> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
