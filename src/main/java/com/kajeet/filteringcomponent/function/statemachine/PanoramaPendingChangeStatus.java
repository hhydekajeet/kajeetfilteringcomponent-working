package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.Map;

public class PanoramaPendingChangeStatus {

    private static final Logger log = LogManager.getLogger(PanoramaPendingChangeStatus.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException {
        log.debug("PanoramaPendingChangesStatus.handleRequest()");
        log.info("Checking for pending changes", input.getTransactionType());


        String panoIP = input.getPanoUrl();
        String key = input.getApiKey();

        String commandType = "op";
        String command = "<check><pending-changes></pending-changes></check>";

        String getURL = String.format("https://"+panoIP+"/api/");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("type", commandType);
        queryParams.put("cmd", command);
        queryParams.put("key", key);

        RestClient rc = new RestClient(true);
        log.info("Calling get to check for pending changes");

        RestResponse response = rc.doGet(getURL, queryParams);
        if(response.getStatus().is2xxSuccessful()){
            log.debug("Success response: [{}]", response.getMessage());

            Document doc = Utility.convertStringToXMLDocument( response.getMessage() );
            if (doc.getElementsByTagName("result").item(0).getTextContent().equalsIgnoreCase("yes")) {
                log.info("There are pending changes");
                input.setStatus("YES");
                String pendingType = doc.getElementsByTagName("location").item(0).getTextContent();
                log.info("Setting pending type: [{}]", pendingType);
                input.setPendingType(pendingType);
            }
            else{
                input.setStatus("NO");
                input.setPendingType(null);
            }
        }
        else{
            throw new SystemException("Error Updating policy");
        }
        return input;
    }

}

