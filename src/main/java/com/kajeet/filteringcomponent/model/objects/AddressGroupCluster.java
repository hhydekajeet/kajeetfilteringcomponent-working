package com.kajeet.filteringcomponent.model.objects;

public class AddressGroupCluster {
    private int addressGroupId;
    private String addressGroupName;
    private int clusterId;
    private String clusterName;

    public int getAddressGroupId() {
        return addressGroupId;
    }

    public void setAddressGroupId(int addressGroupId) {
        this.addressGroupId = addressGroupId;
    }

    public String getAddressGroupName() {
        return addressGroupName;
    }

    public void setAddressGroupName(String addressGroupName) {
        this.addressGroupName = addressGroupName;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
}
