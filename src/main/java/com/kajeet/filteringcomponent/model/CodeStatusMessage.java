package com.kajeet.filteringcomponent.model;

import com.amazonaws.util.json.Jackson;

public class CodeStatusMessage {
	private String code;
	private String status;
	private String message;

	public CodeStatusMessage() {
		super();
	}

	public CodeStatusMessage(String code, String status, String message) {
		this.code = code;
		this.message = message;
		this.status = status;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return Jackson.toJsonString(this);
	}
}
