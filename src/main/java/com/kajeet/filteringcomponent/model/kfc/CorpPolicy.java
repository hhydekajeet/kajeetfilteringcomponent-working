package com.kajeet.filteringcomponent.model.kfc;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonInclude;

public class CorpPolicy {
    private String corpId;
    private String policyId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sourceSystem;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionId;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
