package com.kajeet.filteringcomponent.model.mapping;

public enum PolicyMapping {
    ID ("id"),
    NAME ("name"),
    PALO_ALTO_ID ("palo_alto_id"),
    DESCRIPTION ("description"),
    CUSTOMER_FILTERING ("custom_filtering"),
    CREATED_BY ("created_by"),
    CREATED_ON("created_on"),
    UPDATED_BY ("updated_by"),
    UDATED_ON("updated_on");

    private final String dbColumnName;

    PolicyMapping (String dbColumnName){
        this.dbColumnName = dbColumnName;
    }

    public String getDbColumnName(){
        return dbColumnName;
    }

}
