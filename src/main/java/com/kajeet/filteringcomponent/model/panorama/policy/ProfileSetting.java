package com.kajeet.filteringcomponent.model.panorama.policy;

import com.amazonaws.util.json.Jackson;

public class ProfileSetting {

    public Profiles profiles;

    public Profiles getProfiles() {
        return profiles;
    }

    public void setProfiles(Profiles profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
