package com.kajeet.filteringcomponent.common.exception;

import java.util.ArrayList;
import java.util.List;

public class InvalidInputException extends Exception {

  private static final long serialVersionUID = -916016762492026789L;

  private int code = 404;
  private List<String> fields = null;

  public InvalidInputException(String message) {
    super(message);
    this.fields = new ArrayList<String>();
  }

  public InvalidInputException(String message, List<String> fields) {
    super(message);
    this.fields = fields;
  }

  public InvalidInputException(String message, int code) {
    super(message);
    this.code = code;
  }
  public int getCode() { return code; }

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public String getMessageWithFields() {
    return fields != null && !fields.isEmpty()
      ? getMessage() + " " + String.join(", ", fields)
      : getMessage();
  }
}
