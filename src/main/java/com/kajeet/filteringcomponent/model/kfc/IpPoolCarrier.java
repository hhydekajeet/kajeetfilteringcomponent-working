package com.kajeet.filteringcomponent.model.kfc;

import com.amazonaws.util.json.Jackson;

public class IpPoolCarrier {
    private String ipPool;
    private String carrier;

    public String getIpPool() {
        return ipPool;
    }

    public void setIpPool(String ipPool) {
        this.ipPool = ipPool;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }
    public String toString() {
        return Jackson.toJsonString(this);
    }

}
