var _ = require('lodash');
var logger = require('./logger');


var EndpointTestCase = {
  init: function (endpoint, method, responseCode, config, globals, spec) {
    this.responseCode = responseCode || 200; // Defaults to 200
    this.endpoint = {
      path: endpoint,
      method: method,
      spec: spec
    };

    this.setName(endpoint, method, responseCode, config);
    this.setHeaders(config.headers);
    this.setPathParams(config, globals, spec);
    this.setQueryParams(config.queryParams);
    this.setBody(config.body);
    this.setAssertions(config.assertions);
  },
  setName: function (endpoint, method, responseCode, config) {
    // Construct automated test name
    var name = method + '@' + endpoint;
    var suffix;

    if (config.suffix) {
      suffix =  responseCode + ' : ' + config.suffix;
    } else if (responseCode) {
      suffix = responseCode;
    } else {
      suffix = '';
    }

    name = name + ' ' + suffix;

    // Prefer configured name if provided
    this.name = (config.name) ? (config.name) : name;
  },
  setHeaders: function (headers) {
    this.headers = (_.isObject(headers)) ? headers : null;
  },
  setPathParams: function (endpointEnvConfig, globals, spec) {
    var params = {};

    // Extract an array of path parameter objects defined in spec
    var specParams = _.filter(spec.parameters, {'in':'path'});

    _(specParams).forEach(function(p, i){
      // If param is in globals, add it and assign global value
      if (globals[p.name]) {
        params[p.name] = globals[p.name];
      }
    });

    // Overwrite previous params if defined in endpointEnvConfig
    if (endpointEnvConfig.pathParams) {
      _(endpointEnvConfig.pathParams).forEach(function(pathValue, pathName){
          params[pathName] = pathValue;
      });
    }

    if (_.isEmpty(params)) params = null;

    this.pathParams = params;
  },
  setQueryParams: function (params) {
    this.queryParams = (_.isObject(params)) ? params : null;
  },
  setBody: function (body) {
    this.body = (_.isObject(body)) ? body : null;
  },
  setAssertions: function (assertions) {
    var testAssertions = {};

    if (_.isObject(assertions)) {
      _(assertions).forEach(function (val, key){
        if (_.isObject(val)) testAssertions[key] = val;
      });
    } else {
      testAssertions = null;
    }

    this.assertions = testAssertions;
  }
};


function createTestCases(endpoint, method, spec, env) {
  var testCases = [];
  var endpointKey = method + '@' + endpoint;
  var endpointEnvConfig = env.endpoints[endpointKey] || {};

  // Normalize configured tests array for easier parsing
  var configuredTestCases = (_.isArray(endpointEnvConfig)) ? endpointEnvConfig : [endpointEnvConfig];

  // Create test cases for endpoint
  _(configuredTestCases).forEach(function(testConfig){
    var test = Object.create(EndpointTestCase);
    var resCode = testConfig.responseCode || env.defaultResponseCode || null;

    test.init(endpoint, method, resCode, testConfig, env.globals, spec);
    testCases.push(test);
  });
  
  // Warn if settings do not exist for this endpoint
  if (!endpointEnvConfig) {
    logger.warn('Endpoint '+ endpointKey + ' not found in environment');
  }
  
  return testCases;
}


module.exports = function(swaggerSpec, environment, endpointArg, configOnly) {
  var tests = [];

  // If endpointArg not specified, generate test cases for all endpoints defined in Swagger spec
  if (!endpointArg) {
    _(swaggerSpec.paths).forEach(function(methods, endpoint) {
      _(methods).forEach(function(endpointSpec, method){
        var endpointKey = method + '@' + endpoint;
        var testCases = createTestCases(endpoint, method, endpointSpec, environment);

        // If configOnly specified, only add test cases for configured endpoints
        if (configOnly) {
          if (environment.endpoints[endpointKey]) {
            _(testCases).forEach(function(test){ tests.push(test) });
          }
        }
        // if not, add test cases for all endpoints defined in Swagger spec 
        else {
          _(testCases).forEach(function(test){ tests.push(test) });
        }
      });
    });
  }
  // when specified, generate test cases ONLY for the provided endpoint
  else {
    // Extract method and endpoint from endpointArg
    var method = _.split(endpointArg, '@')[0];
    var endpoint = _.split(endpointArg, '@')[1];

    try {
      var endpointSpec = swaggerSpec.paths[endpoint][method];
      var testCases = createTestCases(endpoint, method, endpointSpec, environment);

      // Add test cases for provided endpoint
      _(testCases).forEach(function(test){ tests.push(test) });
    } catch (err) {
      if (err) throw new Error('Endpoint not found in Swagger spec');
    }
  }

  return tests;
};