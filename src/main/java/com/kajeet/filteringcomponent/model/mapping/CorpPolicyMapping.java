package com.kajeet.filteringcomponent.model.mapping;

public enum CorpPolicyMapping {
    ID ("id"),
    NAME ("name"),
    POLICY_ID ("policy_id"),
    POLICY_NAME ("policy_name"),
    PALO_ALTO_ID ("policy_palo_alto_id"),
    POLICY_DESCRIPTION ("policy_description"),
    CUSTOMER_FILTERING ("custom_filtering"),
    CREATED_BY ("created_by"),
    CREATED_ON("created_on"),
    UPDATED_BY ("updated_by"),
    UDATED_ON("updated_on");

    private final String dbColumnName;

    CorpPolicyMapping(String dbColumnName){
        this.dbColumnName = dbColumnName;
    }

    public String getDbColumnName(){
        return dbColumnName;
    }

}
