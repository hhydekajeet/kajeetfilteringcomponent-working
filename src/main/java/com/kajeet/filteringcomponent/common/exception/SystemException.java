package com.kajeet.filteringcomponent.common.exception;

import java.util.ArrayList;
import java.util.List;

public class SystemException extends Exception {

  private static final long serialVersionUID = -91601692026789L;

  private int code = 500;
  private List<String> fields = null;

  public SystemException(String message) {
    super(message);
    this.fields = new ArrayList<String>();
  }
  public SystemException(String message, int code) {
    super(message);
    this.code = code;
  }

  public SystemException(String message, List<String> fields) {
    super(message);
    this.fields = fields;
  }
  public SystemException(String message, int code, List<String> fields) {
    super(message);
    this.code = code;
    this.fields = fields;
  }

  public int getCode() {
    return code;
  }
  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public String getMessageWithFields() {
    return fields != null && !fields.isEmpty()
      ? getMessage() + " " + String.join(", ", fields)
      : getMessage();
  }
}
