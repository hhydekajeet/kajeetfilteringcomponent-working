package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.JobNotCompleteException;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.common.util.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PanoramaPollComplete {

    private static final Logger log = LogManager.getLogger(PanoramaPollComplete.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) throws SystemException, JobNotCompleteException {
        log.debug("PanoramaPollComplete.handleRequest()");

        log.info("Number of Jobs to Poll: [{}] ", input.getJobsToPoll().size());

        String notCompleteMessage = "Jobs not complete %s";
        String panoIP = input.getPanoUrl();
        String key = input.getApiKey();
        String commandType = "op";
        String command = "<show><jobs><id>%s</id></jobs></show>";

        String getURL = String.format("https://" + panoIP + "/api/");
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("location", "shared");
        queryParams.put("type", commandType);
        queryParams.put("key", key);

        ArrayList<String> completedJobs = new ArrayList<>();
        for (String jobNumber: input.getJobsToPoll()) {
            log.info("Polling Job Number: [{}] ", jobNumber);
            queryParams.put("cmd", String.format(command, jobNumber));
            RestClient rc = new RestClient(true);
            log.info("Calling api to get job info");

            RestResponse response = rc.doGet(getURL, queryParams);
            if (response.getStatus().is2xxSuccessful()) {
                log.debug("Success response: [{}]", response.getMessage());

                Document doc = Utility.convertStringToXMLDocument(response.getMessage());
                Element resultElement = (Element) doc.getElementsByTagName("result").item(0);
                Element jobElement = (Element) resultElement.getElementsByTagName("job").item(0);
                String jobStatus = jobElement.getElementsByTagName("status").item(0).getTextContent();
                String jobResult = jobElement.getElementsByTagName("result").item(0).getTextContent();

                if (jobStatus.equals("ACT")) {
                    continue;
                } else if (jobStatus.equals("FIN")) {
                    if (jobResult.equals("OK")) {
                        completedJobs.add(jobNumber);
                        log.info(String.format("Job %s complete", jobNumber));
                    } else {
                        if (input.getJobsInError() != null) {
                            input.getJobsInError().add(jobNumber);
                        }
                        else{
                            input.setJobsInError(new ArrayList<>());
                            input.getJobsInError().add(jobNumber);
                        }
                        log.info(String.format("Error in job %s", jobNumber));
                    }
                }
            } else {
                throw new SystemException("Error committing changes");
            }
        }
        for (String completeJob : completedJobs){
            input.getJobsToPoll().remove(completeJob);
        }
        if (input.getJobsInError()!= null && input.getJobsInError().size() > 0){
            throw new SystemException(String.format("Error in %s jobs", input.getJobsInError().size()));
        }
        if (input.getJobsToPoll().size() == 0) {
            input.setStatus("YES");
            log.info("All jobs complete");
        }
        else{
            String jobsStillRunning = "";
            for (String jobNumber: input.getJobsToPoll()){
                jobsStillRunning = jobsStillRunning + jobNumber+":";
            }
            throw new JobNotCompleteException(String.format(notCompleteMessage, jobsStillRunning));
        }

        return input;
    }
}