package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class DeviceInfo {
    private List<ClusterInfo> clusters;

    public List<ClusterInfo> getClusters() {
        return clusters;
    }

    public void setClusters(List<ClusterInfo> clusters) {
        this.clusters = clusters;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
