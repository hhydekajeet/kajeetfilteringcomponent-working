package com.kajeet.filteringcomponent.common.exception;

import java.util.ArrayList;
import java.util.List;

public class MethodNotAllowed405Exception extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 376198983288592290L;
  private List<String> fields = null;

  private int code = 405;

  public MethodNotAllowed405Exception(String message) {
    super(message);
    this.fields = new ArrayList<>();
  }

  public MethodNotAllowed405Exception(String message, List<String> fields) {
    super(message);
    this.fields = fields;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public String getMessageWithFields() {
    return fields != null && !fields.isEmpty()
      ? getMessage() + " " + String.join(", ", fields)
      : getMessage();
  }
}
