package com.kajeet.filteringcomponent.function.statemachine.util;

import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.client.http.rest.RestClient;
import com.kajeet.filteringcomponent.common.client.http.rest.RestResponse;
import com.kajeet.filteringcomponent.common.exception.SystemException;
import com.kajeet.filteringcomponent.model.panorama.policy.Entry;
import com.kajeet.filteringcomponent.model.panorama.policy.PutBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PolicyUtil {
    private static final Logger log = LogManager.getLogger(PolicyUtil.class.getName());

    private static final String GET_SECRUITY_RULE_URL = "/restapi/%s/Policies/SecurityPreRules";
    private static final String UPDATE_SECRUITY_RULE_URL = "/restapi/%s/Policies/SecurityPreRules";

    private String panoIP;
    private String panoVersion;
    private String apiKey;

    private PolicyUtil(){}

    public PolicyUtil (String panoIP, String panoVersion, String apiKey){
        this.panoIP = panoIP;
        this.panoVersion = panoVersion;
        this.apiKey = apiKey;
    }

    public RestResponse getPolicyInfo(String clusterName, String policyName) {

        String getURL = String.format("https://" + panoIP + GET_SECRUITY_RULE_URL, panoVersion);

        Map<String, String> queryParams;
        RestClient rc = new RestClient(true);
        rc.getHeaders().add(Constant.PANO_HEADER_KEY_TYPE, apiKey);
        RestResponse response;

        queryParams = new HashMap<>();
        queryParams.put("location", "device-group");
        queryParams.put("device-group", clusterName);
        queryParams.put("name", policyName);
        log.info("Called GET for {}", policyName);
        response = rc.doGet(getURL, queryParams);

        return response;
    }

    public RestResponse putPolicyInfo(String clusterName, String policyName, ArrayList<Entry> postEntries) throws SystemException{

        String putURL = String.format("https://"+panoIP+UPDATE_SECRUITY_RULE_URL, panoVersion);

        PutBody putBody = new PutBody();
        Map<String, String> queryParams;
        RestClient rc = new RestClient(true);
        rc.getHeaders().add(Constant.PANO_HEADER_KEY_TYPE, apiKey);
        RestResponse response;

        queryParams = new HashMap<>();
        queryParams.put("location", "device-group");
        queryParams.put("device-group", clusterName);
        queryParams.put("name", policyName);


        putBody.setEntry(postEntries);
        log.debug("Calling PUT: [{}]", putURL);
        log.debug("Body: [{}]", putBody);
        //Let's update the policy now
        response = rc.doPut(putURL, putBody.toString(), queryParams);

        if (response.getStatus().is2xxSuccessful()) {
            log.info("Success -  address groups added to policy [{}] for cluster [{}]", policyName, clusterName);
            log.debug("Response [{}]", response);
        } else {
            throw new SystemException("Error Updating policy");
        }

        return response;
    }

}
