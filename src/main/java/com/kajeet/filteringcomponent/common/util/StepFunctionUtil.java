package com.kajeet.filteringcomponent.common.util;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;

public class StepFunctionUtil {

    public static String startStateMachine(Regions region, String stepFunctionArn, String sfnInput){

        AWSStepFunctions sfClient = AWSStepFunctionsClientBuilder.defaultClient();
        StartExecutionRequest request = new StartExecutionRequest()
                .withStateMachineArn(stepFunctionArn)
                .withInput(sfnInput);

        StartExecutionResult result = sfClient.startExecution(request);

        return "";
    }

}
