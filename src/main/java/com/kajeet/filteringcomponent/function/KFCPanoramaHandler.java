package com.kajeet.filteringcomponent.function;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.datadoghq.datadog_lambda_java.DDLambda;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kajeet.filteringcomponent.common.Constant;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import com.kajeet.filteringcomponent.common.util.SecretManagerUtils;
import com.kajeet.filteringcomponent.common.util.StepFunctionUtil;
import com.kajeet.filteringcomponent.model.SourceTransactionType;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KFCPanoramaHandler implements RequestHandler<SQSEvent, Void> {

    private static final String deviceUpdateMachineArn = System.getenv(Constant.DEVICE_UPDATE_MACHINE_ARN);
    private static final String policyUpdateMachineArn = System.getenv(Constant.POLICY_UPDATE_MACHINE_ARN);
    private static final String customUrlUpdateMachineArn = System.getenv(Constant.CUSTOM_URL_MACHINE_ARN);

    private static final String QUEUE_NAME = System.getenv(Constant.KFC_QUEUE);
    private static final Regions region = Regions.fromName(System.getenv("AWS_REGION"));
    private static final Logger log = LogManager.getLogger(com.kajeet.filteringcomponent.function.KFCPanoramaHandler.class.getName());
    private static  String panoApiUrl = "";
    private static  String panoApiVersion = "";
    private static  String panoApiKey = "";

    static {
        String secretsManager = System.getenv(Constant.PANO_KFC_SECRETS_MANAGER);
        panoApiUrl = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PANO_API_URL);
        panoApiVersion = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PANO_API_VERSION);
        panoApiKey = SecretManagerUtils.getSecretValueByName(secretsManager, Constant.SECRET_PANO_API_KEY);

        log.info("pano api url: {}", panoApiUrl);
        log.info("pano api version: {}", panoApiVersion);
        log.info("panoKey(last4): {}",panoApiKey.substring(panoApiKey.length()-4));
    }

    @Override
    public Void handleRequest(SQSEvent sqsEvent, Context context) {

        log.debug("KFCPanoramaHandler.handleRequest()");
        Map<String, String> envProps = System.getenv();

        Set<String> keys = envProps.keySet();
        int numKeys = keys.size();
        log.debug("Number of keys: {}", numKeys);
        for (String aKey: keys){
            log.debug("A Key: {}", aKey);
        }

        log.debug("Device Update SM ARN: {}", deviceUpdateMachineArn);
        log.debug("Policy Update SM ARN: {}", policyUpdateMachineArn);
        log.debug("Custom URL Update SM ARN: {}", customUrlUpdateMachineArn);
        log.debug("Region: {}", region.getName());

        List<SQSEvent.SQSMessage> sqsMsgList = new ArrayList<>();
        String asyncCommand = "";
        KFCAsyncMessage queueMessage = null;

        try {
            for (SQSEvent.SQSMessage msg : sqsEvent.getRecords()) {
                log.info("Processing msg: ID [{}]", msg.getMessageId());
                sqsMsgList.add(msg);
                log.debug("Message [{}]", msg.getBody());
                ObjectMapper objectMapper = new ObjectMapper();
                queueMessage = objectMapper.readValue(msg.getBody(), KFCAsyncMessage.class);
                queueMessage.setPanoUrl(panoApiUrl);
                queueMessage.setPanoVersion(panoApiVersion);
                queueMessage.setApiKey(panoApiKey);
                log.info("TransactionType: [{}]", queueMessage.getTransactionType());
                if (queueMessage.getTransactionType().equals(SourceTransactionType.PUT_CORP_POLICY)){
                    log.info("Starting policy state machine");
                    //Need to update the address group policies
                    StepFunctionUtil.startStateMachine(region, policyUpdateMachineArn, queueMessage.toString());
                }
                else if (queueMessage.getTransactionType().equals(SourceTransactionType.PUT_CUSTOM_URL) ||
                        queueMessage.getTransactionType().equals(SourceTransactionType.UPDATE_CUSTOM_URL)){
                    log.info("Starting customer urls state machine");
                    //Need to update the custom urls
                    StepFunctionUtil.startStateMachine(region, customUrlUpdateMachineArn, queueMessage.toString());
                }
                else if (queueMessage.getTransactionType().equals(SourceTransactionType.POST_DEVICES) ||
                        queueMessage.getTransactionType().equals(SourceTransactionType.DELETE_DEVICES)){
                    log.info("Starting device state machine");
                    //Need to add or delete an address from an address group
                    StepFunctionUtil.startStateMachine(region, deviceUpdateMachineArn, queueMessage.toString());
                }
            }
        } catch (Exception e) {
            log.error("Error in Panorama Handler: " + e.getMessage());
            e.printStackTrace();
            cleanUp(sqsMsgList);
            throw new RuntimeException(e.getMessage());
        } finally {
            cleanUp(sqsMsgList);
            // DataDog Logging:
            DDLambda ddl = new DDLambda(context);
            final Span span = GlobalTracer.get().activeSpan();
            if (span != null) {
                span.setTag(Constant.CLASS_DOT_NAME, this.getClass().getName());
                span.setTag(Constant.KFC_QUEUE, QUEUE_NAME);
                if (queueMessage != null) {
                    span.setTag(Constant.TRANSACTIONID, queueMessage.getTransactionId());
//                    span.setTag(Constant.TO_POLICY, queueMessage.getPolicyInfo().getToPolicy());
                    span.setTag(Constant.PANO_URL, queueMessage.getPanoUrl());
                    span.setTag(Constant.SOURCE_TRANSACTION_TYPE, queueMessage.getTransactionType()!=null?queueMessage.getTransactionType().getLabel():null);
                }
                span.setTag(Constant.ATTEMPT_DOT_NUMBER, 1);
                span.finish();
            }
        }
        return null;
    }

    private void deleteMessageFromSQSQueue(SQSEvent.SQSMessage msg) {
        log.info("KFCPanoramaHandler deleting message from [{}]", QUEUE_NAME);
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        String queueUrl = sqs.getQueueUrl(QUEUE_NAME).getQueueUrl();
        log.debug("queueUrl = " + queueUrl);
        sqs.deleteMessage(queueUrl, msg.getReceiptHandle());
    }

    private void cleanUp(List<SQSEvent.SQSMessage> sqsMsgList) {
        log.debug("cleanUp (ArrayList<SQSEvent.SQSMessage> sqsMsgList) -- size: " + sqsMsgList.size());
        for (SQSEvent.SQSMessage sqsMessage : sqsMsgList) {
            deleteMessageFromSQSQueue(sqsMessage);
        }
    }
}
