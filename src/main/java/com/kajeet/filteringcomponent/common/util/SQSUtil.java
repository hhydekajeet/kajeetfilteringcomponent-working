package com.kajeet.filteringcomponent.common.util;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

public class SQSUtil {
    private static final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

    public static String sendSQSMessage(String queueUrl, String message){

        String messageId = null;

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message)
                .withDelaySeconds(5);

        SendMessageResult smr = sqs.sendMessage(send_msg_request);
        messageId = smr.getMessageId();

        return messageId;
    }
}
