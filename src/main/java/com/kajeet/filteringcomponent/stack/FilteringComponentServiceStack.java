package com.kajeet.filteringcomponent.stack;

import com.kajeet.filteringcomponent.common.Constant;
import software.amazon.awscdk.*;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.services.apigateway.*;
import software.amazon.awscdk.services.apigateway.Resource;
import software.amazon.awscdk.services.apigateway.Stage;
import software.amazon.awscdk.services.cloudwatch.*;
import software.amazon.awscdk.services.cloudwatch.actions.SnsAction;
import software.amazon.awscdk.services.ec2.*;
import software.amazon.awscdk.services.iam.*;
import software.amazon.awscdk.services.kms.Key;
import software.amazon.awscdk.services.lambda.Runtime;
import software.amazon.awscdk.services.lambda.*;
import software.amazon.awscdk.services.lambda.eventsources.SqsEventSource;
import software.amazon.awscdk.services.logs.LogGroup;
import software.amazon.awscdk.services.logs.RetentionDays;
import software.amazon.awscdk.services.rds.*;
import software.amazon.awscdk.services.rds.InstanceProps;
import software.amazon.awscdk.services.secretsmanager.Secret;
import software.amazon.awscdk.services.secretsmanager.SecretStringGenerator;
import software.amazon.awscdk.services.sns.Topic;
import software.amazon.awscdk.services.sns.TopicProps;
import software.amazon.awscdk.services.sns.subscriptions.SqsSubscription;
import software.amazon.awscdk.services.sqs.Queue;
import software.amazon.awscdk.services.sqs.QueueProps;
import software.amazon.awscdk.services.stepfunctions.*;
import software.amazon.awscdk.services.stepfunctions.tasks.LambdaInvoke;
import software.constructs.Construct;

import java.util.*;
import java.util.Map;

public class FilteringComponentServiceStack extends Stack {

    private String CTX_APP_NAME     = "application.name";
    private String JAR_PATH         = "target/filteringcomponentservice-1.0.jar";
    private String CTX_ENV          = "environment";
    private String CTX_LOG_LEVEL    = "loggingLevel";
    private String CTX_VPC_ID       = "vpc.id";

    private String PROD_EVIRO       = "prod";

    private final String KAJEET     = "kajeet";
    private final String SERVICE    = "service";
    private final String FILTERING_COMPONENT = "filteringcomponent";
    private final String KFC        = "kfc";
    private final String POLICY     = "policy";
    private final String POLICIES   = "policies";
    private final String CUSTOM_URL = "customurl";
    private final String DEVICES    = "devices";
    private final String PATH_VAR_CORP_ID = "{corp_id}";
    private final String STATUS     = "status";
    private final String TRANSACTION_ID = "{transaction_id}";
    private final String IP_POOL    = "ippool";
    private final String CARRIER    = "{carrier}";
    public static final String PANORAMA_SECRETS = "pano_secrets";


    private final String DASH                  = "-";
    private final String LAMBDA_HANDLERS_ROLE  = "LambdaHandlersRole";
    private final String I_VPC_ID              = "VpcId";
    private final String LOGGING_POLICY        = "LoggingPolicy";
    private final String KMS_POLICY        = "KMSPolicy";
    private final String SECRETS_POLICY        = "SecretsPolicy";
    private final String KEY_ID                = "KeyId";
    private final String DB_SECURITY_GROUP_ID     = "RDSSecurityGroupId";
    private final String RDS_SECURITY_GROUP_NAME = "RDSSecurityGroup";
    private final String SECURITY_GROUP_INGRESS_ID = "SecurityGroupIngressId";
    private final String KFC_SUBNET_1_ID = "KFCSubnet1";
    private final String KFC_SUBNET_2_ID = "KFCSubnet2";
    private final String KFC_SUBNET_GROUP_ID = "KFCSubnetGroupId";
    private final String KFC_SUBNET_GROUP_NAME = "KFCSubnetGroup";
    private final String RDS_SECRET_ID = "RDSSecretId";
    private final String RDS_SECRET_NAME = "RDSSecret";
    private final String PANO_SECRET_ID = "PanoSecretId";
    private final String PANO_SECRET_NAME = "PanoSecret";
    private final String RDS_READ_WRITE_INSTANCE_ID = "RDSReadWriteInstanceId";
    private final String RDS_READ_ONLY_INSTANCE_ID = "RDSReadOnlyInstanceId";
    private final String RDS_PARAMETER_GROUP_ID = "RDSParameterGroupId";
    private final String RDS_CLUSTER_ID = "RDSClusterId";




    private final String LAMBDA_SECURITY_GROUP_ID     = "LambdaSecurityGroupId";
    private final String LAMBDA_SECURITY_GROUP_NAME = "LambdaSecurityGroup";
    private final String SQS_POLICY            = "SQSPolicy";
    private final String STEP_FUNCTION_POLICY = "StepFunctionPolicy";
    private final String API                   = "API";
    private final String API_DEPLOYMENT        = "ApiDeployment";
    private final String API_STAGE             = "DMApiStage";
    private final String API_KEY               = "ApiKey";
    private final String KFC_QUEUE             = "KFCQueue";
    private final String POLICIES_FUNCTION     = "PoliciesFunction";
    private final String DEVICES_FUNCTION      = "DevicesFunction";
    private final String KFC_FUNCTION          = "KfcFunction";
    private final String KFC_PANORAMA_FUNCTION = "KfcPanoramaFunction";
    private final String KFC_PANORAMA_DEVICE_UPDATE_FUNCTION = "KfcPanoramaDeviceUpdateFunction";
    private final String KFC_PANORAMA_POLICY_UPDATE_FUNCTION = "KfcPanoramaPolicyUpdateFunction";
    private final String KFC_PANORAMA_CUSTOM_URL_UPDATE_FUNCTION = "KfcPanoramaCustomUrlUpdateFunction";
    private final String KFC_PANORAMA_PENDING_CHANGES_STATUS_FUNCTION = "KfcPanoramaPendingChangesFunction";
    private final String KFC_PANORAMA_COMMIT_CHANGES_FUNCTION = "KfcPanoramaCommitChangesFunction";
    private final String KFC_PANORAMA_PUSH_CHANGES_FUNCTION = "KfcPanoramaPushChangesFunction";
    private final String KFC_PANORAMA_POLL_COMPLETE_FUNCTION = "KfcPanoramaPollCompleteFunction";
    private final String KFC_PANORAMA_MACHINE_COMPLETE_FUNCTION = "KfcPanoramaMachineCompleteFunction";
    private final String DEVICE_UPDATE_MACHINE_NAME = "DeviceUpdateStateMachine";
    private final String POLICY_UPDATE_MACHINE_NAME = "PolicyUpdateStateMachine";
    private final String CUSTOM_URL_MACHINE_NAME = "CustomUrlUpdateStateMachine";

    private final String KFC_PANORAMA_POLICIES_UPDATE_FUNCTION = "KfcPanoramaPoliciesUpdateFunction";
    private final String APPLICATION_JSON      = "application/json";
    private final String STATUS_CODE_200       = "{ \"statusCode\": \"200\" }";
    private final String USAGE_PLAN            = "UsagePlan";
    private final String ERROR_TOPIC           = "ErrorTopic";
    private final String ERROR4XX              = "4XXErrors";
    private final String STAT_SUM              = "sum";
    private final String COUNT                 = "Count";
    private final String NUM_REQUESTS          = "# Requests";

    private String environment;
    private String loggingLevel;
    private String appName;
    private static final String CTX_DD_LAYER_ARN = "dd.layer.arn";
    private static final String CTX_DD_ENV = "dd.env";

    private static final String CTX_DB_SUBNET_ID_1 = "db.subnet.id.1";
    private static final String CTX_DB_SUBNET_ID_2 = "db.subnet.id.2";
    private static final String KMS_CALLER_ACCOUNT = "kms:CallerAccount";
    private static final String KMS_VIA_SERVICE = "kms:ViaService";

    public FilteringComponentServiceStack(final Construct scope, final String id) {
        this(scope, id, null);
    } 
    public FilteringComponentServiceStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        //Get the variables from the environment
        getEnvironmentVariables();

        //Is this Production?
        boolean isProd = environment != null && environment.equals(PROD_EVIRO);

        //Lambda Environment Variables to pass to the Lambdas
        HashMap<String, String> environmentVars = new HashMap<>();
        environmentVars.put(Constant.ENVIRONMENT, environment);
        environmentVars.put(Constant.LOG_LEVEL, loggingLevel);
//        environmentVars.put(Constant.ODMP_BASE_URL, odmpBaseDomain);

        environmentVars.put(Constant.JAVA_TOOL_OPTIONS, Constant.DATADOG_JAVA_AGENT_JAR);
        environmentVars.put(Constant.DD_LOGS_INJECTION, Constant.TRUE);
        environmentVars.put(Constant.DD_JMXFETCH_ENABLED, Constant.FALSE);
        environmentVars.put(Constant.DD_TRACE_ENABLED, Constant.TRUE);
        environmentVars.put(Constant.DD_ENV, (String) this.getNode().tryGetContext(CTX_DD_ENV));
        environmentVars.put(Constant.DD_SERVICE, FILTERING_COMPONENT); /////////////////////////////////////////////////

//        PIPE-362/618
        Tags.of(this).add("env", (String)this.getNode().tryGetContext(CTX_DD_ENV));
        Tags.of(this).add("service", FILTERING_COMPONENT);

        ILayerVersion ddLayerVersion = LayerVersion.fromLayerVersionArn(this, appName +"DataDogLambdaLayer",
                        (String)this.getNode().tryGetContext(CTX_DD_LAYER_ARN));

        List<EndpointType> regionType = new ArrayList<>();
        regionType.add(EndpointType.REGIONAL);

        // Lambda Handler Role
        String roleID = isProd ? appName+LAMBDA_HANDLERS_ROLE : appName+LAMBDA_HANDLERS_ROLE+DASH + environment;
        String roleName = isProd ? appName+LAMBDA_HANDLERS_ROLE : appName+LAMBDA_HANDLERS_ROLE+DASH + environment;
        Role lambdaHandlerRole = new Role(this, roleID, RoleProps.builder()
                .roleName(roleName)
                .description("Role for Filtering Component Lambda Handlers")
                .assumedBy(ServicePrincipal.Builder.create("lambda.amazonaws.com").build())
                .build());

        //VPC that will be used
        String iVPCID = isProd ? appName+I_VPC_ID : appName+I_VPC_ID+DASH+environment;
        IVpc vpc = Vpc.fromLookup(this,iVPCID, VpcLookupOptions.builder()
                .vpcId((String) this.getNode().tryGetContext(CTX_VPC_ID))
                .build());

        //Security Groups
        //DB Security Group
        String rdsSecurityGroupID = isProd ? appName+DB_SECURITY_GROUP_ID :  appName+DB_SECURITY_GROUP_ID+DASH+environment;
        String rdsSecurityGroupDesc = isProd ? appName+" RDS Security Group" :  appName+" RDS Security Group"+DASH+environment;
        String rdsSecurityGroupName = isProd ? appName+RDS_SECURITY_GROUP_NAME :  appName+RDS_SECURITY_GROUP_NAME+DASH+environment;
        ISecurityGroup rdsSecurityGroup =  new SecurityGroup(this, rdsSecurityGroupID, SecurityGroupProps.builder()
                .allowAllOutbound(true)
                .description(rdsSecurityGroupDesc)
                .vpc(vpc)
                .securityGroupName(rdsSecurityGroupName)
                .build());
        // Adding ingress rule for access to db via Kajeet VPN
        rdsSecurityGroup.addIngressRule(Peer.ipv4("10.101.8.0/21"), Port.tcp(3306));

        //Lambda Security Group
        String lambdaSecurityGroupID = isProd ? appName+LAMBDA_SECURITY_GROUP_ID :  appName+LAMBDA_SECURITY_GROUP_ID+DASH+environment;
        String lambdaSecurityGroupDesc = isProd ? appName+" Lambda Security Group" :  appName+" Lambda Security Group"+DASH+environment;
        String lambdaSecurityGroupName = isProd ? appName+LAMBDA_SECURITY_GROUP_NAME :  appName+LAMBDA_SECURITY_GROUP_NAME+DASH+environment;
        ISecurityGroup lambdaSecurityGroup = new SecurityGroup(this, lambdaSecurityGroupID, SecurityGroupProps.builder()
                .allowAllOutbound(true)
                .description(lambdaSecurityGroupDesc)
                .vpc(vpc)
                .securityGroupName(lambdaSecurityGroupName)
                .build());


        String securityGroupIngressID = isProd ? appName+SECURITY_GROUP_INGRESS_ID :  appName+SECURITY_GROUP_INGRESS_ID+DASH+environment;
        CfnSecurityGroupIngress cfnSecurityGroupIngress = CfnSecurityGroupIngress.Builder.create(this, securityGroupIngressID)
                .ipProtocol("tcp")
                .description("description")
                .fromPort(3306)
                .groupId(rdsSecurityGroup.getSecurityGroupId())
                .sourceSecurityGroupId(lambdaSecurityGroup.getSecurityGroupId())
                .toPort(3306)
                .build();

        //Create the subnets
        String kfcSubnet1ID = isProd ? appName+KFC_SUBNET_1_ID :  appName+KFC_SUBNET_1_ID+DASH+environment;
        ISubnet kfcSubnet1 = (ISubnet) Subnet.fromSubnetId(this,kfcSubnet1ID, (String)this.getNode().tryGetContext(CTX_DB_SUBNET_ID_1));

        String kfcSubnet2ID = isProd ? appName+KFC_SUBNET_2_ID :  appName+KFC_SUBNET_2_ID+DASH+environment;
        ISubnet kfcSubnet2 = (ISubnet) Subnet.fromSubnetId(this,kfcSubnet2ID, (String)this.getNode().tryGetContext(CTX_DB_SUBNET_ID_2));

        String kfcSubnetGroupID = isProd ? appName+KFC_SUBNET_GROUP_ID :  appName+KFC_SUBNET_GROUP_ID+DASH+environment;
        String kfcSubnetGroupName = isProd ? appName+KFC_SUBNET_GROUP_NAME :  appName+KFC_SUBNET_GROUP_NAME+DASH+environment;
        String kfcSubnetGroupDesc = isProd ? appName+" KFC Subnet Group" :  appName+" KFC Subnet Group"+DASH+environment;
        SubnetGroup rdsSubnetGroup = SubnetGroup.Builder.create(this, kfcSubnetGroupID)
                .subnetGroupName(kfcSubnetGroupName)
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1,kfcSubnet2))
                        .availabilityZones(List.of("us-east-1a", "us-ease-1b"))
                        .onePerAz(false)
                        .build())
                .description(kfcSubnetGroupDesc)
                .build();

        //Logging policy
        PolicyStatement loggingPolicyStatement = PolicyStatement.Builder.create()
                .actions(List.of("logs:*"))
                .resources(List.of("arn:aws:logs:*:*:log-group:/aws/lambda/*FilteringComponentService*:*"))
                .effect(Effect.ALLOW)
                .build();

        String loggingPolicyName = isProd ? appName+LOGGING_POLICY : appName+LOGGING_POLICY+DASH+environment;
        Policy.Builder.create(this, "ANS Logging Policy")
                .statements(List.of(loggingPolicyStatement))
                .policyName(loggingPolicyName)
                .roles(List.of(lambdaHandlerRole))
                .build();

        //API Gateway
        String apigwId = isProd ? appName+API : appName+API+DASH+environment;
        String apigwApiName = isProd ? appName+API : appName+API+DASH+environment;

        RestApi api = RestApi.Builder.create(this, apigwId)
                .restApiName(apigwApiName)
                .description("This is the Filtering Component service API.")
                .endpointConfiguration(EndpointConfiguration.builder().types(regionType).build())
                .failOnWarnings(true)
                .cloudWatchRole(false)
                .build();

        String deploymentId = isProd ? appName+API_DEPLOYMENT :  appName+API_DEPLOYMENT+DASH+environment;
        Deployment deployment = Deployment.Builder.create(this, deploymentId)
                .api(api)
                .description("Deployment for Filtering Component Service Rest API")
                .build();


        LogGroup devLogGroup = new LogGroup(this, "DevLogs");

        String stageId = isProd ? appName+" ApiStage" :  appName+" ApiStage-"+environment;
        Stage stage = null;
        if (!isProd) {
            stage = Stage.Builder.create(this, stageId)
                    .stageName(environment)
                    .deployment(deployment)
                    .metricsEnabled(true)
                    .build();

            api.setDeploymentStage(stage);
        } else {
            stage = api.getDeploymentStage();
        }

        //Method Options
        MethodOptions methodOpts = MethodOptions.builder()
                .apiKeyRequired(true)
                .build();

        /*******************************************************
         * *****************************************************
         * Define RDS
         * *****************************************************
         * *****************************************************
         */
        // Build KMS Key/Policy Statement

        PolicyStatement kmsPolicyStatement = PolicyStatement.Builder.create()
                .actions(List.of("kms:*"))
                .resources(List.of("*"))
                .effect(Effect.ALLOW)
                .build();

        String kmsPolicyName = isProd ? appName+ KMS_POLICY : appName+KMS_POLICY+DASH+environment;
        String kmsPolicyId = isProd ? appName+ KMS_POLICY : appName+KMS_POLICY+DASH+environment;

        Policy.Builder.create(this, kmsPolicyId)
                .statements(List.of(kmsPolicyStatement))
                .policyName(kmsPolicyName)
                .roles(List.of(lambdaHandlerRole))
                .build();

        String keyID =  isProd ? appName+KEY_ID :  appName+KEY_ID+DASH+environment;
        String keyDesc =  isProd ? appName+" DB Key" :  appName+" DB Key"+DASH+environment;
        Key key = Key.Builder.create(this, keyID)
                .description(keyDesc)
                .enableKeyRotation(true)
                .enabled(true)
                .build();

        // Templated secret
        //This is the secret for RDS
        String rdsSecretID =  isProd ? appName+RDS_SECRET_ID :  appName+RDS_SECRET_ID+DASH+environment;
        String rdsSecretName = isProd ? appName+RDS_SECRET_NAME : appName+RDS_SECRET_NAME+DASH+environment;
        String rdsSecretDesc = isProd ? appName+" RDS Master Username and Password" :  appName+" RDS Master Username and Password"+DASH+environment;
        Secret rdsSecret = Secret.Builder.create(this, rdsSecretID )
                .generateSecretString(SecretStringGenerator.builder()
                        .secretStringTemplate("{\"username\":\"root\"}")
                        .generateStringKey("password")
                        .excludeCharacters("\"@/\\")
                        .passwordLength(16)
                        .build())
                .secretName(rdsSecretName)
                .encryptionKey(key)
                .description(rdsSecretDesc)
                .build();

        environmentVars.put(Constant.KFC_SECRETS_MANAGER, rdsSecret.getSecretName());

        //This is the secret for Pano
        String panoSecretID =  isProd ? appName+PANO_SECRET_ID :  appName+PANO_SECRET_ID+DASH+environment;
        String panoSecretName = isProd ? appName+PANO_SECRET_NAME : appName+PANO_SECRET_NAME+DASH+environment;
        String panoSecretDesc = isProd ? appName+" Secret for Panorama" :  appName+" Secret for Panorama"+DASH+environment;
        Secret panoSecret = Secret.Builder.create(this, panoSecretID )
                .generateSecretString(SecretStringGenerator.builder()
                        .secretStringTemplate("{\"username\":\"root\"}")
                        .generateStringKey("password")
                        .excludeCharacters("\"@/\\")
                        .passwordLength(16)
                        .build())
                .secretName(panoSecretName)
                .encryptionKey(key)
                .description(panoSecretDesc)
                .build();

        environmentVars.put(Constant.PANO_KFC_SECRETS_MANAGER, panoSecret.getSecretName());

        //Secrets policy
        PolicyStatement secretsPolicyStatement = PolicyStatement.Builder.create()
                .actions(List.of("secretsmanager:GetSecretValue",
                        "secretsmanager:DescribeSecret"))
                .resources(List.of(rdsSecret.getSecretArn(),
                        panoSecret.getSecretArn()))
                .effect(Effect.ALLOW)
                .build();

        String secretsPolicyName = isProd ? appName+ SECRETS_POLICY : appName+SECRETS_POLICY+DASH+environment;
        String secretsPolicyId = isProd ? appName+ SECRETS_POLICY : appName+SECRETS_POLICY+DASH+environment;

        Policy.Builder.create(this, secretsPolicyId)
                .statements(List.of(secretsPolicyStatement))
                .policyName(secretsPolicyName)
                .roles(List.of(lambdaHandlerRole))
                .build();

        // RDS Cluster
        String rdsClusterID =  isProd ? appName+RDS_CLUSTER_ID :  appName+RDS_CLUSTER_ID+environment;
        String rdsDatabaseName = "kfcdb";
        DatabaseCluster rdsCluster = DatabaseCluster.Builder.create(this, rdsClusterID)
                .engine(DatabaseClusterEngine.AURORA_MYSQL)
                .defaultDatabaseName(rdsDatabaseName)
                .clusterIdentifier(rdsClusterID)
                .subnetGroup(rdsSubnetGroup)
                .instanceProps(InstanceProps.builder()
                        .instanceType(InstanceType.of(InstanceClass.BURSTABLE3, InstanceSize.SMALL))
                        .vpc(vpc)
                        .securityGroups(List.of(rdsSecurityGroup))
                        .build())
                .storageEncrypted(true)
                .credentials(Credentials.fromSecret(rdsSecret,"root"))
                .build();

        // RDS Information as outputs
        new CfnOutput(this, rdsSecretName, CfnOutputProps.builder()
                .description("The RDS Secret name for the 'root' password")
                .value(rdsSecretName)
                .exportName(rdsSecretName)
                .build());

        new CfnOutput(this, RDS_READ_WRITE_INSTANCE_ID+"URL", CfnOutputProps.builder()
                .description("DBInstance1 Endpoint URL for "+ environment)
                .value("jdbc:mariadb:aurora://" + rdsCluster.getClusterEndpoint().getHostname() + ":3306/"+rdsDatabaseName)
                .exportName(RDS_READ_WRITE_INSTANCE_ID+"URL")
                .build());

        /*******************************************************
         * *****************************************************
         * Define SQS
         * *****************************************************
         * *****************************************************
         */

        String kfcQueueName = isProd ? appName+KFC_QUEUE :  appName+KFC_QUEUE+DASH + environment;
        QueueProps queueProps = QueueProps.builder()
                .retentionPeriod(Duration.hours(6))
                .visibilityTimeout(Duration.minutes(15))
                .queueName(kfcQueueName)
                .build();
        Queue kfcQueue = new Queue(this, kfcQueueName, queueProps);
        SqsSubscription sqsSubscription = SqsSubscription.Builder.create(kfcQueue)
                .rawMessageDelivery(true)
                .build();

        environmentVars.put(Constant.KFC_QUEUE, kfcQueue.getQueueName());
        environmentVars.put(Constant.KFC_QUEUE_URL, kfcQueue.getQueueUrl());

        //SQS policy
        PolicyStatement sqsPolicyStatement = PolicyStatement.Builder.create()
                .actions(List.of("sqs:DeleteMessage",
                        "sqs:ReceiveMessage",
                        "sqs:SendMessage"))
                .resources(List.of(kfcQueue.getQueueArn()))
                .effect(Effect.ALLOW)
                .build();

        String sqsPolicyName = isProd ? appName+ SQS_POLICY : appName+SQS_POLICY+DASH+environment;
        String sqsPolicyId = isProd ? appName+ SQS_POLICY : appName+SQS_POLICY+DASH+environment;

        Policy.Builder.create(this, sqsPolicyId)
                .statements(List.of(sqsPolicyStatement))
                .policyName(sqsPolicyName)
                .roles(List.of(lambdaHandlerRole))
                .build();

        /*******************************************************
         * *****************************************************
         * Define Step Functions here
         * *****************************************************
         * *****************************************************
         */
        // Kfc Panorama Device Update Function
        String kfcPanoramaDeviceUpdateFunctionName = isProd ? appName+DASH+KFC_PANORAMA_DEVICE_UPDATE_FUNCTION :  appName+DASH+KFC_PANORAMA_DEVICE_UPDATE_FUNCTION+DASH+environment;
        final Function kfcPanoramaDeviceUpdateFunction = Function.Builder.create(this, kfcPanoramaDeviceUpdateFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaDeviceUpdateFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaDeviceUpdate::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Policy Update Function
        String kfcPanoramaPolicyUpdateFunctionName = isProd ? appName+DASH+KFC_PANORAMA_POLICY_UPDATE_FUNCTION :  appName+DASH+KFC_PANORAMA_POLICY_UPDATE_FUNCTION+DASH+environment;
        final Function kfcPanoramaPolicyUpdateFunction = Function.Builder.create(this, kfcPanoramaPolicyUpdateFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaPolicyUpdateFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaPoliciesUpdate::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Custom URL Update Function
        String kfcPanoramaCustomUrlUpdateFunctionName = isProd ? appName+DASH+KFC_PANORAMA_CUSTOM_URL_UPDATE_FUNCTION :  appName+DASH+KFC_PANORAMA_CUSTOM_URL_UPDATE_FUNCTION+DASH+environment;
        final Function kfcPanoramaCustomUrlUpdateFunction = Function.Builder.create(this, kfcPanoramaCustomUrlUpdateFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaCustomUrlUpdateFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaCustomUrlUpdate::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Pending Changes Function
        String kfcPanoramaPendingChangesStatusFunctionName = isProd ? appName+DASH+KFC_PANORAMA_PENDING_CHANGES_STATUS_FUNCTION :  appName+DASH+KFC_PANORAMA_PENDING_CHANGES_STATUS_FUNCTION+DASH+environment;
        final Function kfcPanoramaPendingChangesStatusFunction = Function.Builder.create(this, kfcPanoramaPendingChangesStatusFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaPendingChangesStatusFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaPendingChangeStatus::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Commit Changes Function
        String kfcPanoramaCommitChangesFunctionName = isProd ? appName+DASH+KFC_PANORAMA_COMMIT_CHANGES_FUNCTION :  appName+DASH+KFC_PANORAMA_COMMIT_CHANGES_FUNCTION+DASH+environment;
        final Function kfcPanoramaCommitChangesFunction = Function.Builder.create(this, kfcPanoramaCommitChangesFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaCommitChangesFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaCommitChanges::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Push Changes Function
        String kfcPanoramaPushChangesFunctionName = isProd ? appName+DASH+KFC_PANORAMA_PUSH_CHANGES_FUNCTION :  appName+DASH+KFC_PANORAMA_PUSH_CHANGES_FUNCTION+DASH+environment;
        final Function kfcPanoramaPushChangesFunction = Function.Builder.create(this, kfcPanoramaPushChangesFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaPushChangesFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaPushChanges::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Poll Complete Function
        String kfcPanoramaPollCompleteFunctionName = isProd ? appName+DASH+KFC_PANORAMA_POLL_COMPLETE_FUNCTION :  appName+DASH+KFC_PANORAMA_POLL_COMPLETE_FUNCTION+DASH+environment;
        final Function kfcPanoramaPollCompleteFunction = Function.Builder.create(this, kfcPanoramaPollCompleteFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaPollCompleteFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaPollComplete::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        // Kfc Panorama Poll Complete Function
        String kfcPanoramaMachineCompleteFunctionName = isProd ? appName+DASH+KFC_PANORAMA_MACHINE_COMPLETE_FUNCTION :  appName+DASH+KFC_PANORAMA_MACHINE_COMPLETE_FUNCTION+DASH+environment;
        final Function kfcPanoramaMachineCompleteFunction = Function.Builder.create(this, kfcPanoramaMachineCompleteFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaMachineCompleteFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.statemachine.PanoramaStateMachineComplete::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        //This is for updating devices
        LambdaInvoke deviceUpdateTask = LambdaInvoke.Builder.create(this, "Device Update Task")
                .lambdaFunction(kfcPanoramaDeviceUpdateFunction)
                .resultPath("$")
                .build();

        LambdaInvoke devicePendingChangesTask = LambdaInvoke.Builder.create(this, "Device Pending Changes Task")
                .lambdaFunction(kfcPanoramaPendingChangesStatusFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke deviceCommitChangesTask = LambdaInvoke.Builder.create(this, "Device Commit Changes Task")
                .lambdaFunction(kfcPanoramaCommitChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke devicePushChangesTask = LambdaInvoke.Builder.create(this, "Device Push Changes Task")
                .lambdaFunction(kfcPanoramaPushChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke devicePollCompleteTask = LambdaInvoke.Builder.create(this, "Device Poll Async")
                .lambdaFunction(kfcPanoramaPollCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        RetryProps completePollRetry = RetryProps.builder()
                .maxAttempts(10)
                .interval(Duration.seconds(30))
                .errors(List.of("com.kajeet.filteringcomponent.common.exception.JobNotCompleteException"))
                .build();

        devicePollCompleteTask.addRetry(completePollRetry);

        LambdaInvoke deviceCleanUpTask = LambdaInvoke.Builder.create(this, "Device Clean Up")
                .lambdaFunction(kfcPanoramaMachineCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();


        Choice deviceUpdateChoice = new Choice(this, "Device(s) updated?");
        Condition deviceConditionInitComplete = Condition.stringEquals("$.Payload.status","SUCCESS");

        //Define the state machine for updating devices
        Choice devicePendingJobs = new Choice(this, "Device jobs pending?");
        Condition devicePendingJobsCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice devicePendingJobsTypeChoice = new Choice(this, "Device local job pending?");
        Condition devicePendingJobTypeLocalCondition = Condition.stringEquals("$.Payload.pendingType","local");
        Condition devicePendingJobTypeDeviceTypeCondition = Condition.stringEquals("$.Payload.pendingType","device-group");

        Choice deviceCommitChangesChoice = new Choice(this, "Device changes committed?");
        Condition deviceCommitChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition deviceCommitChangesRetryConditions = Condition.stringEquals("$.Payload.retryCommit","YES");

        Choice devicePushChangesChoice = new Choice(this, "Device changes pushed?");
        Condition devicePushChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice devicePollJobCompleteChoice = new Choice(this, "Device job complete?");
        Condition devicePollJobCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition devicePollJobCompleteRetryCondition = Condition.stringEquals("$.Payload.retryPolling","YES");

        Pass deviceEndState = new Pass(this, "DevicesUpdated");

        Chain deviceUpdateChainDefinition = Chain.start(deviceUpdateTask)
                .next(deviceUpdateChoice.when(deviceConditionInitComplete,
                                devicePendingChangesTask.next(devicePendingJobs.when(devicePendingJobsCondition,
                                                devicePendingJobsTypeChoice.when(devicePendingJobTypeLocalCondition,
                                                                deviceCommitChangesTask.next(deviceCommitChangesChoice.when(deviceCommitChangesCompleteCondition,
                                                                        devicePollCompleteTask.next(devicePollJobCompleteChoice.when(devicePollJobCompleteCondition,
                                                                                devicePendingChangesTask)))))
                                                        .when(devicePendingJobTypeDeviceTypeCondition,
                                                                devicePushChangesTask.next(devicePushChangesChoice.when(devicePushChangesCompleteCondition,
                                                                        devicePollCompleteTask))))
                                        .otherwise(deviceCleanUpTask.next(deviceEndState))))
                        .otherwise(deviceCleanUpTask));

        String deviceUpdateStateMachineName = isProd ? appName+DASH+DEVICE_UPDATE_MACHINE_NAME :  appName+DASH+DEVICE_UPDATE_MACHINE_NAME+DASH+environment;
        StateMachine deviceUpdateStateMachine = StateMachine.Builder.create(this, deviceUpdateStateMachineName)
                .definition(deviceUpdateChainDefinition)
                .build();

        environmentVars.put(Constant.DEVICE_UPDATE_MACHINE_ARN, deviceUpdateStateMachine.getStateMachineArn());

        //This is for updating policies
        LambdaInvoke policyUpdateTask = LambdaInvoke.Builder.create(this, "Policy Update Task")
                .lambdaFunction(kfcPanoramaPolicyUpdateFunction)
                .resultPath("$")
                .build();

        LambdaInvoke policyPendingChangesTask = LambdaInvoke.Builder.create(this, "Policy Pending Changes Task")
                .lambdaFunction(kfcPanoramaPendingChangesStatusFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke policyCommitChangesTask = LambdaInvoke.Builder.create(this, "Policy Commit Changes Task")
                .lambdaFunction(kfcPanoramaCommitChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke policyPushChangesTask = LambdaInvoke.Builder.create(this, "Policy Push Changes Task")
                .lambdaFunction(kfcPanoramaPushChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke policyPollCompleteTask = LambdaInvoke.Builder.create(this, "Policy Poll Async")
                .lambdaFunction(kfcPanoramaPollCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        policyPollCompleteTask.addRetry(completePollRetry);

        LambdaInvoke policyCleanUpTask = LambdaInvoke.Builder.create(this, "Policy Clean Up")
                .lambdaFunction(kfcPanoramaMachineCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        //Define the state machine for updating policies
        Choice policyUpdateChoice = new Choice(this, "Policy updated?");
        Condition policyConditionInitComplete = Condition.stringEquals("$.Payload.status","SUCCESS");

        Choice policyPendingJobs = new Choice(this, "Policy jobs pending?");
        Condition policyPendingJobsCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice policyPendingJobsTypeChoice = new Choice(this, "Policy local job pending?");
        Condition policyPendingJobTypeLocalCondition = Condition.stringEquals("$.Payload.pendingType","local");
        Condition policyPendingJobTypePolicyTypeCondition = Condition.stringEquals("$.Payload.pendingType","device-group");

        Choice policyCommitChangesChoice = new Choice(this, "Policy changes committed?");
        Condition policyCommitChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition policyCommitChangesRetryConditions = Condition.stringEquals("$.Payload.retryCommit","YES");

        Choice policyPushChangesChoice = new Choice(this, "Policy changes pushed?");
        Condition policyPushChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice policyPollJobCompleteChoice = new Choice(this, "Policy Job complete?");
        Condition policyPollJobCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition policyPollJobCompleteRetryCondition = Condition.stringEquals("$.Payload.retryPolling","YES");

        Pass policyEndState = new Pass(this, "PolicyUpdated");

        //Define the state machine for updating policies
        Chain policyUpdateChainDefinition = Chain.start(policyUpdateTask)
                .next(policyUpdateChoice.when(policyConditionInitComplete,
                                policyPendingChangesTask.next(policyPendingJobs.when(policyPendingJobsCondition,
                                                policyPendingJobsTypeChoice.when(policyPendingJobTypeLocalCondition,
                                                                policyCommitChangesTask.next(policyCommitChangesChoice.when(policyCommitChangesCompleteCondition,
                                                                        policyPollCompleteTask.next(policyPollJobCompleteChoice.when(policyPollJobCompleteCondition,
                                                                                policyPendingChangesTask)))))
                                                        .when(policyPendingJobTypePolicyTypeCondition,
                                                                policyPushChangesTask.next(policyPushChangesChoice.when(policyPushChangesCompleteCondition,
                                                                        policyPollCompleteTask))))
                                        .otherwise(policyCleanUpTask.next(policyEndState))))
                        .otherwise(policyCleanUpTask));

        String policyUpdateStateMachineName = isProd ? appName+DASH+POLICY_UPDATE_MACHINE_NAME :  appName+DASH+POLICY_UPDATE_MACHINE_NAME+DASH+environment;
        StateMachine policyUpdateStateMachine = StateMachine.Builder.create(this, policyUpdateStateMachineName)
                .definition(policyUpdateChainDefinition)
                .build();

        environmentVars.put(Constant.POLICY_UPDATE_MACHINE_ARN, policyUpdateStateMachine.getStateMachineArn());

        //This is for updating custom urls
        LambdaInvoke customUrlUpdateTask = LambdaInvoke.Builder.create(this, "CustomUrl Update Task")
                .lambdaFunction(kfcPanoramaCustomUrlUpdateFunction)
                .resultPath("$")
                .build();

        LambdaInvoke customUrlPendingChangesTask = LambdaInvoke.Builder.create(this, "CustomUrl Pending Changes Task")
                .lambdaFunction(kfcPanoramaPendingChangesStatusFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke customUrlCommitChangesTask = LambdaInvoke.Builder.create(this, "CustomUrl Commit Changes Task")
                .lambdaFunction(kfcPanoramaCommitChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke customUrlPushChangesTask = LambdaInvoke.Builder.create(this, "CustomUrl Push Changes Task")
                .lambdaFunction(kfcPanoramaPushChangesFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        LambdaInvoke customUrlPollCompleteTask = LambdaInvoke.Builder.create(this, "CustomUrl Poll Async")
                .lambdaFunction(kfcPanoramaPollCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        customUrlPollCompleteTask.addRetry(completePollRetry);

        LambdaInvoke customUrlCleanUpTask = LambdaInvoke.Builder.create(this, "CustomUrl Clean Up")
                .lambdaFunction(kfcPanoramaMachineCompleteFunction)
                .inputPath("$.Payload")
                .resultPath("$")
                .build();

        //Define the state machine for updating policies
        Choice customUrlUpdateChoice = new Choice(this, "CustomUrl updated?");
        Condition customUrlConditionInitComplete = Condition.stringEquals("$.Payload.status","SUCCESS");

        Choice customUrlPendingJobs = new Choice(this, "CustomUrl jobs pending?");
        Condition customUrlPendingJobsCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice customUrlPendingJobsTypeChoice = new Choice(this, "CustomUrl local job pending?");
        Condition customUrlPendingJobTypeLocalCondition = Condition.stringEquals("$.Payload.pendingType","local");
        Condition customUrlPendingJobTypeCustomUrlTypeCondition = Condition.stringEquals("$.Payload.pendingType","device-group");

        Choice customUrlCommitChangesChoice = new Choice(this, "CustomUrl changes committed?");
        Condition customUrlCommitChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition customUrlCommitChangesRetryConditions = Condition.stringEquals("$.Payload.retryCommit","YES");

        Choice customUrlPushChangesChoice = new Choice(this, "CustomUrl changes pushed?");
        Condition customUrlPushChangesCompleteCondition = Condition.stringEquals("$.Payload.status","YES");

        Choice customUrlPollJobCompleteChoice = new Choice(this, "CustomUrl Job complete?");
        Condition customUrlPollJobCompleteCondition = Condition.stringEquals("$.Payload.status","YES");
        Condition customUrlPollJobCompleteRetryCondition = Condition.stringEquals("$.Payload.retryPolling","YES");

        Pass customUrlEndState = new Pass(this, "CustomUrlUpdated");

        //Define the state machine for updating policies
        Chain customUrlUpdateChainDefinition = Chain.start(customUrlUpdateTask)
                .next(customUrlUpdateChoice.when(customUrlConditionInitComplete,
                                customUrlPendingChangesTask.next(customUrlPendingJobs.when(customUrlPendingJobsCondition,
                                                customUrlPendingJobsTypeChoice.when(customUrlPendingJobTypeLocalCondition,
                                                                customUrlCommitChangesTask.next(customUrlCommitChangesChoice.when(customUrlCommitChangesCompleteCondition,
                                                                        customUrlPollCompleteTask.next(customUrlPollJobCompleteChoice.when(customUrlPollJobCompleteCondition,
                                                                                customUrlPendingChangesTask)))))
                                                        .when(customUrlPendingJobTypeCustomUrlTypeCondition,
                                                                customUrlPushChangesTask.next(customUrlPushChangesChoice.when(customUrlPushChangesCompleteCondition,
                                                                        customUrlPollCompleteTask))))
                                        .otherwise(customUrlCleanUpTask.next(customUrlEndState))))
                        .otherwise(customUrlCleanUpTask));

        String customUrlUpdateStateMachineName = isProd ? appName+DASH+CUSTOM_URL_MACHINE_NAME :  appName+DASH+CUSTOM_URL_MACHINE_NAME+DASH+environment;
        StateMachine customUrlUpdateStateMachine = StateMachine.Builder.create(this, customUrlUpdateStateMachineName)
                .definition(customUrlUpdateChainDefinition)
                .build();

        environmentVars.put(Constant.CUSTOM_URL_MACHINE_ARN, customUrlUpdateStateMachine.getStateMachineArn());

        //Policy to allow lambda to access Step Functions
        PolicyStatement stepFunctionPolicyStatement = PolicyStatement.Builder.create()
                .actions(List.of("states:StartExecution"))
                .resources(List.of(deviceUpdateStateMachine.getStateMachineArn(),
                        policyUpdateStateMachine.getStateMachineArn(),
                        customUrlUpdateStateMachine.getStateMachineArn()))
                .effect(Effect.ALLOW)
                .build();

        String stepFunctionPolicyName = isProd ? appName+ STEP_FUNCTION_POLICY : appName+STEP_FUNCTION_POLICY+DASH+environment;
        String stepFunctionPolicyId = isProd ? appName+ STEP_FUNCTION_POLICY : appName+STEP_FUNCTION_POLICY+DASH+environment;

        Policy.Builder.create(this, stepFunctionPolicyId)
                .statements(List.of(stepFunctionPolicyStatement))
                .policyName(stepFunctionPolicyName)
                .roles(List.of(lambdaHandlerRole))
                .build();

        /*******************************************************
         * *****************************************************
         * Define Resources (paths), Functions, and Integrations here
         * *****************************************************
         * *****************************************************
         */

        //Resource(s) for policy
        Resource filteringComponentResource =  api.getRoot().addResource(KAJEET)
                .addResource(SERVICE)
                .addResource(KFC);

        Resource kfcPolicyResource =  filteringComponentResource.addResource(POLICY);
        Resource kfcPoliciesResource =  filteringComponentResource.addResource(POLICIES);
        Resource policyCorpIdResource = kfcPolicyResource.addResource(PATH_VAR_CORP_ID);
        Resource customUrlResource = kfcPolicyResource.addResource(CUSTOM_URL);
        Resource customUrlCorpIdResource = customUrlResource.addResource(PATH_VAR_CORP_ID);

        lambdaHandlerRole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName("service-role/AWSLambdaVPCAccessExecutionRole"));

        // Kfc Policies function
        String kfcPoliciesFunctionName = isProd ? appName+DASH+POLICIES_FUNCTION :  appName+DASH+POLICIES_FUNCTION+DASH+environment;
        final Function kfcPoliciesFunction = Function.Builder.create(this, kfcPoliciesFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPoliciesFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.KfcPoliciesHandler::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        Version kfcPoliciesFunctionVersion = Version.Builder.create(this, appName +"kfcPoliciesFunction")
                .lambda(kfcPoliciesFunction)
                .provisionedConcurrentExecutions(1)
                .build();

        Alias kfcPoliciesFunctionAlias = Alias.Builder.create(this, appName+"kfcPoliciesFunctionAlias")
                .aliasName("kfcPoliciesLambdaAlias")
                .version(kfcPoliciesFunctionVersion)
                .build();

        LambdaIntegration kfcPoliciesFunctionIntegration = LambdaIntegration.Builder.create(kfcPoliciesFunction)
                .requestTemplates(new HashMap<String, String>() {
                    {
                        put(APPLICATION_JSON, STATUS_CODE_200);
                    }
                })
                .build();

        //Add Methods here
        kfcPoliciesResource.addMethod(Constant.HTTP_GET, kfcPoliciesFunctionIntegration, methodOpts);
        kfcPolicyResource.addMethod(Constant.HTTP_POST, kfcPoliciesFunctionIntegration, methodOpts);
        policyCorpIdResource.addMethod(Constant.HTTP_GET, kfcPoliciesFunctionIntegration, methodOpts);
        customUrlResource.addMethod(Constant.HTTP_DELETE, kfcPoliciesFunctionIntegration, methodOpts);
        customUrlResource.addMethod(Constant.HTTP_PUT, kfcPoliciesFunctionIntegration, methodOpts);
        customUrlCorpIdResource.addMethod(Constant.HTTP_GET, kfcPoliciesFunctionIntegration, methodOpts);

        Resource kfcDevicesResource =  filteringComponentResource.addResource(DEVICES);
        Resource devicesCorpIdResource = kfcDevicesResource.addResource(PATH_VAR_CORP_ID);

        // Kfc Policies function
        String kfcDevicesFunctionName = isProd ? appName+DASH+DEVICES_FUNCTION :  appName+DASH+DEVICES_FUNCTION+DASH+environment;
        final Function kfcDevicesFunction = Function.Builder.create(this, kfcDevicesFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcDevicesFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.KfcDevicesHandler::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        Version kfcDevicesFunctionVersion = Version.Builder.create(this, appName +"kfcDevicesFunction")
                .lambda(kfcDevicesFunction)
                .provisionedConcurrentExecutions(1)
                .build();

        Alias kfcDevicesFunctionAlias = Alias.Builder.create(this, appName+"kfcDevicesFunctionAlias")
                .aliasName("kfcDevicesLambdaAlias")
                .version(kfcDevicesFunctionVersion)
                .build();

        LambdaIntegration kfcDevicesFunctionIntegration = LambdaIntegration.Builder.create(kfcDevicesFunction)
                .requestTemplates(new HashMap<String, String>() {
                    {
                        put(APPLICATION_JSON, STATUS_CODE_200);
                    }
                })
                .build();

        //Add Methods here
        kfcDevicesResource.addMethod(Constant.HTTP_POST, kfcDevicesFunctionIntegration, methodOpts);
        kfcDevicesResource.addMethod(Constant.HTTP_DELETE, kfcDevicesFunctionIntegration, methodOpts);
        devicesCorpIdResource.addMethod(Constant.HTTP_GET, kfcDevicesFunctionIntegration, methodOpts);


        Resource kfcStatusResource =  filteringComponentResource.addResource(STATUS);
        Resource statusTransactionIdResource =  kfcStatusResource.addResource(TRANSACTION_ID);
        Resource kfcIpPoolResource =  filteringComponentResource.addResource(IP_POOL);
        Resource ipPoolCarrierResource =  kfcIpPoolResource.addResource(CARRIER);

        // Kfc Handler function
        String kfcFunctionName = isProd ? appName+DASH+KFC_FUNCTION :  appName+DASH+KFC_FUNCTION+DASH+environment;
        final Function kfcFunction = Function.Builder.create(this, kfcFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.KfcHandler::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        Version kfcFunctionVersion = Version.Builder.create(this, appName +"kfcFunction")
                .lambda(kfcFunction)
                .provisionedConcurrentExecutions(1)
                .build();

        Alias kfcFunctionAlias = Alias.Builder.create(this, appName+"kfcFunctionAlias")
                .aliasName("kfcLambdaAlias")
                .version(kfcFunctionVersion)
                .build();

        LambdaIntegration kfcFunctionIntegration = LambdaIntegration.Builder.create(kfcFunction)
                .requestTemplates(new HashMap<String, String>() {
                    {
                        put(APPLICATION_JSON, STATUS_CODE_200);
                    }
                })
                .build();


        //Add Methods here
        statusTransactionIdResource.addMethod(Constant.HTTP_GET, kfcFunctionIntegration, methodOpts);
        ipPoolCarrierResource.addMethod(Constant.HTTP_GET, kfcFunctionIntegration, methodOpts);


        // Kfc Panorama Handler function
        String kfcPanoramaHandlerFunctionName = isProd ? appName+DASH+KFC_PANORAMA_FUNCTION :  appName+DASH+KFC_PANORAMA_FUNCTION+DASH+environment;
        final Function kfcPanoramaHandlerFunction = Function.Builder.create(this, kfcPanoramaHandlerFunctionName)
                .runtime(Runtime.JAVA_11)
                .functionName(kfcPanoramaHandlerFunctionName)
                .timeout(Duration.seconds(900))
                .memorySize(2048)
                .environment(environmentVars)
                .code(Code.fromAsset(JAR_PATH))
                .handler("com.kajeet.filteringcomponent.function.KFCPanoramaHandler::handleRequest")
                .layers(List.of(ddLayerVersion))
                .logRetention(RetentionDays.THREE_MONTHS)
                .securityGroups(List.of(lambdaSecurityGroup))
                .vpc(vpc)
                .vpcSubnets(SubnetSelection.builder()
                        .subnets(List.of(kfcSubnet1, kfcSubnet2))
                        .build())
                .role(lambdaHandlerRole)
                .build();

        Version kfcPanoramaFunctionVersion = Version.Builder.create(this, appName +"kfcPanoramaFunction")
                .lambda(kfcPanoramaHandlerFunction)
                .provisionedConcurrentExecutions(1)
                .build();

        //Let this lambda consume messages from the queue
        kfcQueue.grantConsumeMessages(kfcPanoramaHandlerFunction);
        kfcPanoramaHandlerFunction.addEventSource(new SqsEventSource(kfcQueue));

        // API Key
        String apiKeyId = isProd ? appName+API_KEY :  appName+API_KEY+DASH + environment;
        String apiKeyName = isProd ? appName+API_KEY :  appName+API_KEY+DASH + environment;

        ApiKey apiKey = ApiKey.Builder.create(this, apiKeyId)
                .apiKeyName(apiKeyName)
                .description("API Key for " + appName + " API Gateway")
                .enabled(true)
                .build();

        // Usage Plan to associate with API Key
        UsagePlanPerApiStage usagePlanPerApiStage = UsagePlanPerApiStage.builder()
                .api(api)
                .stage(stage)
                .build();

        List<UsagePlanPerApiStage> usagePlanList = new ArrayList<>();
        usagePlanList.add(usagePlanPerApiStage);

        String usagePlanId = isProd ? appName+USAGE_PLAN :  appName+USAGE_PLAN+DASH + environment;
        UsagePlan usagePlan = UsagePlan.Builder.create(this, usagePlanId)
                .apiStages(usagePlanList)
                .description("Usage Plan for " + appName)
                .build();

        usagePlan.addApiKey(apiKey);


        /*******************************************************
         * *****************************************************
         * Define Alarms here
         * *****************************************************
         * *****************************************************
         */

        // Error Topic
        String errorTopicId = isProd ? appName+ERROR_TOPIC :  appName+ERROR_TOPIC+DASH + environment;
        String errorTopicName = isProd ? appName+ERROR_TOPIC :  appName+ERROR_TOPIC+DASH + environment;

        // Create SNS Topic for error alarms
        Topic errorTopic = new Topic(this, errorTopicId, TopicProps.builder()
                .topicName(errorTopicName)
                .displayName(errorTopicName)
                .build());

        // ---------------------------------------------------
        // Create ApiGateway 4xx Error Percentage Metric/Alarm
        // ---------------------------------------------------
        Metric m1 = this.metricForApiGateway(api.getRestApiId(), environment, ERROR4XX, ERROR4XX, STAT_SUM);
        Metric m2 = this.metricForApiGateway(api.getRestApiId(), environment, COUNT, NUM_REQUESTS, STAT_SUM);
        Map<String, Metric> apiGateway4xxErrorPctMap = new HashMap<>();
        apiGateway4xxErrorPctMap.put("m1", m1);
        apiGateway4xxErrorPctMap.put("m2", m2);

        MathExpression apiGateway4xxErrorPercentage = MathExpression.Builder.create()
                .expression("m1/m2*100")
                .label("% API Gateway 4xx Errors")
                .usingMetrics(apiGateway4xxErrorPctMap)
                .period(Duration.minutes(5)).build();

        // 4xx User Errors
        Alarm apiGateway4xxAlarm = Alarm.Builder.create(this, "API Gateway 4XX Errors > 0")
                .metric(this.metricForApiGateway(api.getRestApiName(), environment.toLowerCase(), "4XXError", "4XX Errors","Average"))
                .threshold(0)
                .evaluationPeriods(1)
                .datapointsToAlarm(1)
                .comparisonOperator(ComparisonOperator.GREATER_THAN_THRESHOLD)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"ApiGateway4xxAlarm")
                .build();
        apiGateway4xxAlarm.addAlarmAction(new SnsAction(errorTopic));
        apiGateway4xxAlarm.addOkAction(new SnsAction(errorTopic));


        // 5xx internal Server Errors
        Alarm apiGateway5xxAlarm = Alarm.Builder.create(this, "API Gateway 5XX Errors > 0")
                .metric(this.metricForApiGateway(api.getRestApiName(), environment, "5XXError", "5XX Errors","Average"))
                .threshold(0)
                .evaluationPeriods(1)
                .datapointsToAlarm(1)
                .comparisonOperator(ComparisonOperator.GREATER_THAN_THRESHOLD)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"ApiGateway5xxAlarm")
                .build();
        apiGateway5xxAlarm.addAlarmAction(new SnsAction(errorTopic));
        apiGateway5xxAlarm.addOkAction(new SnsAction(errorTopic));

        // API Latency >= 1s
        Alarm apiGatewayLatency1sAlarm = Alarm.Builder.create(this, "API p99 latency alarm >= 1s")
                .metric(this.metricForApiGateway(api.getRestApiName(), environment, "Latency", "API GW Latency", "p99"))
                .threshold(1000)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"ApiGatewayLatencyP99Alarm")
                .build();
        apiGatewayLatency1sAlarm.addAlarmAction(new SnsAction(errorTopic));

        // ---------------------------------------------------
        // Policy Function Error Metrics
        // ---------------------------------------------------
        Metric invokeKfcPoliciesFunctionMetric = kfcPoliciesFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric errorPoliciesFunctionMetric = kfcPoliciesFunction.metric("Errors", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcPoliciesFunctionErrorPctMap = new HashMap<>(){{
            put("i", invokeKfcPoliciesFunctionMetric);
            put("e", errorPoliciesFunctionMetric);
        }};

        MathExpression kfcPoliciesFunctionErrorPercentage = MathExpression.Builder.create()
                .expression("(e / i) * 100")
                .label("% of invocation errors, last 5 mins")
                .usingMetrics(kfcPoliciesFunctionErrorPctMap)
                .period(Duration.minutes(5)).build();
        
        // ---------------------------------------------------
        // KFC Policies Function Throttle Metric
        // ---------------------------------------------------
        Metric throttleKfcPoliciesFunctionMetric = kfcPoliciesFunction.metric("Throttles", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric tInvokeKfcPoliciesFunctionMetric = kfcPoliciesFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcPoliciesFunctionThrottleMap = new HashMap<>(){{
            put("t", throttleKfcPoliciesFunctionMetric);
            put("i", tInvokeKfcPoliciesFunctionMetric);
        }};

        MathExpression kfcPoliciesFunctionThrottlePercentage = MathExpression.Builder.create()
                .expression("(t / (i + t)) * 100")
                .label("% of throttled requests, last 30 mins")
                .usingMetrics(kfcPoliciesFunctionThrottleMap)
                .period(Duration.minutes(5)).build();


        //*************************************
        //KFC Policies Function
        //*************************************

        // KFC Policies Function Errors > 2%
        Alarm kfcPoliciesFunctionErrorPctAlarm = Alarm.Builder.create(this, "KFC Policies Function Errors > 2%")
                .metric(kfcPoliciesFunctionErrorPercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PoliciesFunctionError2PctAlarm")
                .build();
        kfcPoliciesFunctionErrorPctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // 1% of policy function invocations taking longer than 1 second
        Alarm kfcPoliciesFunctionInvocationAlarm = Alarm.Builder.create(this, "KFC Policies Function p99 Long Duration (>1s)")
                .metric(kfcPoliciesFunction.metricDuration()
                        .with(MetricOptions.builder()
                                .statistic("p99")
                                .period(Duration.seconds(60))
                                .build()))
                .threshold(10000)
                .evaluationPeriods(5)
                .datapointsToAlarm(5)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PoliciesFunctionErrorPct99gt1sAlarm")
                .build();
        kfcPoliciesFunctionInvocationAlarm.addAlarmAction(new SnsAction(errorTopic));

        // KFC Policies Function Throttle > 2%
        Alarm kfcPoliciesFunctionErrorThrottlePctAlarm = Alarm.Builder.create(this, "KFC Policies Function 2% Throttled")
                .metric(kfcPoliciesFunctionThrottlePercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PoliciesFunctionErrorThrottleAlarm")
                .build();
        kfcPoliciesFunctionErrorThrottlePctAlarm.addAlarmAction(new SnsAction(errorTopic));


        // ---------------------------------------------------
        // Device Function Error Metrics
        // ---------------------------------------------------
        Metric invokeKfcDevicesFunctionMetric = kfcDevicesFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric errorDevicesFunctionMetric = kfcDevicesFunction.metric("Errors", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcDevicesFunctionErrorPctMap = new HashMap<>(){{
            put("i", invokeKfcDevicesFunctionMetric);
            put("e", errorDevicesFunctionMetric);
        }};

        MathExpression kfcDevicesFunctionErrorPercentage = MathExpression.Builder.create()
                .expression("(e / i) * 100")
                .label("% of invocation errors, last 5 mins")
                .usingMetrics(kfcDevicesFunctionErrorPctMap)
                .period(Duration.minutes(5)).build();


        // ---------------------------------------------------
        // KFC Devices Function Throttle Metric
        // ---------------------------------------------------
        Metric throttleKfcDevicesFunctionMetric = kfcDevicesFunction.metric("Throttles", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric tInvokeKfcDevicesFunctionMetric = kfcDevicesFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcDevicesFunctionThrottleMap = new HashMap<>(){{
            put("t", throttleKfcDevicesFunctionMetric);
            put("i", tInvokeKfcDevicesFunctionMetric);
        }};

        MathExpression kfcDevicesFunctionThrottlePercentage = MathExpression.Builder.create()
                .expression("(t / (i + t)) * 100")
                .label("% of throttled requests, last 30 mins")
                .usingMetrics(kfcDevicesFunctionThrottleMap)
                .period(Duration.minutes(5)).build();


        //*************************************
        //KFC Devices Function
        //*************************************

        // KFC Devices Function Errors > 2%
        Alarm kfcDevicesFunctionErrorPctAlarm = Alarm.Builder.create(this, "KFC Devices Function Errors > 2%")
                .metric(kfcDevicesFunctionErrorPercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"DevicesFunctionError2PctAlarm")
                .build();
        kfcDevicesFunctionErrorPctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // 1% of device function invocations taking longer than 1 second
        Alarm kfcDevicesFunctionInvocationAlarm = Alarm.Builder.create(this, "KFC Devices Function p99 Long Duration (>1s)")
                .metric(kfcDevicesFunction.metricDuration()
                        .with(MetricOptions.builder()
                                .statistic("p99")
                                .period(Duration.seconds(60))
                                .build()))
                .threshold(10000)
                .evaluationPeriods(5)
                .datapointsToAlarm(5)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"DevicesFunctionErrorPct99gt1sAlarm")
                .build();
        kfcDevicesFunctionInvocationAlarm.addAlarmAction(new SnsAction(errorTopic));

        // KFC Devices Function Throttle > 2%
        Alarm kfcDevicesFunctionErrorThrottlePctAlarm = Alarm.Builder.create(this, "KFC Devices Function 2% Throttled")
                .metric(kfcDevicesFunctionThrottlePercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"DevicesFunctionErrorThrottleAlarm")
                .build();
        kfcDevicesFunctionErrorThrottlePctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // ---------------------------------------------------
        // KFC Handler Function Error Metrics
        // ---------------------------------------------------
        Metric invokeKfcHandlerFunctionMetric = kfcFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric errorHandlerFunctionMetric = kfcFunction.metric("Errors", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcHandlerFunctionErrorPctMap = new HashMap<>(){{
            put("i", invokeKfcHandlerFunctionMetric);
            put("e", errorHandlerFunctionMetric);
        }};

        MathExpression kfcHandlerFunctionErrorPercentage = MathExpression.Builder.create()
                .expression("(e / i) * 100")
                .label("% of invocation errors, last 5 mins")
                .usingMetrics(kfcHandlerFunctionErrorPctMap)
                .period(Duration.minutes(5)).build();


        // ---------------------------------------------------
        // KFC Handler Function Throttle Metric
        // ---------------------------------------------------
        Metric throttleKfcHandlerFunctionMetric = kfcFunction.metric("Throttles", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric tInvokeKfcHandlerFunctionMetric = kfcFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> kfcHandlerFunctionThrottleMap = new HashMap<>(){{
            put("t", throttleKfcHandlerFunctionMetric);
            put("i", tInvokeKfcHandlerFunctionMetric);
        }};

        MathExpression kfcHandlerFunctionThrottlePercentage = MathExpression.Builder.create()
                .expression("(t / (i + t)) * 100")
                .label("% of throttled requests, last 30 mins")
                .usingMetrics(kfcHandlerFunctionThrottleMap)
                .period(Duration.minutes(5)).build();


        //*************************************
        //KFC Handler Function
        //*************************************

        // KFC Handler Function Errors > 2%
        Alarm kfcHandlerFunctionErrorPctAlarm = Alarm.Builder.create(this, "KFC Handler Function Errors > 2%")
                .metric(kfcHandlerFunctionErrorPercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"HandlerFunctionError2PctAlarm")
                .build();
        kfcHandlerFunctionErrorPctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // 1% of handler function invocations taking longer than 1 second
        Alarm kfcHandlerFunctionInvocationAlarm = Alarm.Builder.create(this, "KFC Handler Function p99 Long Duration (>1s)")
                .metric(kfcFunction.metricDuration()
                        .with(MetricOptions.builder()
                                .statistic("p99")
                                .period(Duration.seconds(60))
                                .build()))
                .threshold(10000)
                .evaluationPeriods(5)
                .datapointsToAlarm(5)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"HandlerFunctionErrorPct99gt1sAlarm")
                .build();
        kfcHandlerFunctionInvocationAlarm.addAlarmAction(new SnsAction(errorTopic));

        // KFC Handler Function Throttle > 2%
        Alarm kfcHandlerFunctionErrorThrottlePctAlarm = Alarm.Builder.create(this, "KFC Handler Function 2% Throttled")
                .metric(kfcHandlerFunctionThrottlePercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"HandlerFunctionErrorThrottleAlarm")
                .build();
        kfcHandlerFunctionErrorThrottlePctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // ---------------------------------------------------
        // Panorama Handler Function Error Metric
        // ---------------------------------------------------
        Metric invokePanoramaHandlerFunctionMetric = kfcPanoramaHandlerFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric errorPanoramaHandlerFunctionMetric = kfcPanoramaHandlerFunction.metric("Errors", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> panoramaHandlerFunctionErrorPctMap = new HashMap<>(){{
            put("i", invokePanoramaHandlerFunctionMetric);
            put("e", errorPanoramaHandlerFunctionMetric);
        }};

        MathExpression panoramaHandlerFunctionErrorPercentage = MathExpression.Builder.create()
                .expression("(e / i) * 100")
                .label("% of invocation errors, last 5 mins")
                .usingMetrics(panoramaHandlerFunctionErrorPctMap)
                .period(Duration.minutes(5)).build();

        // ---------------------------------------------------
        // Panorama Handler Throttle Metric
        // ---------------------------------------------------
        Metric throttlePanoramaHandlerFunctionMetric = kfcPanoramaHandlerFunction.metric("Throttles", MetricOptions.builder()
                .statistic("sum")
                .build());
        Metric tInvokePanoramaHandlerFunctionMetric = kfcPanoramaHandlerFunction.metric("Invocations", MetricOptions.builder()
                .statistic("sum")
                .build());

        Map<String, Metric> odmpHandlerFunctionThrottleMap = new HashMap<>(){{
            put("t", throttlePanoramaHandlerFunctionMetric);
            put("i", tInvokePanoramaHandlerFunctionMetric);
        }};

        MathExpression panoramaHandlerFunctionThrottlePercentage = MathExpression.Builder.create()
                .expression("(t / (i + t)) * 100")
                .label("% of throttled requests, last 30 mins")
                .usingMetrics(odmpHandlerFunctionThrottleMap)
                .period(Duration.minutes(5)).build();

        //*************************************
        //Panorama Handler Function
        //*************************************

        // Panorama Handler Function Errors > 2%
        Alarm panoramaHandlerFunctionErrorPctAlarm = Alarm.Builder.create(this, "Panorama Handler Function Errors > 2%")
                .metric(panoramaHandlerFunctionErrorPercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PanoramaHandlerFunctionError2PctAlarm")
                .build();
        panoramaHandlerFunctionErrorPctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // 1% of Panorama Handler function invocations taking longer than 1 second
        Alarm panoramaHandlerFunctionInvocationAlarm = Alarm.Builder.create(this, "Panorama Handler Function p99 Long Duration (>1s)")
                .metric(kfcPanoramaHandlerFunction.metricDuration()
                        .with(MetricOptions.builder()
                                .statistic("p99")
                                .period(Duration.seconds(60))
                                .build()))
                .threshold(10000)
                .evaluationPeriods(5)
                .datapointsToAlarm(5)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PanoramaHandlerFunctionErrorPct99gt1sAlarm")
                .build();
        panoramaHandlerFunctionInvocationAlarm.addAlarmAction(new SnsAction(errorTopic));

        // Panorama Handler Function Throttle > 2%
        Alarm panoramaHandlerFunctionErrorThrottlePctAlarm = Alarm.Builder.create(this, "Panorama Handler Function 2% Throttled")
                .metric(panoramaHandlerFunctionThrottlePercentage)
                .threshold(2)
                .evaluationPeriods(6)
                .datapointsToAlarm(1)
                .treatMissingData(TreatMissingData.NOT_BREACHING)
                .alarmName(appName+"-"+"PanoramaHandlerFunctionErrorThrottleAlarm")
                .build();
        panoramaHandlerFunctionErrorThrottlePctAlarm.addAlarmAction(new SnsAction(errorTopic));

        // Panorama Handler 4XX0/5XX Alarm
        Alarm panoramaHandlerFunction4XXAlarm = Alarm.Builder.create(this, "Panorama Handler 4xx Errors")
                .metric(new Metric(MetricProps.builder()
                        .metricName("Errors")
                        .namespace("AWS/Lambda")
                        .dimensions(Map.of("FunctionName", kfcPanoramaHandlerFunction.getFunctionName()))
                        .period(Duration.seconds(60))
                        .statistic("Maximum")
                        .build()))
                .datapointsToAlarm(5)
                .evaluationPeriods(5)
                .actionsEnabled(true)
                .threshold(1)
                .treatMissingData(TreatMissingData.MISSING)
                .comparisonOperator(ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD)
                .alarmName(kfcPanoramaHandlerFunction.getFunctionName()+"LambdaErrorAlarm")
                .alarmDescription("Alarm for 4xx/5xx Errors on " + kfcPanoramaHandlerFunction.getFunctionName()).build();

        panoramaHandlerFunction4XXAlarm.addAlarmAction(new SnsAction(errorTopic));
        panoramaHandlerFunction4XXAlarm.addOkAction(new SnsAction(errorTopic));

    }

    private void getEnvironmentVariables(){

        appName = (String) this.getNode().tryGetContext(CTX_APP_NAME);
        environment = (String) this.getNode().tryGetContext(CTX_ENV);
        loggingLevel = (String) this.getNode().tryGetContext(CTX_LOG_LEVEL);
    }

    private Metric metricForApiGateway(String apiName, String stage, String metricName, String label, String stat) {
        Map<String,String> dimensions = new HashMap<>();
        dimensions.put("ApiName", apiName);
        dimensions.put("Stage", stage);

        return buildMetric(metricName,"AWS/ApiGateway", dimensions, Unit.COUNT, label, stat, 900);

    }

    private Metric buildMetric(String metricName, String namespace, Map<String,String> dimensions, Unit unit, String label, String stat, int period) {

        return new Metric(MetricProps.builder()
                .metricName(metricName)
                .namespace(namespace)
                .dimensions(dimensions)
                .unit(unit)
                .label(label)
                .statistic(stat)
                .period(Duration.seconds(period))
                .build());
    }

}
