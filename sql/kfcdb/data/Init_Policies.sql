USE kfcdb;

INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering) 
VALUES ('Minimal', 'Kajeet-Enterprise-Minimal', 'Kajeet Minimal Filtering Policy', 'init', 'init', 'N');

INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering) 
VALUES ('Moderate', 'Kajeet-Enterprise-Moderate', 'Kajeet Moderate Filtering Policy', 'init', 'init', 'N');

INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering) 
VALUES ('Strict', 'Kajeet-Enterprise-Strict', 'Kajeet Strict Filtering Policy', 'init', 'init', 'N');

INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering, custom_filtering_type) 
VALUES ('Deny All', 'Kajeet_Deny', 'All sites, IPs, and domains are blocked unless an Exceptions list is defined. The Exceptions list will Allow any sites, IPs, domains listed.', 'init', 'init', 'Y', 'Allow');

INSERT INTO policy (name, palo_alto_id, description, created_by, updated_by, custom_filtering, custom_filtering_type) 
VALUES ('Open', 'Kajeet-Enterprise-Open', 'All sites, IPs, and domains are allowed unless an Exceptions list is defined. The Exceptions list will Block any sites, IPs, domains listed.', 'init', 'init', 'Y', 'Deny');

commit;
