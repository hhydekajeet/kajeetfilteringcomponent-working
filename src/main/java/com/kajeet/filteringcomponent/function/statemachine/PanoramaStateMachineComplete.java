package com.kajeet.filteringcomponent.function.statemachine;

import com.amazonaws.services.lambda.runtime.Context;
import com.kajeet.filteringcomponent.common.KFCAsyncMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;

public class PanoramaStateMachineComplete {

    private static final Logger log = LogManager.getLogger(PanoramaStateMachineComplete.class.getName());

    public KFCAsyncMessage handleRequest(KFCAsyncMessage input, Context context) {
        log.debug("PanoramaStateMachineComplete.handleRequest()");

        log.info("Cleanup complete, ending");

        return input;
    }

    }
