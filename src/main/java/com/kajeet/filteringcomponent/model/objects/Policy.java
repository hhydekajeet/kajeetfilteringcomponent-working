package com.kajeet.filteringcomponent.model.objects;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;

public class Policy {
    private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";

    private int id;
    private String name;
    @JsonIgnore
    private String paloAltoId;
    private String description;
    private String custom_filtering;
    @JsonIgnore
    private String created_by;
    @JsonIgnore
    private LocalDateTime createdOn;
    @JsonIgnore
    private String updated_by;
    @JsonIgnore
    private LocalDateTime updatedOn;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaloAltoId() {
        return paloAltoId;
    }

    public void setPaloAltoId(String paloAltoId) {
        this.paloAltoId = paloAltoId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustom_filtering() {
        return custom_filtering;
    }

    public void setCustom_filtering(String custom_filtering) {
        this.custom_filtering = custom_filtering;
    }

    @Override
    public String toString() {
        return Jackson.toJsonString(this);
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }


}
