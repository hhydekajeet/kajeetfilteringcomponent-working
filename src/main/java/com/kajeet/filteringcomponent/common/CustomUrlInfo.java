package com.kajeet.filteringcomponent.common;

import com.amazonaws.util.json.Jackson;

import java.util.List;

public class CustomUrlInfo {
    private List<ClusterInfo> clusters;
    private CustomUrls allowedUrlList;
    private CustomUrls blockedUrlList;
    private List<CustomUrls> listsToEdit;
    private String securityProfileName;
    private String policyName;

    public List<ClusterInfo> getClusters() {
        return clusters;
    }

    public void setClusters(List<ClusterInfo> clusters) {
        this.clusters = clusters;
    }

    public CustomUrls getAllowedUrlList() {
        return allowedUrlList;
    }

    public void setAllowedUrlList(CustomUrls allowedUrlList) {
        this.allowedUrlList = allowedUrlList;
    }

    public CustomUrls getBlockedUrlList() {
        return blockedUrlList;
    }

    public void setBlockedUrlList(CustomUrls blockedUrlList) {
        this.blockedUrlList = blockedUrlList;
    }

    public List<CustomUrls> getListsToEdit() {
        return listsToEdit;
    }

    public void setListsToEdit(List<CustomUrls> listsToEdit) {
        this.listsToEdit = listsToEdit;
    }

    public String getSecurityProfileName() {
        return securityProfileName;
    }

    public void setSecurityProfileName(String securityProfileName) {
        this.securityProfileName = securityProfileName;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String toString() {
        return Jackson.toJsonString(this);
    }

}
