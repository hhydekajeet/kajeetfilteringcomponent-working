package com.kajeet.filteringcomponent.common;

import com.kajeet.filteringcomponent.model.device.Device;
import com.kajeet.filteringcomponent.model.device.Devices;
import com.kajeet.filteringcomponent.model.kfc.CustomUrlApi;
import com.kajeet.filteringcomponent.model.kfc.IpPoolCarrier;
import com.kajeet.filteringcomponent.model.objects.Policy;

import java.util.ArrayList;
import java.util.List;

public class MockResponse {


//    public static List<Policy> getAllPoliciesList() {
//        List<Policy> policyList = new ArrayList<>();
//        Policy policy123 = new Policy ("123", "One Two Three");
//        Policy policy456 = new Policy ("456", "Four Five Six");
//        Policy policy789 = new Policy ("789", "Seven Eight Nine");
//        policyList.add(policy123);
//        policyList.add(policy456);
//        policyList.add(policy789);
//        return policyList;
//    }
//
//    public static Policies getAllPolicies() {
//        Policies policies = new Policies();
//        List<Policy> policyList = new ArrayList<>();
//        Policy policy123 = new Policy ("123", "One Two Three");
//        Policy policy456 = new Policy ("456", "Four Five Six");
//        Policy policy789 = new Policy ("789", "Seven Eight Nine");
//        policyList.add(policy123);
//        policyList.add(policy456);
//        policyList.add(policy789);
//        policies.setPolicies(policyList);
//        return policies;
//    }
//
    public static Policy getOnePolicy(String corpId) {
        Policy policy123 = new Policy ();
        return policy123;
    }

    public static CustomUrlApi getMockCustomUrl (String corpId) {
        CustomUrlApi customUrl = new CustomUrlApi();
        customUrl.setCorpId(corpId);
        customUrl.setType("SomeMockType");
        List<String> urls = new ArrayList<>();
        urls.add("www.kajeet.com");
        urls.add("https://www.cinnabon.com/");
        urls.add("www.youtube.com");
        return customUrl;
    }

    public static IpPoolCarrier getMockIpPoolCarrier (String carrier) {
        IpPoolCarrier ipPoolCarrier = new IpPoolCarrier();
        ipPoolCarrier.setCarrier(carrier);
        ipPoolCarrier.setIpPool("SomeMockIpPool");
        return ipPoolCarrier;
    }

    public static Devices getMockDevices (String corpId) {
        // Not doing anything with input parameter
        Devices devices = new Devices();
        Device device = new Device();
        List<Device> deviceList = new ArrayList<>();
        device.setImei("Imei0987");
        device.setIpAddress("10.1.1.1");
        device.setType("MockType1");
        device.setMdn("Mdn0987");
        deviceList.add(device);
        device = new Device();
        device.setImei("Imei3145");
        device.setIpAddress("10.1.1.2");
        device.setType("MockType2");
        device.setMdn("Mdn3145");
        deviceList.add(device);
        device = new Device();
        device.setImei("Imei2468");
        device.setIpAddress("10.1.1.3");
        device.setType("MockType3");
        device.setMdn("Mdn2468");
        deviceList.add(device);
        devices.setDevices(deviceList);
        return devices;
    }

//    private static final String IMEI_355498696244992 = "DEVICESETTINGS_ORBIC_355498696244992";
//    private static final String IMEI_523685246883306 = "DEVICESETTINGS_ORBIC_523685246883306";
//    private static final String IMEI_307458489336594 = "DEVICESETTINGS_ORBIC_307458489336594";
//    private static final String IMEI_913528973546639 = "DEVICESETTINGS_ORBIC_913528973546639";
//
//    public static String getMockODMPDDeviceSettingsJson(String imei){
//
//        String mockJsonToReturn = "DEVICESETTINGS_ORBIC_"+imei;
//        String jsonToReturn = "";
//
//        if (mockJsonToReturn.equals(IMEI_355498696244992)){
//            jsonToReturn = MockJSON.DEVICESETTINGS_ORBIC_355498696244992;
//        }
//        else if (mockJsonToReturn.equals(IMEI_523685246883306)){
//            jsonToReturn = MockJSON.DEVICESETTINGS_ORBIC_523685246883306;
//        }
//        else if (mockJsonToReturn.equals(IMEI_307458489336594)){
//            jsonToReturn = MockJSON.DEVICESETTINGS_ORBIC_307458489336594;
//        }
//        else if (mockJsonToReturn.equals(IMEI_913528973546639)){
//            jsonToReturn = MockJSON.DEVICESETTINGS_ORBIC_913528973546639;
//        }
//
//        return jsonToReturn;
//    }


}